<form id="editArea">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Área: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="area_nombre" name="area_nombre" value="{{ $area->area_nombre }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="area_user_modificate" name="area_user_modificate" value="{{ Auth::user()->id_usuario }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="id_area" name="id_area" value="{{ $area->id_area }}">
      </div>
    </div>
  </div>
</form>
