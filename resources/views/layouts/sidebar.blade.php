<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-{{ Auth::user()->getConf()->system_theme }}-danger elevation-4">
  <!-- Brand Logo -->
  <a  class="brand-link">
    <img src="{{ asset('img/logo-sidebar.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">SGI</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image align-self-center text-center">
        <img src="{{ Auth::user()->usu_foto_perfil }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->getFullnameAttribute() }}</a>
        <small><a href="{{ route('infoUser') }}" class="text-secondary" onclick="event.preventDefault();
                      document.getElementById('edit-form').submit();">
          <form id="edit-form" action="{{ route('infoUser') }}" method="POST" style="display: none;">
            <input type="hidden" name="id_usuario" value="{{ Auth::user()->id_usuario }}">
              @csrf
          </form><i class="fas fa-pen"></i> Mi Perfil</a></small>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        @if(Auth::user()->rol[0]->id_rol == env('ID_ADMINISTRADOR'))
        <li class="nav-item">
          <a href="{{ route('home') }}" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
              DASHBOARD
              {{-- <span class="right badge badge-danger">New</span> --}}
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link ">
            <i class="nav-icon fas fa-cogs"></i>
            <p>
              Administración
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('listArea') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Área</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('listJob') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Cargo</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('listCategoria') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Categoria</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('listSubCategoria') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Sub Categoria</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('listNivel') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Nivel</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('listPrioridad') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Prioridad</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('listRole') }}" class="nav-link ">
                <i class="far fa-circle nav-icon"></i>
                <p>Perfil</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('listUsers') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Usuarios</p>
              </a>
            </li>
          </ul>
        </li>
        @endif
        <li class="nav-item">
          <a href="{{ route('bandeja') }}" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Bandeja
              {{-- <span class="right badge badge-danger">New</span> --}}
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('newTicket') }}" class="nav-link">
            <i class="nav-icon fas fa-plus-square"></i>
            <p>
              Solicitar
            </p>
          </a>
        </li>
        @if(Auth::user()->rol[0]->id_rol == env('ID_SOPORTE') || Auth::user()->rol[0]->id_rol == env('ID_ADMINISTRADOR'))
         <li class="nav-item ">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Reportes
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('generateReport',['type'=> 1]) }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Reporte RRI</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('generateReport',['type'=> 2]) }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Reporte RULI</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('generateReport',['type'=> 3]) }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Reporte Incidencias</p>
              </a>
            </li>
          </ul>
         </li>
         @endif
        <li class="nav-item">
          <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>
              Cerrar Sesión
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
