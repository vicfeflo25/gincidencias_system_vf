@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endsection

@section('content')
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 {{ Auth::user()->getConf()->letra_theme }}">Listado de Prioridades</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Configuración</a></li>
            <li class="breadcrumb-item active">Prioridades</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- Default box -->
        <div class="col-md-12">
          <div class="card {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-header {{ Auth::user()->getConf()->border_theme }}">
              <h3 class="card-title">Prioridades</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col text-right">
                  <a class="btn btn-success btn-sm text-white mb-3" id="btnAddPrioridad" data-toggle="modal" data-target="#modalPrioridad"><i class="fas fa-plus"></i> Agregar</a>
                </div>
              </div>
              <table class="table table-striped projects display nowrap" id="tblPrioridad" width="100%">
                <thead>
                  <tr>
                    <th style="width: 1%">#</th>
                    <th style="width: 40%">Nombre Prioridad</th>
                    <th style="width: 10%" class="text-center">Estado</th>
                    <th style="width: 20%"  class="text-right">Auditoria</th>
                    <th style="width: 20%"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($prioridad as $key => $value)
                    <tr>
                      <td>#</td>
                      <td><b>{{ $value->prioridad_name }}</b></td>
                      <td class="text-center"><span class="badge badge-{{ $value->color_estado }}">{{ $value->nombre_estado }}</span></td>
                      <td class="text-right">
                        <small>
                          <b>{{ $value->fecha_auditoria }}</b>
                          <b>{{ $value->hora_auditoria }}</b>
                          <br>
                          {{ $value->nombre_auditoria }}
                        </small>
                      </td>
                      <td class="project-actions text-right">
                        <a class="btn btn-primary btn-sm text-white btn-visualize" title="Visualizar" data-id="{{ $value->id_prioridad }}">
                          <i class="fas fa-eye"></i>
                        </a>
                        <a class="btn btn-info btn-sm text-white btn-edit" title="Editar" data-id="{{ $value->id_prioridad }}">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a class="btn btn-{{ ($value->prioridad_estado == 0)?'success':'danger' }}  btn-sm text-white btn-change" title="{{ ($value->prioridad_estado == 0)?'Activar':'Inactivar' }}" data-id="{{ $value->id_prioridad }}" data-user="{{ Auth::user()->id_usuario }}">
                          <i class="fas fa-exchange-alt"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
  </section>

  <div class="modal fade" id="modalPrioridad" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="modalPrioridadTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content {{ Auth::user()->getConf()->body_theme }}">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <a type="button" class="btn btn-primary text-white">Guardar</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      $('#tblPrioridad').DataTable({
        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 1 },
            { responsivePriority: 2, targets: 2 }
        ]
      });

      $("table.dataTable").on('click','.btn-visualize',function(){
        var id_prioridad = $(this).data('id')
        $.ajax({
          url: '{{ route('viewPrioridad') }}',
          method: "POST",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_prioridad":id_prioridad},
          success: function(respuesta) {
            $(".modal#modalPrioridad .modal-title").html('VISUALIZADOR DE PRIORIDAD')
            $(".modal#modalPrioridad .btn-primary").attr('hidden','hidden')
            $(".modal#modalPrioridad").modal('show')
            $(".modal#modalPrioridad .modal-body").html(respuesta)
          }
        });
      })

      $("table.dataTable").on('click','.btn-change',function(){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        var id_prioridad = $(this).data('id')
        var id_user = $(this).data('user')
        $.ajax({
          url: '{{ route('changePrioridad') }}',
          method: "PUT",
          headers: {
            'X-CSRF-Token': $('input[name="csrfToken"]').attr('value')
          },
          data: {"id_prioridad":id_prioridad,"id_user":id_user},
          success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
          }
        });
      })

      $("table.dataTable").on('click','.btn-edit',function(){
        $(".modal#modalPrioridad .btn-primary").removeAttr('hidden')
        var id_prioridad = $(this).data('id')
        $.ajax({
          url: '{{ route('editPrioridad') }}',
          method: "POST",
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_prioridad":id_prioridad},
          success: function(respuesta) {
            $(".modal#modalPrioridad .modal-title").html('EDITAR PRIORIDAD')
            $(".modal#modalPrioridad .btn-primary").attr('data-tipo','2')
            $(".modal#modalPrioridad").modal('show')
            $(".modal#modalPrioridad .modal-body").html(respuesta)
          }
        });
      })

    });

    $(function () {
      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $('a img.img-fluid').removeClass('border border-dark')
        $(this).children().addClass('border border-dark')
        $("#rol_icono").val($(this).attr('href'))
      });
    })

    $("#btnAddPrioridad").on('click',function(){
      $(".modal#modalPrioridad .btn-primary").removeAttr('hidden')
      $(".modal#modalPrioridad .modal-title").html('AGREGAR NUEVA PRIORIDAD')
      $(".modal#modalPrioridad .btn-primary").attr('data-tipo','1')
      $.ajax({
      	url: '{{ route('newPrioridad') }}',
        method: "GET",
      	success: function(respuesta) {
      		$(".modal#modalPrioridad .modal-body").html(respuesta)
      	}
      });
    })

    $(".modal#modalPrioridad .btn-primary").on('click',function(){
      var count = 0;
      var url = '';
      var data = '';
      if($(this).data('tipo') == 1){
        url = '{{ route('savePrioridad') }}';
        data = $("form#addPrioridad").serialize();
        if($("form#addPrioridad #prioridad_name").val() == null || $("form#addPrioridad #prioridad_name").val() == ''){
          $("form#addPrioridad #prioridad_name").addClass('is-invalid');
          count++;
        }else{
          $("form#addPrioridad #prioridad_name").removeClass('is-invalid');
        }
      }else{
        url = '{{ route('updatePrioridad') }}';
        data = $("form#editPrioridad").serialize();
        if($("form#editPrioridad #prioridad_name").val() == null || $("form#editPrioridad #prioridad_name").val() == ''){
          $("form#editPrioridad #prioridad_name").addClass('is-invalid');
          count++;
        }else{
          $("form#editPrioridad #prioridad_name").removeClass('is-invalid');
        }
      }
      if(count == 0){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        $.ajax({
        	url: url,
          method: "PUT",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: data,
        	success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
        	}
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar todos los campos obligatorios para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })


  </script>
@endsection
