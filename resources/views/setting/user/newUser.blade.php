<form id="addUser">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Nombres: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="usu_nombre" name="usu_nombre">
        <input type="hidden" maxlength="150" class="form-control req"  id="usu_usuario_creacion" name="usu_usuario_creacion" value="{{ Auth::user()->id_usuario }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Apellido Paterno: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="usu_apellidop" name="usu_apellidop">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Apellido Materno: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="usu_apellidom" name="usu_apellidom">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Fecha Nacimiento: <span class="text-danger">*</span></label>
        <input type="date"  class="form-control input-sm req adm" id="usu_fechaN" name="usu_fechaN">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>E-Mail: <span class="text-danger">*</span></label>
        <input type="email" class="form-control input-sm req adm" id="usu_email" name="usu_email">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Perfil: <span class="text-danger">*</span></label>
        <select class="form-control req adm" id="id_rol" name="id_rol">
          <option value="0">Seleccione</option>
          @foreach ($list_rol as $key => $value)
            <option value="{{ $value->id_rol }}">{{ $value->rol_nombre }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Cargo: <span class="text-danger">*</span></label>
        <select class="form-control req adm" id="id_cargo" name="id_cargo">
          <option value="0">Seleccione</option>
          @foreach ($list_cargo as $key => $value)
            <option value="{{ $value->id_cargo }}">{{ $value->cargo_nombre }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6" hidden id="div-nivel">
      <div class="form-group">
        <label>Nivel Atención: <span class="text-danger">*</span></label>
        <select class="form-control" id="id_nivel" name="id_nivel">
          <option value="0">Seleccione</option>
          @foreach ($nivel as $key => $value)
            <option value="{{ $value->id_nivel }}">{{ $value->nivel_descripcion }}</option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
</form>
<script>
  $("form#addUser #id_rol").on('change',function(){
    if($('form#addUser #id_rol option:selected').text() == 'SOPORTE'){
      $("form#addUser #div-nivel").removeAttr('hidden');
    }else{
      $("form#addUser #div-nivel").attr('hidden','hidden');
    }
  });
</script>
