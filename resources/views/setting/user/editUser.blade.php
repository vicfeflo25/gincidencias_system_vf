<form id="editUser">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Nombres: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="usu_nombre" name="usu_nombre" value="{{ $user->usuario->usu_nombre }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="usu_usuario_modificacion" name="usu_usuario_modificacion" value="1">
        <input type="hidden" maxlength="150" class="form-control req"  id="id_usuario" name="id_usuario" value="{{ $user->usuario->id_usuario }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Apellido Paterno: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="usu_apellidop" name="usu_apellidop" value="{{ $user->usuario->usu_apellidop }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Apellido Materno: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="usu_apellidom" name="usu_apellidom" value="{{ $user->usuario->usu_apellidom }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Fecha Nacimiento: <span class="text-danger">*</span></label>
        <input type="date"  class="form-control input-sm req adm" id="usu_fechaN" name="usu_fechaN" value="{{ $user->usuario->usu_fechaN }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>E-Mail: <span class="text-danger">*</span></label>
        <input type="email" class="form-control input-sm req adm" id="usu_email" name="usu_email" value="{{ $user->usuario->usu_email }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Perfil: <span class="text-danger">*</span></label>
        <select class="form-control req adm" id="id_rol" name="id_rol">
          <option value="0">Seleccione</option>
          @foreach ($user->list_rol as $key => $value)
            <option value="{{ $value->id_rol }}" {{ ($value->id_rol==$user->usuario->id_rol)?'selected':'' }}>{{ $value->rol_nombre }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6">
      <div class="form-group">
        <label>Cargo: <span class="text-danger">*</span></label>
        <select class="form-control req adm" id="id_cargo" name="id_cargo">
          <option value="0">Seleccione</option>
          @foreach ($user->list_cargo as $key => $value)
            <option value="{{ $value->id_cargo }}" {{ ($value->id_cargo==$user->usuario->id_cargo)?'selected':'' }}>{{ $value->cargo_nombre }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6"  id="div-nivel">
      <div class="form-group">
        <label>Nivel Atención: <span class="text-danger">*</span></label>
        <select class="form-control" id="id_nivel" name="id_nivel">
          <option value="0">Seleccione</option>
          @foreach ($user->nivel as $key => $value)
            <option value="{{ $value->id_nivel }}" {{ ($value->id_nivel==$user->usuario->id_nivel)?'selected':'' }}>{{ $value->nivel_descripcion }}</option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
  $(document).ready( function () {
    if({{ $user->usuario->id_rol }} == {{ env('ID_SOPORTE') }}){
      $("#div-nivel").removeAttr('hidden','hidden')
    }else{
      $("#div-nivel").attr('hidden','hidden')
    }

  })
  $("form#editUser #id_rol").on('change',function(){
    if($('form#editUser #id_rol option:selected').text() == 'SOPORTE'){
      $("form#editUser #div-nivel").removeAttr('hidden');
    }else{
      $("form#editUser #div-nivel").attr('hidden','hidden');
    }
  });
</script>
