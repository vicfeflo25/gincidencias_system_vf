<form id="addNivel">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Nivel: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="nivel_descripcion" name="nivel_descripcion">
        <input type="hidden" maxlength="150" class="form-control req"  id="nivel_user_create" name="nivel_user_create" value="{{ Auth::user()->id_usuario }}">
      </div>
    </div>
  </div>
</form>
