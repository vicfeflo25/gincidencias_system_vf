<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    @yield('style')
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
      .is-invalid {
        border: 1px solid #e8000a !important;
        color: #E8000A;
      }

      .preloader-wrapper { height: 0px;-webkit-transform: translate(0%,0%);transform: translate(0%,0%);display: none; }
      .preloader-wrapper.active { display:block; height: 100%;width: 100%;background: #fff;position: fixed;top: 0; left: 0;z-index: 9999999; }
      .preloader-wrapper.active .preloader { position: absolute;top: 50%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%); }
      .preloader-wrapper.active .preloader p { font-size: 10pt; text-align: center; color: #727275;padding-top: 1em;}

      .modal-header-dark {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: start;
        align-items: flex-start;
        -ms-flex-pack: justify;
        justify-content: space-between;
        padding: 1rem;
        border-bottom: 1px solid #e9ecef;
        border-top-left-radius: .3rem;
        border-top-right-radius: .3rem;
        color: #fff!important;
        background-color:#343a40;
      }

      .card-primary.card-outline {
          border-top: 3px solid #007bff;
      }

      .switch{
        background: #343D5B;
        border-radius: 1000px;
        border: none;
        position: relative;
        cursor: pointer;
        display: flex;
        outline: none;
      }

      .switch::after{
        content: "";
    		display: block;
    		width: 30px;
    		height: 30px;
    		position: absolute;
    		background: #F1F1F1;
    		top: 10;
    		left: 0;
    		right: unset;
    		border-radius: 100px;
    		transition: .3s ease all;
    		box-shadow: 0px 0px 2px 2px rgba(0,0,0,.2);
      }

      .switch.active{
        background: orange;
        color: #000;
      }

      .switch.active::after{
        right: 0;
	      left: unset;
      }

      .switch span{
        width: 30px;
    		height: 30px;
    		line-height: 30px;
    		display: block;
    		background: none;
    		color: #fff;
      }

      .btn-circle.btn-sm {
            width: 30px;
            height: 30px;
            padding: 6px 0px;
            border-radius: 15px;
            font-size: 8px;
            text-align: center;
        }
        .btn-circle.btn-md {
            width: 50px;
            height: 50px;
            padding: 7px 10px;
            border-radius: 25px;
            font-size: 10px;
            text-align: center;
        }
        .btn-circle.btn-xl {
            width: 70px;
            height: 70px;
            padding: 10px 16px;
            border-radius: 35px;
            font-size: 12px;
            text-align: center;
        }

        .badge-status{
          background: rgb(217, 140, 194);
          width: 28px;
          height: 28px;
          border-radius: 14px;
        }

        .inputbtn button{
          border-top-right-radius: none;
          border-bottom-right-radius: none;
        }
    </style>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed sidebar-collapse">
    <div class="preloader-wrapper active {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->letra_theme }}" >
      <div class="preloader">
        <img class="center-block" src="{{ asset('img/colegio_logo.png') }}" style="width: 250px;">
        <p><img src="https://www.gestionflesan.cl/controlflujo/images/preloader_2019.gif" style="width: 25px;"> <strong class="{{ Auth::user()->getConf()->letra_theme }}">OBTENIENDO DATOS</strong></p>
      </div>
    </div>
    <div class="wrapper">

      @include('layouts.nav')
      @include('layouts.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->letra_theme }}">
        @yield('content')
      </div>
      <!-- /.content-wrapper -->

      @include('layouts.sidebar-dark')
      @include('layouts.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    @yield('script_cabecera')
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/adminlte.js') }}"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ asset('js/demo.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


    <!-- PAGE SCRIPTS -->
    {{-- <script src="{{ asset('js/pages/dashboard2.js') }}"></script> --}}
    @yield('script')
    <script type="text/javascript">
    $(function () {
        var url = window.location.pathname;
        var activePage = url.substring(url.lastIndexOf('/') + 1);
        $('.sidebar a').each(function(){
          //var linkPage = this.ahref.substring(this.href.lastIndexOf('/') + 1);
          var linkPage = $(this).attr('href').substring($(this).attr('href').lastIndexOf('/') + 1)
          if (activePage == linkPage) {
              $(this).parents('li').parents('ul').parents('li').addClass('menu-open');
              $(this).parents('li').parents('ul').parents('li').children('a').addClass('active');
              $(this).addClass('active');
          }
        })
      })

    </script>
  </body>
</html>
