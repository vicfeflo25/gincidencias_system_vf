<form id="editCategoria">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Categoria: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)" class="form-control input-sm req" id="categoria_name" value="{{ $categoria->categoria_name }}" name="categoria_name" />
        <input type="hidden" maxlength="150" class="form-control req"  id="categoria_user_modificate" name="categoria_user_modificate" value="{{ Auth::user()->id_usuario }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="id_categoria" name="id_categoria" value="{{ $categoria->id_categoria }}">
      </div>
    </div>
  </div>
</form>
