<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model{

    protected $connection = 'mysql';
    protected $table = 'sgi_nivel';
    protected $primaryKey = 'id_nivel';
    public $timestamps = false;

    public static function getListNivel(){
      $nivel = Nivel::select('sgi_nivel.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador')
                      ->join('usuario_aplicacion as u','u.id_usuario','sgi_nivel.nivel_user_create')
                      ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','sgi_nivel.nivel_user_modificate')
                      ->get();

      foreach ($nivel as $key => $value) {
        if($value->nivel_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else{
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }

        if(empty($value->nivel_user_modificate)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->nivel_fecha_create));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->nivel_fecha_create));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->nivel_fecha_modificate));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->nivel_fecha_modificate));
        }
      }
      return $nivel;
    }

    public static function NivelSave(){
      try {
        $nivel_existe = Nivel::where('nivel_descripcion',strtoupper(request()->nivel_descripcion))->first();
        if(empty($nivel_existe)){
          $nivel = new Nivel();
          $nivel->nivel_user_create = request()->nivel_user_create;
          $nivel->nivel_fecha_create = now();
          $nivel->nivel_descripcion = strtoupper(request()->nivel_descripcion);
          $nivel->nivel_estado = 1;
          $nivel->save();
          return array("error"=>0,"mensaje"=>"Se agregó exitosamente el Nivel ".$nivel->nivel_descripcion,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El Nivel ".strtoupper(request()->nivel_descripcion)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function NivelUpdate(){
      try {
        $nivel_existe = Nivel::where('nivel_descripcion',strtoupper(request()->nivel_descripcion))->where('id_nivel','!=',request()->id_nivel)->first();
        if(empty($nivel_existe)){
          $nivel = Nivel::find(request()->id_nivel);
          $nivel->nivel_user_modificate = request()->nivel_user_modificate;
          $nivel->nivel_fecha_modificate = now();
          $nivel->nivel_descripcion = strtoupper(request()->nivel_descripcion);
          $nivel->save();
          return array("error"=>0,"mensaje"=>"Se modificó exitosamente el Nivel ".$nivel->nivel_descripcion,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El Nivel ".strtoupper(request()->nivel_descripcion)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeStateNivel(){
      try {
        $nivel = Nivel::find(request()->id_nivel);
        if($nivel->nivel_estado == 1){
          $nivel->nivel_estado = 0;
          $mensaje = 'Se inactivo exitosamente el Nivel '.$nivel->nivel_descripcion;
        }else{
          $nivel->nivel_estado = 1;
          $mensaje = 'Se activo exitosamente el Nivel '.$nivel->nivel_descripcion;
        }
        $nivel->nivel_user_modificate = request()->id_user;
        $nivel->nivel_fecha_modificate = now();
        $nivel->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function getNivel(){
      $nivel = Nivel::where('id_nivel',request()->id_nivel)->first();
      return $nivel;
    }

}
