function dashboardfunction(json){
  var json_meses = json.ultimos_meses;
  var json_porc = json.ultimos_porc;
  var fechas = json.fechas;
  var categorias = json.categorias
  var soporte = json.soporte
  var solucion = json.soporte_solucion

  window.Apex = {
    chart: {
      foreColor: '#ccc',
      toolbar: {
        show: false
      },
    },
    stroke: {
      width: 3
    },
    dataLabels: {
      enabled: false
    },
    tooltip: {
      theme: 'dark'
    },
    grid: {
      borderColor: "#535A6C",
      xaxis: {
        lines: {
          show: true
        }
      }
    }
  };

  $.each( categorias, function( key, value ) {
    $("#title_spark"+key).html(value.name)
    var count = {
      chart: {
        id: 'spark1',
        group: 'sparks',
        type: 'bar',
        // type: 'line',
        height: 80,
        sparkline: {
          enabled: true
        },
        dropShadow: {
          enabled: true,
          top: 1,
          left: 1,
          blur: 2,
          opacity: 0.2,
        }
      },
      series: [{
        data: value.data
      }],
      stroke: {
        curve: 'smooth'
      },
      markers: {
        size: 0
      },
      grid: {
        padding: {
          top: 20,
          bottom: 10,
          left: 110
        }
      },
      colors: ['#fff'],
      tooltip: {
        x: {
          show: false
        },
        y: {
          title: {
            formatter: function formatter(val) {
              return '';
            }
          }
        }
      }
    }
     var name = ('#spark'+key.toString())
     new ApexCharts(document.querySelector(name), count).render();
  });


  var optionsLine = {
    chart: {
      height: 328,
      type: 'line',
      zoom: {
        enabled: false
      },
      dropShadow: {
        enabled: true,
        top: 3,
        left: 2,
        blur: 4,
        opacity: 1,
      }
    },
    stroke: {
      curve: 'smooth',
      width: 2
    },
    //colors: ["#3F51B5", '#2196F3'],
    series: categorias,
    title: {
      text: 'CATEGORIAS CON MÁS SOLICITUDES',
      align: 'left',
      offsetY: 25,
      offsetX: 20
    },
    subtitle: {
      text: 'ESTADISTICAS',
      offsetY: 55,
      offsetX: 20
    },
    markers: {
      size: 6,
      strokeWidth: 0,
      hover: {
        size: 9
      }
    },
    grid: {
      show: true,
      padding: {
        bottom: 0
      }
    },
    labels: fechas,
    xaxis: {
      tooltip: {
        enabled: false
      }
    },
    legend: {
      position: 'top',
      horizontalAlign: 'right',
      offsetY: -20
    }
  }

  var chartLine = new ApexCharts(document.querySelector('#line-adwords'), optionsLine);
  chartLine.render();

  var optionsCircle4 = {
    chart: {
      type: 'radialBar',
      height: 310,
      width: 340,
    },
    plotOptions: {
      radialBar: {
        size: undefined,
        inverseOrder: true,
        hollow: {
          margin: 5,
          size: '40%',
          background: 'transparent',

        },
        track: {
          show: false,
        },
        startAngle: -180,
        endAngle: 180
      },
    },
    stroke: {
      lineCap: 'round'
    },
    title: {
      text: '% SOLICITUDES SOLUCIONADAS',
      align: 'left',
      offsetY: 25,
      offsetX: 20
    },
    series: json_porc,
    labels: json_meses,
    legend: {
      show: true,
      floating: true,
      position: 'right',
      offsetX: -70,
      offsetY: 220,
      fontSize: '12px',
      fontFamily: 'Helvetica, Arial',
      height: 200,
      width: 150
    },
  }

  var chartCircle4 = new ApexCharts(document.querySelector('#radialBarBottom'), optionsCircle4);
  chartCircle4.render();


  var optionsBar = {
    chart: {
      height: 380,
      type: 'bar',
      stacked: true,
    },
    plotOptions: {
      bar: {
        columnWidth: '30%',
        horizontal: false,
      },
    },
    title: {
      text: 'USUARIOS - N° ASIGNACIONES DIARIAS',
      align: 'left',
      offsetY: 25,
      offsetX: 20
    },
    series: soporte,
    xaxis: {
      categories: fechas,
    },
    fill: {
      opacity: 1
    },

  }

  var chartBar = new ApexCharts(
    document.querySelector("#barchart"),
    optionsBar
  );

  chartBar.render();

  var optionsArea = {
    chart: {
      height: 380,
      type: 'area',
      stacked: false,
    },
    stroke: {
      curve: 'straight'
    },
    title: {
      text: 'USUARIOS - N° SOLICITUDES SOLUCIONADAS DIARIAS',
      align: 'left',
      offsetY: 25,
      offsetX: 20
    },
    series: solucion,
    xaxis: {
      categories: fechas,
    },
    tooltip: {
      followCursor: true
    },
    fill: {
      opacity: 1,
    },

  }

  var chartArea = new ApexCharts(
    document.querySelector("#areachart"),
    optionsArea
  );

  chartArea.render();

  /* fusionexport integrations START */
  (() => {
      const btn = document.getElementById('fusionexport-btn')
      btn.addEventListener('click', async function() {
        const endPoint = 'https://www.fusioncharts.com/demos/dashboards/fusionexport-apexcharts/api/export-dashboard'
        const information = {
          dashboardName: 'dark'
        };

        this.setAttribute('disabled', true);
        const { data } = await axios.post(endPoint, information, {
          responseType: 'blob'
        });
        await download(data, 'apexCharts-dark-dashboard.pdf', 'application/pdf')
        this.removeAttribute('disabled')
      });
    }
  )();
}










/* fusionexport integrations END */
