<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ticket;
use App\User;

class SolicitudController extends Controller{

  public function saveSolicitud(){
    $ticket = Ticket::saveSolicitud();
    return $ticket;
  }

  public function updateTema(){
    $tema = Ticket::updateTema();
    return $tema;
  }

  public function updateAsigna(){
    $tema = Ticket::updateAsigna();
    return $tema;
  }

  public function addComentario(){
    $ticket = Ticket::addComentario();
    return $ticket;
  }

  public function updateStatus(){
    $tema = Ticket::updateStatus();
    return $tema;
  }

  public function descartSolicitud(){
    $tema = Ticket::descartSolicitud();
    return $tema;
  }

}
