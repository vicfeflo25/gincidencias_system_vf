<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['middleware' => 'auth:member-api'], function(){
  Route::put('/saveRole','API\SettingController@saveRole')->name('saveRole');
  Route::put('/updateRole','API\SettingController@updateRole')->name('updateRole');
  Route::put('/changestatusRole','API\SettingController@changeRole')->name('changeRole');

  Route::put('/saveUser','API\SettingController@saveUser')->name('saveUser');
  Route::put('/updateUser','API\SettingController@updateUser')->name('updateUser');
  Route::put('/changestatusUser','API\SettingController@changeUser')->name('changeUser');

  Route::put('/saveCargo','API\SettingController@saveCargo')->name('saveCargo');
  Route::put('/updateCargo','API\SettingController@updateCargo')->name('updateCargo');
  Route::put('/changestatusJob','API\SettingController@changeJob')->name('changeJob');

  Route::put('/saveNivel','API\SettingController@saveNivel')->name('saveNivel');
  Route::put('/updateNivel','API\SettingController@updateNivel')->name('updateNivel');
  Route::put('/changestatusNivel','API\SettingController@changeNivel')->name('changeNivel');

  Route::put('/savePrioridad','API\SettingController@savePrioridad')->name('savePrioridad');
  Route::put('/updatePrioridad','API\SettingController@updatePrioridad')->name('updatePrioridad');
  Route::put('/changestatusPrioridad','API\SettingController@changePrioridad')->name('changePrioridad');

  Route::put('/saveArea','API\SettingController@saveArea')->name('saveArea');
  Route::put('/updateArea','API\SettingController@updateArea')->name('updateArea');
  Route::put('/changestatusArea','API\SettingController@changeArea')->name('changeArea');

  Route::put('/saveCategoria','API\SettingController@saveCategoria')->name('saveCategoria');
  Route::put('/updateCategoria','API\SettingController@updateCategoria')->name('updateCategoria');
  Route::put('/changestatusCategoria','API\SettingController@changeCategoria')->name('changeCategoria');
  Route::put('/saveSubCategoria','API\SettingController@saveSubCategoria')->name('saveSubCategoria');
  Route::put('/updateSubCategoria','API\SettingController@updateSubCategoria')->name('updateSubCategoria');
  Route::put('/changestatusSubCategoria','API\SettingController@changeSubCategoria')->name('changeSubCategoria');

  Route::post('/saveSolicitud','API\SolicitudController@saveSolicitud')->name('saveSolicitud');

  Route::post('/changeTema','API\SolicitudController@updateTema')->name('changeTema');
  Route::post('/asignaUserSolicitud','API\SolicitudController@updateAsigna')->name('changeAsigna');
  Route::post('/addComentario','API\SolicitudController@addComentario')->name('addComentario');
  Route::post('/changeStatusSolicitud','API\SolicitudController@updateStatus')->name('changeStatusSolicitud');
  Route::post('/descartSolicitud','API\SolicitudController@descartSolicitud')->name('descartSolicitud');


  Route::put('/changeTheme','API\SettingController@changeTheme')->name('changeTheme');
  Route::put('/changePassword','API\SettingController@changePassword')->name('changePassword');
  Route::put('/restaurarPassword','API\SettingController@restaurarPassword')->name('restaurarPassword');

  Route::put('/getRRI','API\ReporteController@getResolucion')->name('getResolucion');
  Route::put('/getRULI','API\ReporteController@getResolucionRULI')->name('getResolucionRULI');
  Route::put('/getINCIDENCIA','API\ReporteController@getResolucionINCIDENCIA')->name('getResolucionINCIDENCIA');

  Route::post('/prueba','API\SettingController@prueba');
  //Route::post('/dashboard','API\ReporteController@dashboard');
// });
