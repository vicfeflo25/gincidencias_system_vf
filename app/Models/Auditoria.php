<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model{

    protected $connection = 'mysql';
    protected $table = 'sgi_auditoria';
    protected $primaryKey = 'id_auditoria';
    public $timestamps = false;

}
