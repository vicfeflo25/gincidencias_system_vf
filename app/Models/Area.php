<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model{

    protected $connection = 'mysql';
    protected $table = 'sgi_area';
    protected $primaryKey = 'id_area';
    public $timestamps = false;

    public static function getListArea(){
      $area = Area::select('sgi_area.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador')
                      ->join('usuario_aplicacion as u','u.id_usuario','sgi_area.area_user_create')
                      ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','sgi_area.area_user_modificate')
                      ->get();

      foreach ($area as $key => $value) {
        if($value->area_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else{
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }

        if(empty($value->area_user_modificate)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->area_fecha_create));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->area_fecha_create));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->area_fecha_modificate));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->area_fecha_modificate));
        }
      }
      return $area;
    }

    public static function getArea(){
      $area = Area::where('id_area',request()->id_area)->first();
      return $area;
    }

    public static function getAreas(){
      $list = Area::where('area_estado',1)->orderBy('id_area','asc')->get();
      return $list;
    }

    public static function areaSave(){
      try {
        $area_existe = Area::where('area_nombre',strtoupper(request()->area_nombre))->first();
        if(empty($area_existe)){
          $area = new area();
          $area->area_user_create = request()->area_user_create;
          $area->area_fecha_create = now();
          $area->area_nombre = strtoupper(request()->area_nombre);
          $area->area_estado = 1;
          $area->save();
          return array("error"=>0,"mensaje"=>"Se agregó exitosamente el Área ".$area->area_nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El Área ".strtoupper(request()->area_nombre)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function areaUpdate(){
      try {
        $area_existe = Area::where('area_nombre',strtoupper(request()->area_nombre))->where('id_area','!=',request()->id_area)->first();
        if(empty($area_existe)){
          $area = Area::find(request()->id_area);
          $area->area_user_modificate = request()->area_user_modificate;
          $area->area_fecha_modificate = now();
          $area->area_nombre = strtoupper(request()->area_nombre);
          $area->save();
          return array("error"=>0,"mensaje"=>"Se modificó exitosamente el Área ".$area->area_nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El Área ".strtoupper(request()->area_nombre)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeStateArea(){
      try {
        $area = Area::find(request()->id_area);
        if($area->area_estado == 1){
          $area->area_estado = 0;
          $mensaje = 'Se inactivo exitosamente el Área '.$area->area_nombre;
        }else{
          $area->area_estado = 1;
          $mensaje = 'Se activo exitosamente el Área '.$area->area_nombre;
        }
        $area->area_user_modificate = request()->id_user;
        $area->area_fecha_modificate = now();
        $area->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }


}
