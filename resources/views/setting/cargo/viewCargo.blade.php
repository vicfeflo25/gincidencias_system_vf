<form id="viewCargo">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Cargo: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)" disabled class="form-control input-sm req" id="cargo_nombre" name="cargo_nombre" value="{{ $cargo->cargo_nombre }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="cargo_user_modificate" name="cargo_user_modificate" value="{{ Auth::user()->id_usuario }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="id_cargo" name="id_cargo" value="{{ $cargo->id_cargo }}">
      </div>
    </div>
  </div>
</form>
