<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
          if(Auth::user()->rol[0]->getRol()->rol_nombre == 'SOLICITANTE'){
            return redirect(RouteServiceProvider::BANDEJA);
          }else {
            return redirect(RouteServiceProvider::HOME);
          }
        }

        return $next($request);
    }
}
