@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endsection

@section('content')
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 {{ Auth::user()->getConf()->letra_theme }}">Listado de Usuarios</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Configuración</a></li>
            <li class="breadcrumb-item active">Usuarios</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- Default box -->
        <div class="col-md-12">
          <div class="card {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-header {{ Auth::user()->getConf()->border_theme }}">
              <h3 class="card-title">Usuarios</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col text-right">
                  <a class="btn btn-success btn-sm text-white mb-3" id="btnAddUser" data-toggle="modal" data-target="#modalUser"><i class="fas fa-plus"></i> Agregar</a>
                </div>
              </div>
              <table class="table table-striped projects display nowrap" id="tblRole" width="100%">
                <thead>
                  <tr>
                    <th style="width: 1%">#</th>
                    <th style="width: 20%">Nombres</th>
                    <th style="width: 20%">Usuario</th>
                    <th style="width: 20%" class="text-center">Cargo</th>
                    <th style="width: 20%" class="text-center">Nivel</th>
                    <th style="width: 20%" class="text-center">Perfil</th>
                    <th style="width: 30%" class="text-center">Foto Perfil</th>
                    <th style="width: 8%" class="text-center">Estado</th>
                    <th class="text-right">Auditoria</th>
                    <th style="width: 20%"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($list_user as $key => $value)
                  <tr>
                    <td>{{ ($key+1) }}</td>
                    <td>
                      <a class="name">{{ strtoupper($value->nombre_usuario) }}</a>
                    </td>
                    <td>
                      <b>{{ strtoupper($value->usuario) }}</b>
                    </td>
                    <td class="text-center">
                      <a>{{ strtoupper($value->cargo_nombre) }}</a>
                    </td>
                    <td class="text-center">
                      <a>{{ strtoupper($value->nivel_descripcion) }}</a>
                    </td>
                    <td class="text-center">
                      <b>{{ strtoupper($value->rol_nombre) }}</b>
                    </td>
                    <td class="text-center">
                      <ul class="list-inline">
                        <li class="list-inline-item"><img alt="Foto Perfil" class="table-avatar" src="{{ $value->usu_foto_perfil }}"></li>
                      </ul>
                    </td>
                    <td class="project-state"><span class="badge badge-{{ $value->color_estado }}">{{ $value->nombre_estado }}</span></td>
                    <td class="project_progress text-right">
                      <small><b>{{ $value->fecha_auditoria }}</b><br><b>{{ $value->hora_auditoria }}</b><br>{{ $value->nombre_auditoria }}</small>
                    </td>
                    <td class="project-actions text-right">
                      <a class="btn btn-primary btn-sm text-white btn-visualize" title="Visualizar" data-id="{{ $value->id_usuario }}"><i class="fas fa-eye"></i></a>
                      <a class="btn btn-info btn-sm text-white btn-edit" title="Editar" data-id="{{ $value->id_usuario }}"><i class="fas fa-pencil-alt"></i></a>
                      <a class="btn btn-{{ ($value->rol_estado == 0)?'success':'danger' }}  btn-sm text-white btn-change" title="{{ ($value->rol_estado == 0)?'Activar':'Inactivar' }}" data-id="{{ $value->id_usuario }}" data-user="{{ Auth::user()->id_usuario }}">
                        <i class="fas fa-exchange-alt"></i>
                      </a>
                      <a class="btn btn-warning  btn-sm text-white btn-resta" title="Restaurar Contraseña" data-id="{{ $value->id_usuario }}" data-user="{{ Auth::user()->id_usuario }}">
                        <i class="fas fa-sync-alt"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
  </section>

  <div class="modal fade" id="modalUser" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="modalUserTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content {{ Auth::user()->getConf()->body_theme }}">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <a type="button" class="btn btn-primary text-white">Guardar</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      $('#tblRole').DataTable({
        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 2 },
            { responsivePriority: 2, targets: 7 }
        ]
      });

      $("table.dataTable").on('click','.btn-resta',function(){
        Swal.fire({
          title: '¿Está usted seguro de restablecer la clave al Usuario '+$(this).parent().parent().find('.name').text()+'?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
        }).then((result) => {
          if (result.isConfirmed) {
            $(".preloader-wrapper").fadeIn(500, 'swing');
            $.ajax({
              url: '/api/restaurarPassword',
              method: "PUT",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {
                      'id_usuario': $(this).data('id')
                    },
              success: function(respuesta) {
                $(".preloader-wrapper").fadeOut(500, 'swing');
                Swal.fire({
                  icon: respuesta.tipo,
                  title: respuesta.titulo,
                  text: respuesta.mensaje,
                  showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
                }).then((result) => {
                  if (result.isConfirmed) {
                    if(respuesta.error == 0){
                      location.reload();
                    }
                  }
                });
              }
            });
          }
        })
      })

      $("table.dataTable").on('click','.btn-visualize',function(){
        var id_usuario = $(this).data('id')
        $.ajax({
          url: '{{ route('viewUser') }}',
          method: "POST",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_usuario":id_usuario},
          success: function(respuesta) {
            $(".modal#modalUser .modal-title").html('VISUALIZADOR DE USUARIO')
            $(".modal#modalUser .btn-primary").attr('hidden','hidden')
            $(".modal#modalUser").modal('show')
            $(".modal#modalUser .modal-body").html(respuesta)
          }
        });
      })

      $("table.dataTable").on('click','.btn-change',function(){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        var id_usuario = $(this).data('id')
        var id_user = $(this).data('user')
        $.ajax({
          url: '{{ route('changeUser') }}',
          method: "PUT",
          headers: {
            'X-CSRF-Token': $('input[name="csrfToken"]').attr('value')
          },
          data: {"id_usuario":id_usuario,"id_user":id_user},
          success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                location.reload();
              }
            });
          }
        });
      })

      $("table.dataTable").on('click','.btn-edit',function(){
        $(".modal#modalUser .btn-primary").removeAttr('hidden')
        var id_usuario = $(this).data('id')
        $.ajax({
          url: '{{ route('editUser') }}',
          method: "POST",
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_usuario":id_usuario},
          success: function(respuesta) {
            $(".modal#modalUser .modal-title").html('EDITAR USUARIO')
            $(".modal#modalUser .btn-primary").attr('data-tipo','2')
            $(".modal#modalUser").modal('show')
            $(".modal#modalUser .modal-body").html(respuesta)
          }
        });
      })

    });

    $(function () {
      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $('a img.img-fluid').removeClass('border border-dark')
        $(this).children().addClass('border border-dark')
        $("#rol_icono").val($(this).attr('href'))
      });
    })

    $("#btnAddUser").on('click',function(){
      $(".modal#modalUser .btn-primary").removeAttr('hidden')
      $(".modal#modalUser .modal-title").html('AGREGAR NUEVO USUARIO')
      $(".modal#modalUser .btn-primary").attr('data-tipo','1')
      $.ajax({
      	url: '{{ route('newUser') }}',
        method: "GET",
      	success: function(respuesta) {
      		$(".modal#modalUser .modal-body").html(respuesta)
      	}
      });
    })

    $(".modal#modalUser .btn-primary").on('click',function(){
      var count = 0;
      var url = '';
      var data = '';
      if($(this).data('tipo') == 1){
        url = '{{ route('saveUser') }}';
        data = $("form#addUser").serialize();
        if($("form#addUser #usu_nombre").val().toUpperCase() == 'ADMIN'){
          $.each($("form#addUser .adm"),function(k,v){
            if(v.value == null || v.value == '' || v.value == 0){
              $(v).addClass('is-invalid');
              count++;
            }else{
              $(v).removeClass('is-invalid');
            }
          })
        }else{
          $.each($("form#addUser .req"),function(k,v){
            if(v.value == null || v.value == '' || v.value == 0){
              $(v).addClass('is-invalid');
              count++;
            }else{
              $(v).removeClass('is-invalid');
            }
          })
        }
        if($('form#addUser #id_rol option:selected').text() == 'SOPORTE'){
          if($("form#addUser #id_nivel").val() == null || $("form#addUser #id_nivel").val() == '' || $("form#addUser #id_nivel").val() == 0){
            $("form#addUser #id_nivel").addClass('is-invalid');
            count++;
          }else{
            $("form#addUser #id_nivel").removeClass('is-invalid');
          }
        }
      }else{
        url = '{{ route('updateUser') }}';
        data = $("form#editUser").serialize();
        if($("form#editUser #usu_nombre").val().toUpperCase() == 'ADMIN'){
          $.each($("form#editUser .adm"),function(k,v){
            if(v.value == null || v.value == '' || v.value == 0){
              $(v).addClass('is-invalid');
              count++;
            }else{
              $(v).removeClass('is-invalid');
            }
          })
        }else{
          $.each($("form#editUser .req"),function(k,v){
            if(v.value == null || v.value == '' || v.value == 0){
              $(v).addClass('is-invalid');
              count++;
            }else{
              $(v).removeClass('is-invalid');
            }
          })
        }
        if($('form#editUser #id_rol option:selected').text() == 'SOPORTE'){
          if($("form#editUser #id_nivel").val() == null || $("form#editUser #id_nivel").val() == '' || $("form#editUser #id_nivel").val() == 0){
            $("form#editUser #id_nivel").addClass('is-invalid');
            count++;
          }else{
            $("form#editUser #id_nivel").removeClass('is-invalid');
          }
        }
      }
      if(count == 0){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        $.ajax({
        	url: url,
          method: "PUT",
          headers: {
            'X-CSRF-Token': $('input[name="csrfToken"]').attr('value')
          },
          data: data,
        	success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
        	}
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar todos los campos obligatorios para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })


  </script>
@endsection
