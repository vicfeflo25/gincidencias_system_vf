@if(!empty($reporte))
<table class="table table-bordered table-striped" id="table1">
  <thead  style="background-color:#3B9FF5">
    <tr>
      <th>ITEMS</th>
      <th>FECHA DE REGISTRO</th>
      <th>HORAS INVERTIDAS RESOLUCIÓN DE INCIDENCIAS(H)</th>
      <th>HORAS DISPONIBLES PARA ATENDER INCIDENCIAS</th>
      <th>RATIO DE UTLIZACIÓN LABORAL EN INCIDENCIAS</th>
    </tr>
  </thead>
  <tbody>
    @foreach($reporte as $key => $value)
    <tr>
      <td>{{ $key+1 }}</td>
      <td>{{ $value->solicitud_fecha }}</td>
      <td>{{ $value->HORAS }}</td>
      <td>{{ $value->HORAS_DISPONIBLES }}</td>
      <td>{{ $value->RULI }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@else
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">¡Alerta!</h4>
  <p>No se encontraron registros para las fechas Seleccionadas.</p>
</div>
@endif

<script type="text/javascript">
  $(document).ready( function () {
    $('#table1').DataTable({
      responsive: true,
      columnDefs: [
          { responsivePriority: 1, targets: 1 },
          { responsivePriority: 2, targets: 2 }
      ],
      order: [[ 0, 'asc' ]],
      dom: 'Bfrtip',
      buttons: [
        { extend: 'pdf', text:  'Exportar a PDF' },
        { extend: 'excel', text: 'Exportar a EXCEL' },
        'pageLength'
      ],
      lengthMenu: [
        [ 10, 25, 50, -1 ],
        [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar todo' ]
      ],
    });
  });
</script>
