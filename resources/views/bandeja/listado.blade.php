@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 {{ Auth::user()->getConf()->letra_theme }}">Listado de Tickets</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active"><a href="#">Bandeja</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">

      <!-- Default box -->
      <div class="col-md-12">
        <div class="card {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
          <div class="card-header {{ Auth::user()->getConf()->border_theme }}">
            <h3 class="card-title">Bandeja de Tickets</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col text-right">
                <a class="btn btn-success btn-sm text-white mb-3" href="{{ route('newTicket') }}"><i class="fas fa-plus"></i> Crear Ticket</a>
              </div>
            </div>
            <table class="table table-striped projects display nowrap" id="tblRole" width="100%">
              <thead>
                <tr>
                  <th style="width: 1%">#</th>
                  <th style="width: 20%">Cliente</th>
                  <th style="width: 30%" class="text-left">Tema</th>
                  <th style="width: 30%" class="text-left">Area</th>
                  <th style="width: 8%" class="text-left">Estado</th>
                  {{-- <th style="width: 8%" class="text-left">Tipo</th> --}}
                  <th style="width: 8%" class="text-left">Prioridad</th>
                  <th style="width: 15%" class="text-right">Auditoria</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($list_ticket as $key => $value)
                  <tr>
                    <td><b>#{{ $value->solicitud_numero }}</b></td>
                    <td><span class="badge bg-primary badge-status" title="PRIORIDAD: {{ $value->cliente }}"><label class="h6">{{ $value->cliente[0] }}</label></span> {{ $value->cliente }}</td>
                    <td><a href="{{ route('viewTicket',['numero'=>$value->solicitud_numero]) }}">{{ $value->solicitud_tema }}</a></td>
                    <td class="text-left">{{ $value->area_nombre }}</td>
                    <td class="text-left"><span class="badge {{ $value->color_estado }} badge-status" title="ESTADO: {{ $value->nombre_estado }}"><label class="h6">{{ $value->nombre_estado[0] }}</label></span> {{ $value->nombre_estado }}</td>
                    {{-- <td class="text-left"><span class="badge {{ $value->tipo_color }} badge-status" title="TIPO: {{ $value->tipo }}"><label class="h6">{{ $value->tipo [0] }}</label></span> {{ $value->tipo }}</td> --}}
                    <td class="text-left"><span class="badge {{ $value->prioridad_color }} badge-status" title="PRIORIDAD: {{ $value->prioridad }}"><label class="h6">{{ $value->prioridad[0] }}</label></span> {{ $value->prioridad }}</td>
                    <td class="text-right"><small><b>{{ $value->solicitud_fecha_create_dia }}<br>{{ $value->solicitud_fecha_create_hora }}</b><br>{{ $value->nombre_creador.' '.$value->apellidop_creador.' '.$value->apellidom_creador }}</small></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <!-- /.card -->
    </section>
@endsection

@section('script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      $('#tblRole').DataTable({
        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 1 },
            { responsivePriority: 2, targets: 2 }
        ],
        order: [[ 0, 'desc' ],[ 6, 'desc' ]]
      });

      $("table.dataTable").on('click','.btn-visualize',function(){
        var id_rol = $(this).data('id')
        $.ajax({
          url: '{{ route('viewRole') }}',
          method: "POST",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_rol":id_rol},
          success: function(respuesta) {
            $(".modal#modalRol .modal-title").html('VISUALIZADOR DE ROL')
            $(".modal#modalRol .btn-primary").attr('hidden','hidden')
            $(".modal#modalRol").modal('show')
            $(".modal#modalRol .modal-body").html(respuesta)
          }
        });
      })

      $("table.dataTable").on('click','.btn-change',function(){
        var id_rol = $(this).data('id')
        $.ajax({
          url: '{{ route('changeRole') }}',
          method: "PUT",
          headers: {
            'X-CSRF-Token': $('input[name="csrfToken"]').attr('value')
          },
          data: {"id_rol":id_rol},
          success: function(respuesta) {
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                location.reload();
              }
            });
          }
        });
      })

      $("table.dataTable").on('click','.btn-edit',function(){
        $(".modal#modalRol .btn-primary").removeAttr('hidden')
        var id_rol = $(this).data('id')
        $.ajax({
          url: '{{ route('editRole') }}',
          method: "POST",
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_rol":id_rol},
          success: function(respuesta) {
            $(".modal#modalRol .modal-title").html('EDITAR ROL')
            $(".modal#modalRol .btn-primary").attr('data-tipo','2')
            $(".modal#modalRol").modal('show')
            $(".modal#modalRol .modal-body").html(respuesta)
          }
        });
      })

    });

    $(function () {
      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $('a img.img-fluid').removeClass('border border-dark')
        $(this).children().addClass('border border-dark')
        $("#rol_icono").val($(this).attr('href'))
      });
    })

    $("#btnAddRol").on('click',function(){
      $(".modal#modalRol .btn-primary").removeAttr('hidden')
      $(".modal#modalRol .modal-title").html('AGREGAR NUEVO ROL')
      $(".modal#modalRol .btn-primary").attr('data-tipo','1')
      $.ajax({
      	url: '{{ route('newRole') }}',
        method: "GET",
      	success: function(respuesta) {
      		$(".modal#modalRol .modal-body").html(respuesta)
      	}
      });
    })

    $(".modal#modalRol .btn-primary").on('click',function(){
      var count = 0;
      var url = '';
      var data = '';
      if($(this).data('tipo') == 1){
        url = '{{ route('saveRole') }}';
        data = $("form#addRole").serialize();
        $.each($("form#addRole .req"),function(k,v){
          if(v.value == null || v.value == ''){
            $(v).addClass('is-invalid');
            count++;
          }else{
            $(v).removeClass('is-invalid');
          }
        })
      }else{
        url = '{{ route('updateRole') }}';
        data = $("form#updatedRole").serialize();
        $.each($("form#updatedRole .req"),function(k,v){
          if(v.value == null || v.value == ''){
            $(v).addClass('is-invalid');
            count++;
          }else{
            $(v).removeClass('is-invalid');
          }
        })
      }
      if(count == 0){
        $.ajax({
        	url: url,
          method: "PUT",
          data: data,
        	success: function(respuesta) {
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
        	}
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar todos los campos obligatorios para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })


  </script>
@endsection
