<form id="viewRole">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Nombre Perfil: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)" readonly class="form-control input-sm req" id="rol_nombre" name="rol_nombre" value="{{ $rol->rol_nombre }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="rol_user_create" name="rol_user_create" value="{{ $rol->rol_user_create }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12">
      <div class="form-group text-center">
        <img src="{{ $rol->rol_icono }}" class="img-fluid mb-2 rounded-circle" alt="white sample"/>
      </div>
    </div>
  </div>
</form>
