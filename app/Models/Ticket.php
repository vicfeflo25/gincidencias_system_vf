<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;
use DateTime;
use Carbon\Carbon;

class Ticket extends Model{

    protected $connection = 'mysql';
    protected $table = 'sgi_solicitud';
    protected $primaryKey = 'id_solicitud';
    public $timestamps = false;

    protected $fillable = [
        'id_solicitud','id_tipo','id_prioridad','id_area', 'id_categoria','id_subcategoria','solicitud_tema',
        'solicitud_codigo','solicitud_anio','solicitud_numero','solicitud_fecha', 'solicitud_estado','solicitud_user_create','solicitud_fecha_create',
        'solicitud_user_modificate','solicitud_fecha_modificate','solicitud_asigna_user','solicitud_user_asigna', 'solicitud_fecha_asigna','solicitud_user_soluciona','solicitud_fecha_soluciona',
        'solicitud_user_cerrado','solicitud_fecha_cerrado','solicitud_user_descarte','solicitud_fecha_descarte'
    ];

    public static function getEstado($estado){
      if($estado == 1){
        $nombre_estado = 'ABIERTO';
      }else if($estado == 2){
        $nombre_estado = 'EN PROGRESO';
      }else if($estado == 3){
        $nombre_estado = 'RESUELTA';
      }else if($estado == 4){
        $nombre_estado = 'CERRADO';
      }

       //PENDIENTE _ LO NUEVO
        //SOLUCIONADO - FUE ATENDIDO
        //CERRADO - DESPUES DE 12 HRS SOLUCIONADO
        //DESCARTADO
      return $nombre_estado;
    }

    public static function getColorEstado($estado){
      if($estado == 1){
        $color_estado = 'bg-warning';
      }else if($estado == 2){
        $color_estado = 'bg-primary';
      }else if($estado == 3){
        $color_estado = 'bg-primary';
      }else if($estado == 4){
        $color_estado = 'bg-success';
      }else if($estado == 5){
        $color_estado = 'bg-danger';
      }
      return $color_estado;
    }

    public static function getList(){
      $list_ticket = Ticket::select('sgi_solicitud.*','u.usu_nombre as nombre_creador','u.usu_apellidop as apellidop_creador','u.usu_apellidom as apellidom_creador','u.usuario as usuario_creador','u.usu_email as email_creador','sc.sc_name as subcategoria','c.categoria_name as categoria','p.prioridad_name as prioridad','t.tipo_name as tipo','t.tipo_color','p.prioridad_color','a.area_nombre')
                           ->join('subcategoria as sc','sc.id_subcategoria','sgi_solicitud.id_subcategoria')
                           ->join('categoria as c','c.id_categoria','sgi_solicitud.id_categoria')
                           ->join('prioridad as p','p.id_prioridad','sgi_solicitud.id_prioridad')
                           ->join('sgi_area as a','a.id_area','sgi_solicitud.id_area')
                           ->join('sgi_tipo as t','t.id_tipo','sgi_solicitud.id_tipo')
                           ->join('usuario_aplicacion as u','u.id_usuario','sgi_solicitud.solicitud_user_create')
                           ->orderBy('solicitud_fecha_create','desc')
                           ->get();

      foreach($list_ticket as $key => $value){
        $value->nombre_estado = Ticket::getEstado($value->solicitud_estado);
        $value->color_estado = Ticket::getColorEstado($value->solicitud_estado);
        $value->cliente = $value->nombre_creador.' '.$value->apellidop_creador.' '.$value->apellidom_creador;
        $value->solicitud_fecha_create_dia = date_format(date_create($value->solicitud_fecha_create),'d-m-Y');
        $value->solicitud_fecha_create_hora = date_format(date_create($value->solicitud_fecha_create),'H:i:s');
      }

      if(Auth::user()->rol[0]->id_rol == env('ID_SOLICITANTE')){
        $list_ticket = $list_ticket->where('solicitud_user_create',Auth::user()->id_usuario);
      }


      return $list_ticket;
    }

    public static function newTicket(){
      $solicitud = new Ticket();
      $solicitud->categorias = Categoria::getCategoriaActivate();
      $solicitud->subcategorias = SubCategoria::getSCActivate();
      $solicitud->tipo = Tipo::getTipos();
      $solicitud->prioridades = Prioridad::getPrioridad();
      $solicitud->areas = Area::getAreas();
      return $solicitud;
    }

    public static function saveSolicitud(){
      try {
        $codigo_max = Ticket::select('solicitud_codigo')
                            ->where('solicitud_fecha',date('Y-m-d'))
                            ->orderBy('solicitud_codigo','desc')
                            ->first();

        $user = User::find(request()->id_usuario);

        if(!$codigo_max){
          $codigo_max = new Ticket();
          $codigo_max->solicitud_codigo = 0;
        }

        $solicitud = new Ticket();
        $solicitud->id_tipo = request()->id_tipo;
        $solicitud->id_prioridad = request()->id_prioridad;
        $solicitud->id_categoria = request()->id_categoria;
        $solicitud->id_subcategoria = request()->id_subcategoria;
        $solicitud->id_area = request()->id_area;
        $solicitud->solicitud_tema = mb_strtoupper(request()->solicitud_tema);
        $solicitud->solicitud_user_create = request()->id_usuario;
        $solicitud->solicitud_fecha_create = now();
        $solicitud->solicitud_estado = 1;
        $solicitud->solicitud_anio = date('Y');
        $solicitud->solicitud_fecha = date('Y-m-d');
        $solicitud->solicitud_codigo = ($codigo_max->solicitud_codigo + 1);
        if(strlen($solicitud->solicitud_codigo) == 1){
          $solicitud->solicitud_numero = date('Y').date('m').date('d').'0'.$solicitud->solicitud_codigo;
        }else if(strlen($solicitud->solicitud_codigo) == 2){
          $solicitud->solicitud_numero = date('Y').date('m').date('d').$solicitud->solicitud_codigo;
        }

        $solicitud->save();

        $comentario = new Comentario();
        $comentario->id_solicitud = $solicitud->id_solicitud;
        $comentario->comentario_descripcion =  request()->comentario_descripcion;
        $comentario->comentario_tipo = 1;
        $comentario->comentario_estado = 1;
        $comentario->comentario_user_create = request()->id_usuario;
        $comentario->comentario_fecha_create = now();
        $comentario->save();

        $doc = request()->file('file_adjuntar');
        if(!empty($doc)){
          foreach($doc as $photo){
            $extension = pathinfo($photo->getClientOriginalName(),PATHINFO_EXTENSION);
            $nombre_now = strtotime(date('Y-m-d H:i:s'));
            $nombre = $nombre_now.'.'.$extension;

            \Storage::disk('public')->put('solicitud/'.$solicitud->solicitud_numero.'/'.$nombre,  \File::get($photo));

            $adjunto = new Adjuntar();
            $adjunto->id_comentario = $comentario->id_comentario;
            $adjunto->adjuntar_url = 'storage/solicitud/'.$solicitud->solicitud_numero.'/'.$nombre;
            $adjunto->adjuntar_user_create = request()->id_usuario;
            $adjunto->adjuntar_fecha_create = now();
            $adjunto->adjuntar_estado = 1;
            $adjunto->save();
          }
        }

        $auditoria = new Auditoria();
        $auditoria->id_solicitud = $solicitud->id_solicitud;
        $auditoria->id_usuario = request()->id_usuario;
        $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha registrado la solicitud '.$solicitud->solicitud_numero;
        $auditoria->auditoria_date = date('Y-m-d');
        $auditoria->auditoria_fecha = now();
        $auditoria->auditoria_estado = 1;
        $auditoria->save();

        return array("error"=>0,"mensaje"=>"Se Registró exitosamente el Ticket ".$solicitud->solicitud_numero,"tipo"=>"success","titulo"=>"¡Éxito!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function updateTema(){
      try {
        $ticket = Ticket::find(request()->id_solicitud);
        if($ticket->solicitud_tema == request()->solicitud_tema){
          return array("error"=>1,"mensaje"=>"El Tema que intenta actualizar es el mismo por el cual intenta modificar.","tipo"=>"warning","titulo"=>"¡Alerta!","solicitud_tema"=>$ticket->solicitud_tema);
        }else{
          $ticket->solicitud_tema = mb_strtoupper(request()->solicitud_tema);
          $ticket->solicitud_user_modificate = request()->id_usuario;
          $ticket->solicitud_fecha_modificate = now();
          $ticket->save();

          $user = User::find(request()->id_usuario);

          $auditoria = new Auditoria();
          $auditoria->id_solicitud = $ticket->id_solicitud;
          $auditoria->id_usuario = request()->id_usuario;
          $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha actualizado el tema de la solicitud '.$ticket->solicitud_numero.' a '.request()->solicitud_tema;
          $auditoria->auditoria_date = date('Y-m-d');
          $auditoria->auditoria_fecha = now();
          $auditoria->auditoria_estado = 2;
          $auditoria->save();

          return array("error"=>0,"mensaje"=>"Se Actualizó exitosamente el Tema del Ticket ".$ticket->solicitud_numero,"tipo"=>"success","titulo"=>"¡Éxito!","solicitud_tema"=>$ticket->solicitud_tema);
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function updateAsigna(){
      try {
        $ticket = Ticket::find(request()->id_solicitud);
        if($ticket->solicitud_tema == request()->solicitud_tema){
          return array("error"=>1,"mensaje"=>"El Usuario que intenta asignar ya esta asignado a esta solicitud.","tipo"=>"warning","titulo"=>"¡Alerta!","solicitud_tema"=>$ticket->solicitud_tema);
        }else{
          $ticket->solicitud_asigna_user = request()->id_asignado;
          $ticket->solicitud_user_asigna = request()->id_usuario;
          $ticket->solicitud_fecha_asigna = now();
          $ticket->solicitud_estado = 2;
          $ticket->save();

          $user = User::find(request()->id_usuario);
          $usuario = User::find(request()->id_asignado);

          $auditoria = new Auditoria();
          $auditoria->id_solicitud = $ticket->id_solicitud;
          $auditoria->id_usuario = request()->id_usuario;
          $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha asignado a '.$usuario->usu_nombre.' '.$usuario->usu_apellidop.' '.$usuario->usu_apellidom.' a la solicitud '.$ticket->solicitud_numero;
          $auditoria->auditoria_date = date('Y-m-d');
          $auditoria->auditoria_fecha = now();
          $auditoria->auditoria_estado = 3;
          $auditoria->save();

          return array("error"=>0,"mensaje"=>"Se Asignó exitosamente al usuario ".$usuario->usu_nombre.' '.$usuario->usu_apellidop.' '.$usuario->usu_apellidom." a la Solicitud ".$ticket->solicitud_numero,"tipo"=>"success","titulo"=>"¡Éxito!","solicitud_tema"=>$ticket->solicitud_tema);
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function getTicket($numero){
      $ticket = Ticket::select('sgi_solicitud.*','u.usu_nombre as nombre_creador','u.usu_apellidop as apellidop_creador','u.usu_apellidom as apellidom_creador','u.usuario as usuario_creador','u.usu_email as email_creador','sc.sc_name as subcategoria','sc.id_nivel','c.categoria_name as categoria','p.prioridad_name as prioridad','t.tipo_name as tipo','t.tipo_color','p.prioridad_color','a.area_nombre')
                           ->join('subcategoria as sc','sc.id_subcategoria','sgi_solicitud.id_subcategoria')
                           ->join('categoria as c','c.id_categoria','sgi_solicitud.id_categoria')
                           ->join('prioridad as p','p.id_prioridad','sgi_solicitud.id_prioridad')
                           ->join('sgi_tipo as t','t.id_tipo','sgi_solicitud.id_tipo')
                           ->join('sgi_area as a','a.id_area','sgi_solicitud.id_area')
                           ->join('usuario_aplicacion as u','u.id_usuario','sgi_solicitud.solicitud_user_create')
                           ->where('solicitud_numero',$numero)
                           ->first();

      $ticket->nombre_estado = Ticket::getEstado($ticket->solicitud_estado);
      $ticket->color_estado = Ticket::getColorEstado($ticket->solicitud_estado);
      $ticket->cliente = $ticket->nombre_creador.' '.$ticket->apellidop_creador.' '.$ticket->apellidom_creador;
      $fecha_crea = Carbon::parse($ticket->solicitud_fecha_create);
      $date_crea = $fecha_crea->locale();
      $mes_crea = $fecha_crea->monthName;
      $ticket->mes_creacion = ucwords($mes_crea).' '.date('d',strtotime($ticket->solicitud_fecha_create)).', '.date('Y',strtotime($ticket->solicitud_fecha_create)).' '.date('H:i A',strtotime($ticket->solicitud_fecha_create));
      if(!empty($ticket->solicitud_fecha_asigna)){
        $fecha_asigna = Carbon::parse($ticket->solicitud_fecha_asigna);
        $date_asigna = $fecha_asigna->locale();
        $mes_asigna = $fecha_asigna->monthName;
        $ticket->mes_asignacion = ucwords($mes_asigna).' '.date('d',strtotime($ticket->solicitud_fecha_asigna)).', '.date('Y',strtotime($ticket->solicitud_fecha_asigna)).' '.date('H:i A',strtotime($ticket->solicitud_fecha_asigna));
      }else{
        $ticket->mes_asignacion = '----------------------------------';
      }
      if(!empty($ticket->solicitud_fecha_modificate)){
        $fecha_modificate = Carbon::parse($ticket->solicitud_fecha_modificate);
        $date_modificate = $fecha_modificate->locale();
        $mes_modificate = $fecha_modificate->monthName;
        $ticket->mes_modificate = ucwords($mes_modificate).' '.date('d',strtotime($ticket->solicitud_fecha_modificate)).', '.date('Y',strtotime($ticket->solicitud_fecha_modificate)).' '.date('H:i A',strtotime($ticket->solicitud_fecha_modificate));
      }else{
        $ticket->mes_modificate = '----------------------------------';
      }

      $ticket->usuarios = User::join('usuario_rol as r','r.id_usuario','usuario_aplicacion.id_usuario')
                              ->where('r.id_rol',env('ID_SOPORTE'))
                              ->orWhere('r.id_rol',env('ID_ADMINISTRADOR'))
                              ->where('usu_estado',1)
                              ->get();

      $count = 0;

      $ticket->comentarios = Comentario::select('sgi_solicitud_comentario.*','u.usu_nombre as nombre_creador','u.usu_apellidop as apellidop_creador','u.usu_apellidom as apellidom_creador','u.usuario as usuario_creador','u.usu_email as email_creador','up.usu_nombre as nombre_actualizador','up.usu_apellidop as apellidop_actualizador','up.usu_apellidom as apellidom_actualizador','up.usuario as usuario_actualizador','up.usu_email as email_actualizador')
                                       ->join('usuario_aplicacion as u','u.id_usuario','sgi_solicitud_comentario.comentario_user_create')
                                       ->leftjoin('usuario_aplicacion as up','up.id_usuario','sgi_solicitud_comentario.comentario_user_modificate')
                                       ->where('id_solicitud',$ticket->id_solicitud)
                                       ->where('comentario_estado',1)
                                       ->orderby('comentario_fecha_create','asc')
                                       ->get();

      foreach($ticket->comentarios as $key => $value){
        if($value->comentario_tipo == 4){
          $value->comentario_permisos = array(env('ID_ADMINISTRADOR'),env('ID_SOPORTE'));
        }else{
          $value->comentario_permisos = array(env('ID_ADMINISTRADOR'),env('ID_SOPORTE'),env('ID_SOLICITANTE'));
        }

        if(empty($value->comentario_user_modificate)){
          $value->usuario_apellidop = $value->apellidop_creador;
          $value->usuario_apellidom = $value->apellidom_creador;
          $value->usuario_nombre = $value->nombre_creador;
          $value->comentario_usuario = $value->nombre_creador.' '.$value->apellidop_creador.' '.$value->apellidom_creador;
          $value->comentario_correo = $value->email_creador;
        }else{
          $value->comentario_usuario = $value->nombre_actualizador.' '.$value->apellidop_actualizador.' '.$value->apellidom_actualizador;
          $value->comentario_correo = $value->email_actualizador;
          $value->usuario_apellidop = $value->apellidop_actualizador;
          $value->usuario_apellidom = $value->apellidom_actualizador;
          $value->usuario_nombre = $value->nombre_actualizador;
        }
        $value->imagenes = Adjuntar::where('id_comentario',$value->id_comentario)
                                  ->where('adjuntar_estado',1)
                                  ->orderby('adjuntar_fecha_create','asc')
                                  ->get();
        $count = ($count + count($value->imagenes));
      }
      $ticket->contador = $count;

      return $ticket;
    }

    public static function addComentario(){
      try {
        $user = User::find(request()->comentario_user_create);
        $solicitud = Ticket::find(request()->id_solicitud);

        $comentario = new Comentario();
        $comentario->id_solicitud = request()->id_solicitud;
        $comentario->comentario_descripcion = request()->comentario_descripcion;
        $comentario->comentario_tipo = request()->comentario_tipo;
        $comentario->comentario_estado = 1;
        $comentario->comentario_user_create = request()->comentario_user_create;
        $comentario->comentario_fecha_create = now();
        $comentario->save();

        $solicitud->solicitud_user_modificate = request()->comentario_user_create;
        $solicitud->solicitud_fecha_modificate = now();
        if($comentario->comentario_tipo == 3){
          $solicitud->solicitud_user_soluciona = request()->comentario_user_create;
          $solicitud->solicitud_fecha_soluciona = now();
          $solicitud->solicitud_estado = 3;
        }
        $solicitud->save();

        $doc = request()->file('file_adjuntar');
        if(!empty($doc)){
          foreach($doc as $photo){
            $extension = pathinfo($photo->getClientOriginalName(),PATHINFO_EXTENSION);
            $nombre_now = strtotime(date('Y-m-d H:i:s'));
            $nombre = $nombre_now.'.'.$extension;

            \Storage::disk('public')->put('solicitud/'.$solicitud->solicitud_numero.'/'.$nombre,  \File::get($photo));

            $adjunto = new Adjuntar();
            $adjunto->id_comentario = $comentario->id_comentario;
            $adjunto->adjuntar_url = 'storage/solicitud/'.$solicitud->solicitud_numero.'/'.$nombre;
            $adjunto->adjuntar_user_create = request()->comentario_user_create;
            $adjunto->adjuntar_fecha_create = now();
            $adjunto->adjuntar_estado = 1;
            $adjunto->save();
          }
        }

        if($comentario->comentario_tipo == 3){
          $auditoria = new Auditoria();
          $auditoria->id_solicitud = $solicitud->id_solicitud;
          $auditoria->id_usuario = request()->comentario_user_create;
          $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha respondido y ha dado como Solucionado la solicitud '.$solicitud->solicitud_numero;
          $auditoria->auditoria_date = date('Y-m-d');
          $auditoria->auditoria_fecha = now();
          $auditoria->auditoria_estado = 6;
          $auditoria->save();
          $mensaje = "Se registró exitosamente el comentario y se dio por solucionado exitosamente la Solicitud ".$solicitud->solicitud_numero;
        }else{
          $auditoria = new Auditoria();
          $auditoria->id_solicitud = $solicitud->id_solicitud;
          $auditoria->id_usuario = request()->comentario_user_create;
          $auditoria->auditoria_date = date('Y-m-d');
          $auditoria->auditoria_fecha = now();
          if($comentario->comentario_tipo == 4){
            $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha agregado una nota al personal en la solicitud '.$solicitud->solicitud_numero;
            $auditoria->auditoria_estado = 5;
            $mensaje = "Se registró exitosamente la Nota al Personal en la Solicitud ".$solicitud->solicitud_numero;
          }else{
            $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha respondido a la solicitud '.$solicitud->solicitud_numero;
            $auditoria->auditoria_estado = 4;
            $mensaje = "Se registró exitosamente el comentario en la Solicitud ".$solicitud->solicitud_numero;
          }
          $auditoria->save();
        }

        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","titulo"=>"¡Éxito!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function updateStatus(){
      //id_resolucion = 1 -> SOLUCIONADO
      //id_resolucion = 2 -> SOLUCIONADO Y CERRADO
      //id_resolucion = 3 -> CERRADO
      try{
        $user = User::find(request()->id_usuario);
        $solicitud = Ticket::find(request()->id_solicitud);

        $auditoria = new Auditoria();
        $auditoria->id_solicitud = $solicitud->id_solicitud;
        $auditoria->id_usuario = request()->id_usuario;
        $auditoria->auditoria_date = date('Y-m-d');
        $auditoria->auditoria_fecha = now();

        if(request()->id_resolucion == 1){
          $solicitud->solicitud_user_soluciona = request()->id_usuario;
          $solicitud->solicitud_fecha_soluciona = now();
          $solicitud->solicitud_estado = 3;

          $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha marcado como solucionada la solicitud '.$solicitud->solicitud_numero;
          $auditoria->auditoria_estado = 6;
          $mensaje = "Se Marcó como Solucionada la Solicitud ".$solicitud->solicitud_numero.' exitosamente.';
        }else if(request()->id_resolucion == 2){
          $solicitud->solicitud_user_soluciona = request()->id_usuario;
          $solicitud->solicitud_fecha_soluciona = now();
          $solicitud->solicitud_user_cerrado = request()->id_usuario;
          $solicitud->solicitud_fecha_cerrado = now();
          $solicitud->solicitud_estado = 4;

          $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha marcado como solucionada y cerrada la solicitud '.$solicitud->solicitud_numero;
          $auditoria->auditoria_estado = 7;
          $mensaje = "Se Marcó como Solucionada y Cerrada la Solicitud ".$solicitud->solicitud_numero.' exitosamente.';
        }else if(request()->id_resolucion == 3){
          if(in_array($solicitud->solicitud_estado,array(1,2))){
            return array("error"=>1,"mensaje"=>"Esta solicitud debe estar en estado Solucionado para continuar.","tipo"=>"warning","titulo"=>"¡Alerta!");
          }else{
            $solicitud->solicitud_user_cerrado = request()->id_usuario;
            $solicitud->solicitud_fecha_cerrado = now();
            $solicitud->solicitud_estado = 4;

            $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha marcado como cerrada la solicitud '.$solicitud->solicitud_numero;
            $auditoria->auditoria_estado = 8;
            $mensaje = "Se Cerro la Solicitud ".$solicitud->solicitud_numero.' exitosamente.';
          }
        }
        $solicitud->save();

        $auditoria->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","titulo"=>"¡Éxito!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function descartSolicitud(){
      try {
        $user = User::find(request()->id_usuario);
        $solicitud = Ticket::find(request()->id_solicitud);
        $solicitud->solicitud_user_descarte = request()->id_usuario;
        $solicitud->solicitud_fecha_descarte = now();
        $solicitud->solicitud_estado = 5;
        $solicitud->save();

        $auditoria = new Auditoria();
        $auditoria->id_solicitud = $solicitud->id_solicitud;
        $auditoria->id_usuario = request()->id_usuario;
        $auditoria->auditoria_date = date('Y-m-d');
        $auditoria->auditoria_fecha = now();
        $auditoria->auditoria_descripcion = 'El usuario '.$user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom.' ha descartado la solicitud '.$solicitud->solicitud_numero;
        $auditoria->auditoria_estado = 9;
        $auditoria->save();

        return array("error"=>0,"mensaje"=>"Se Descartó exitosamente la Solicitud ".$solicitud->solicitud_numero,"tipo"=>"success","titulo"=>"¡Éxito!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

}
