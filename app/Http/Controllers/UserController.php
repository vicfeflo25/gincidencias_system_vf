<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Rol;
use App\Models\Cargo;
use App\Models\Nivel;

class UserController extends Controller{

  public function __construct(){
    $this->middleware('auth');
  }

  public function prueba(){
    return request()->all();
  }

  public function getListUsers(){
    $list_user = User::getList();
    return view('setting.user.listUser',compact('list_user'));
  }

  public function newUser(){
    $list_rol = Rol::where('rol_estado',1)->get();
    $list_cargo = Cargo::where('cargo_estado',1)->get();
    $nivel = Nivel::where('nivel_estado',1)
                  ->orderBy('nivel_descripcion','asc')
                  ->get();
    return view('setting.user.newUser',compact('list_rol','list_cargo','nivel'));
  }

  public function viewUser(){
    $user = User::getUserView();
    return view('setting.user.viewUser',compact('user'));
  }

  public function editUser(){
    $user = User::getUserView();
    return view('setting.user.editUser',compact('user'));
  }

  public function viewInfo(){
    $user = User::getProfile();
    return view('profile',compact('user'));
  }


}
