<form id="editPrioridad">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Prioridad: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)" class="form-control input-sm req" id="prioridad_name" value="{{ $prioridad->prioridad_name }}" name="prioridad_name" />
        <input type="hidden" maxlength="150" class="form-control req"  id="prioridad_user_modificate" name="prioridad_user_modificate" value="{{ Auth::user()->id_usuario }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="id_prioridad" name="id_prioridad" value="{{ $prioridad->id_prioridad }}">
      </div>
    </div>
  </div>
</form>
