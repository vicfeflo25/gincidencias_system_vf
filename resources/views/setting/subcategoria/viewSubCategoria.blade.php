<form id="viewSubCategoria">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Categoria: <span class="text-danger">*</span></label>
        <select class="form-control req"  id="id_categoria" disabled name="id_categoria">
          <option value="0">SELECCIONE</option>
          @foreach ($categoria as $key => $value)
            <option value="{{ $value->id_categoria }}" {{ ($subcategoria->id_categoria == $value->id_categoria)?'selected':'' }}>{{ $value->categoria_name }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Sub Categoria: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)" disabled class="form-control input-sm req" id="sc_name" value="{{ $subcategoria->sc_name }}" name="sc_name">
        <input type="hidden" maxlength="150" class="form-control req"  id="sc_modificate_user" name="sc_modificate_user" value="{{ Auth::user()->id_usuario }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Nivel: <span class="text-danger">*</span></label>
        <select type="text" class="form-control input-sm req" disabled id="id_nivel" name="id_nivel">
          <option value="0">SELECCIONE</option>
          @foreach ($nivel as $key => $value)
            <option value="{{ $value->id_nivel }}" {{ ($subcategoria->id_nivel == $value->id_nivel)?'selected':'' }}>{{ $value->nivel_descripcion }}</option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
</form>
