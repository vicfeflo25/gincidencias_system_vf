<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class Tipo extends Model{

    protected $connection = 'mysql';
    protected $table = 'sgi_tipo';
    protected $primaryKey = 'id_tipo';
    public $timestamps = false;

    public static function getTipos(){
      $list_tipos = Tipo::where('tipo_estado',1)->orderBy('id_tipo','asc')->get();
      return $list_tipos;
    }

}
