<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cargo;

class JobController extends Controller{

  public function __construct(){
      $this->middleware('auth');
  }

  public function listJob(){
    $cargo = Cargo::getListCargo();
    return view('setting.cargo.listCargo',compact('cargo'));
  }

  public function jobNew(){
    return view('setting.cargo.newCargo');
  }

  public function viewJob(){
    $cargo = Cargo::getCargo();
    return view('setting.cargo.viewCargo',compact('cargo'));
  }

  public function editJob(){
    $cargo = Cargo::getCargo();
    return view('setting.cargo.editCargo',compact('cargo'));
  }

}
