<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\Models\UsuarioRol;
use App\Models\Rol;
use App\Models\Cargo;
use App\Models\ConfUsuario;
use App\Models\Nivel;

class User extends Authenticatable{

    use Notifiable;

    protected $connection = 'mysql';
    protected $table = 'usuario_aplicacion';
    protected $primaryKey = 'id_usuario';
    public $timestamps = false;

    protected $fillable = [
        'usu_nombre','usu_apellidop','usu_apellidom','usuario', 'password','usu_foto_perfil','password'
    ];

    protected $hidden = [
      'remember_token'
    ];

    protected $casts = [
        'usu_fecha_validacion' => 'datetime',
        'usu_fecha_modificacion' => 'datetime',
        'usu_fecha_creacion' => 'datetime',
    ];

    public function getpass(){
      return "$this->password";
    }

    public function rol(){
       return $this->hasMany(UsuarioRol::class,'id_usuario');
     }

    public function getConf(){
      $conf = ConfUsuario::where('id_usuario',$this->id_usuario)->first();
      return $conf;
    }

    public function getFullnameAttribute(){
      return "{$this->usu_nombre} {$this->usu_apellidop} {$this->usu_apellidom}";
    }

    public function getSystemTheme(){
      return "{$this->system_dark}";
    }

    public static function getProfile(){
      $user = User::join('usuario_rol as ur','ur.id_usuario','usuario_aplicacion.id_usuario')
                  ->join('rol_aplicacion as ra','ra.id_rol','ur.id_rol')
                  ->join('conf_usuario as cu','cu.id_usuario','usuario_aplicacion.id_usuario')
                  ->join('cargo as c','c.id_cargo','usuario_aplicacion.id_cargo')
                  ->where('usuario_aplicacion.id_usuario',request()->id_usuario)
                  ->first();

      return $user;
    }

    public static function getList(){
      $user = User::select('usuario_aplicacion.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador','ur.*','ra.*','c.cargo_nombre','n.nivel_descripcion')
                     ->join('usuario_aplicacion as u','u.id_usuario','usuario_aplicacion.usu_usuario_creacion')
                     ->join('usuario_rol as ur','ur.id_usuario','usuario_aplicacion.id_usuario')
                     ->join('rol_aplicacion as ra','ra.id_rol','ur.id_rol')
                     ->join('cargo as c','c.id_cargo','usuario_aplicacion.id_cargo')
                     ->leftJoin('sgi_nivel as n','n.id_nivel','usuario_aplicacion.id_nivel')
                     ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','usuario_aplicacion.usu_usuario_modificacion')
                     ->get();

      foreach($user as $key => $value){
        if(empty($value->nivel_descripcion)){
          $value->nivel_descripcion = '-';
        }

        $value->nombre_usuario = $value->usu_nombre.' '.$value->usu_apellidop.' '.$value->usu_apellidom;
        if($value->usu_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else if($value->usu_estado == 0){
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }
        if(empty($value->usu_usuario_modificacion)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->usu_fecha_creacion));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->usu_fecha_creacion));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->usu_fecha_modificacion));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->usu_fecha_modificacion));
        }
      }

      return $user;
    }

    public static function userSave(){
      try {
        $user_existe = User::where('usu_nombre',ucwords(request()->usu_nombre))
                            ->where('usu_apellidop',ucwords(request()->usu_apellidop))
                            ->where('usu_apellidom',ucwords(request()->usu_apellidom))
                            ->first();

        if(empty($user_existe)){
          $rol_ap = Rol::where('id_rol',request()->id_rol)->first();

          $user = new User();
          $user->usu_nombre = trim(ucwords(request()->usu_nombre));
          $user->usu_apellidop = trim(ucwords(request()->usu_apellidop));
          $user->usu_apellidom = trim(ucwords(request()->usu_apellidom));
          $user->id_cargo = request()->id_cargo;
          if(request()->id_rol == env('ID_SOPORTE')){
            $user->id_nivel = request()->id_nivel;
          }else{
            $user->id_nivel = null;
          }
          $user->usu_fechaN = request()->usu_fechaN;

          $usuario = strtoupper(trim(request()->usu_nombre[0]).' '.trim(request()->usu_apellidop));
          $user_existe = User::where('usuario',$usuario)->first();
          if(empty($user_existe) && trim(ucwords(request()->usu_nombre)) != 'ADMIN'){
            $usuario = str_replace(" ","",trim($usuario));
          }else if(trim(ucwords(request()->usu_nombre)) == 'ADMIN'){
            $usuario = str_replace(" ","",trim('ADMIN'));
          }else{
            $usuario = str_replace(" ","",trim($usuario)).date("d", strtotime(request()->usu_fechaN));
          }

          $user->usuario = $usuario;
          $user->password = Hash::make('123456');
          $user->usu_email = request()->usu_email;
          $user->usu_usuario_creacion = request()->usu_usuario_creacion;
          $user->usu_fecha_creacion = now();
          $user->usu_estado = 1;
          $user->usu_foto_perfil = $rol_ap->rol_icono;
          $user->save();

          $conf = new ConfUsuario();
          $conf->id_usuario = $user->id_usuario;
          $conf->system_theme = 'light';
          $conf->navbar_theme = 'navbar-white navbar-light';
          $conf->letra_theme = 'dark';
          $conf->system_theme_fecha = now();
          $conf->status_change_password = 0;
          $conf->fecha_change_password = now();
          $conf->save();

          $nombre = $user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom;

          $rol = new UsuarioRol();
          $rol->id_usuario = $user->id_usuario;
          $rol->id_rol = request()->id_rol;
          $rol->ur_usuario_creacion = request()->usu_usuario_creacion;
          $rol->ur_fecha_creacion = now();
          $rol->ur_estado = 1;
          $rol->save();

          return array("error"=>0,"mensaje"=>"Se registró exitosamente el Usuario ".$nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El usuario que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function getUserView(){
      $user = new User();
      $user->usuario = User::join('usuario_rol as r','r.id_usuario','usuario_aplicacion.id_usuario')
                            ->join('cargo as c','c.id_cargo','usuario_aplicacion.id_cargo')
                            ->where('usuario_aplicacion.id_usuario',request()->id_usuario)
                            ->first();
      $user->list_rol = Rol::where('rol_estado',1)->get();
      $user->list_cargo = Cargo::where('cargo_estado',1)->get();
      $user->nivel = Nivel::where('nivel_estado',1)
                    ->orderBy('nivel_descripcion','asc')
                    ->get();
      return $user;
    }

    public static function userUpdate(){
      try {
        $usuario_existe = User::where('usu_nombre',ucwords(request()->usu_nombre))
                            ->where('usu_apellidop',ucwords(request()->usu_apellidop))
                            ->where('usu_apellidom',ucwords(request()->usu_apellidom))
                            ->where('id_usuario','!=',request()->id_usuario)
                            ->first();

        if(empty($usuario_existe)){
          $rol_ap = Rol::where('id_rol',request()->id_rol)->first();

          $user = User::where('id_usuario',request()->id_usuario)->first();
          $user->usu_nombre = trim(ucwords(request()->usu_nombre));
          $user->usu_apellidop = trim(ucwords(request()->usu_apellidop));
          $user->usu_apellidom = trim(ucwords(request()->usu_apellidom));
          $user->usu_fechaN = request()->usu_fechaN;
          $user->id_cargo = request()->id_cargo;
          if(request()->id_rol == env('ID_SOPORTE')){
            $user->id_nivel = request()->id_nivel;
          }else{
            $user->id_nivel = null;
          }

          $usuario = strtoupper(trim(request()->usu_nombre[0]).' '.trim(request()->usu_apellidop));
          $user_existe = User::where('usuario',$usuario)
                            ->where('id_usuario','!=',request()->id_usuario)
                            ->first();

          if(empty($user_existe) && trim(ucwords(request()->usu_nombre)) != 'ADMIN'){
            $usuario = str_replace(" ","",trim($usuario));
          }else if(trim(ucwords(request()->usu_nombre)) == 'ADMIN'){
            $usuario = 'ADMIN';
          }else{
            $usuario = str_replace(" ","",trim($usuario)).date("d", strtotime(request()->usu_fechaN));
          }

          $user->usuario = $usuario;
          $user->password = Hash::make('123456');
          $user->usu_email = request()->usu_email;
          $user->usu_usuario_modificacion = request()->usu_usuario_modificacion;
          $user->usu_fecha_modificacion = now();
          $user->usu_estado = 1;
          $user->usu_foto_perfil = $rol_ap->rol_icono;
          $user->save();

          $nombre = $user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom;

          $rol = UsuarioRol::where('id_usuario',request()->id_usuario)->first();
          $rol->id_usuario = $user->id_usuario;
          $rol->id_rol = request()->id_rol;
          $rol->ur_usuario_creacion = request()->usu_usuario_creacion;
          $rol->ur_fecha_creacion = now();
          $rol->ur_estado = 1;
          $rol->save();

          return array("error"=>0,"mensaje"=>"Se actualizó exitosamente el Usuario ".$nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El usuario que intenta modificar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeUser(){
      try {
        $user = User::find(request()->id_usuario);
        $nombre = $user->usu_nombre.' '.$user->usu_apellidop.' '.$user->usu_apellidom;
        if($user->usu_estado == 1){
          $user->usu_estado = 0;
          $mensaje = 'Se inactivo exitosamente el Usuario '.$nombre;
        }else{
          $user->usu_estado = 1;
          $mensaje = 'Se activo exitosamente el Usuario '.$nombre;
        }
        $user->usu_usuario_modificacion = request()->id_user;
        $user->usu_fecha_modificacion = now();
        $user->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeTheme(){
      try {
        $conf = ConfUsuario::where('id_usuario',request()->id_usuario)->first();

        if(request()->theme == 'dark'){
          $system = 'light';
          $navbar = 'navbar-white navbar-light';
          $body = '';
          $letra = 'text-dark';
          $border = '';
          $footer = '';
          $mensaje = 'Modo Nocturno desactivado';
        }else{
          $system = 'dark';
          $navbar = 'navbar-dark';
          $body = 'bg-dark';
          $letra = 'text-white';
          $border = 'border';
          $footer = 'bg-dark';
          $mensaje = 'Modo Nocturno activado';
        }

        $conf->system_theme = $system;
        $conf->navbar_theme = $navbar;
        $conf->body_theme = $body;
        $conf->letra_theme = $letra;
        $conf->border_theme = $border;
        $conf->footer_theme = $footer;
        $conf->system_theme_fecha = now();
        $conf->save();

        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changePassword(){
      try {
        $user = User::find(request()->id_usuario);
        if(request()->usu_password_new == request()->usu_password_repeat){
          $user->password = Hash::make(request()->usu_password_new);
          $user->save();
          return array("error"=>0,"mensaje"=>"Se modificó exitosamente la contraseña.","tipo"=>"success","title"=>"¡Bien!");
        }else{
            return array("error"=>1,"mensaje"=>"Las contraseñas ingresadas no coinciden","tipo"=>"error","titulo"=>"Alerta");
        }

      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function restaurarPassword(){
      try {
        $user = User::find(request()->id_usuario);
        $user->password = Hash::make('123456');
        $user->save();
        return array("error"=>0,"mensaje"=>"Se modificó exitosamente la contraseña.","tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }
}
