<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class Comentario extends Model{

    protected $connection = 'mysql';
    protected $table = 'sgi_solicitud_comentario';
    protected $primaryKey = 'id_comentario';
    public $timestamps = false;


    protected $fillable = [
        'id_comentario','id_solicitud','comentario_descripcion','comentario_tipo', 'comentario_estado','comentario_user_create','comentario_fecha_create'
    ];

}
