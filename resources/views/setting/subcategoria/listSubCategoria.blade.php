@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endsection

@section('content')
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 {{ Auth::user()->getConf()->letra_theme }}">Listado de Sub Categorias</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Configuración</a></li>
            <li class="breadcrumb-item active">Sub Categorias</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- Default box -->
        <div class="col-md-12">
          <div class="card {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-header {{ Auth::user()->getConf()->border_theme }}">
              <h3 class="card-title">Sub Categorias</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col text-right">
                  <a class="btn btn-success btn-sm text-white mb-3" id="btnAddSubCategoria" data-toggle="modal" data-target="#modalSubCategoria"><i class="fas fa-plus"></i> Agregar</a>
                </div>
              </div>
              <table class="table table-striped projects display nowrap" id="tblSubCategoria" width="100%">
                <thead>
                  <tr>
                    <th style="width: 1%">#</th>
                    <th style="width: 30%">Categoria</th>
                    <th style="width: 30%">Sub Categoria</th>
                    <th style="width: 10%">Nivel</th>
                    <th style="width: 10%" class="text-center">Estado</th>
                    <th style="width: 20%" class="text-right">Auditoria</th>
                    <th style="width: 20%"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($subcategoria->subcategoria as $key => $value)
                    <tr>
                      <td>#</td>
                      <td><b>{{ $value->categoria_name }}</b></td>
                      <td><b>{{ $value->sc_name }}</b></td>
                      <td><b>{{ $value->nivel_descripcion }}</b></td>
                      <td class="text-center"><span class="badge badge-{{ $value->color_estado }}">{{ $value->nombre_estado }}</span></td>
                      <td class="text-right">
                        <small>
                          <b>{{ $value->fecha_auditoria }}</b>
                          <b>{{ $value->hora_auditoria }}</b>
                          <br>
                          {{ $value->nombre_auditoria }}
                        </small>
                      </td>
                      <td class="project-actions text-right">
                        <a class="btn btn-primary btn-sm text-white btn-visualize" title="Visualizar" data-id="{{ $value->id_subcategoria }}">
                          <i class="fas fa-eye"></i>
                        </a>
                        <a class="btn btn-info btn-sm text-white btn-edit" title="Editar" data-id="{{ $value->id_subcategoria }}">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a class="btn btn-{{ ($value->sc_estado == 0)?'success':'danger' }}  btn-sm text-white btn-change" title="{{ ($value->sc_estado == 0)?'Activar':'Inactivar' }}" data-id="{{ $value->id_subcategoria }}" data-user="{{ Auth::user()->id_usuario }}">
                          <i class="fas fa-exchange-alt"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
  </section>

  <div class="modal fade" id="modalSubCategoria" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="modalSubCategoriaTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content {{ Auth::user()->getConf()->body_theme }}">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <a type="button" class="btn btn-primary text-white">Guardar</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      $('#tblSubCategoria').DataTable({
        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 1 },
            { responsivePriority: 2, targets: 2 }
        ]
      });

      $("table.dataTable").on('click','.btn-visualize',function(){
        var id_subcategoria = $(this).data('id')
        $.ajax({
          url: '{{ route('viewSubCategoria') }}',
          method: "POST",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_subcategoria":id_subcategoria},
          success: function(respuesta) {
            $(".modal#modalSubCategoria .modal-title").html('VISUALIZADOR DE SUB CATEGORIA')
            $(".modal#modalSubCategoria .btn-primary").attr('hidden','hidden')
            $(".modal#modalSubCategoria").modal('show')
            $(".modal#modalSubCategoria .modal-body").html(respuesta)
          }
        });
      })

      $("table.dataTable").on('click','.btn-change',function(){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        var id_subcategoria = $(this).data('id')
        var id_user = $(this).data('user')
        $.ajax({
          url: '{{ route('changeSubCategoria') }}',
          method: "PUT",
          headers: {
            'X-CSRF-Token': $('input[name="csrfToken"]').attr('value')
          },
          data: {"id_subcategoria":id_subcategoria,"id_user":id_user},
          success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
          }
        });
      })

      $("table.dataTable").on('click','.btn-edit',function(){
        $(".modal#modalSubCategoria .btn-primary").removeAttr('hidden')
        var id_subcategoria = $(this).data('id')
        $.ajax({
          url: '{{ route('editSubCategoria') }}',
          method: "POST",
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: {"id_subcategoria":id_subcategoria},
          success: function(respuesta) {
            $(".modal#modalSubCategoria .modal-title").html('EDITAR SUB CATEGORIA')
            $(".modal#modalSubCategoria .btn-primary").attr('data-tipo','2')
            $(".modal#modalSubCategoria").modal('show')
            $(".modal#modalSubCategoria .modal-body").html(respuesta)
          }
        });
      })

    });

    $(function () {
      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $('a img.img-fluid').removeClass('border border-dark')
        $(this).children().addClass('border border-dark')
        $("#rol_icono").val($(this).attr('href'))
      });
    })

    $("#btnAddSubCategoria").on('click',function(){
      $(".modal#modalSubCategoria .btn-primary").removeAttr('hidden')
      $(".modal#modalSubCategoria .modal-title").html('AGREGAR NUEVA SUB CATEGORIA')
      $(".modal#modalSubCategoria .btn-primary").attr('data-tipo','1')
      $.ajax({
      	url: '{{ route('newSubCategoria') }}',
        method: "GET",
      	success: function(respuesta) {
      		$(".modal#modalSubCategoria .modal-body").html(respuesta)
      	}
      });
    })

    $(".modal#modalSubCategoria .btn-primary").on('click',function(){
      var count = 0;
      var url = '';
      var data = '';
      if($(this).data('tipo') == 1){
        url = '{{ route('saveSubCategoria') }}';
        data = $("form#addSubCategoria").serialize();
        if($("form#addSubCategoria #sc_name").val() == null || $("form#addSubCategoria #sc_name").val() == ''){
          $("form#addSubCategoria #sc_name").addClass('is-invalid');
          count++;
        }else{
          $("form#addSubCategoria #sc_name").removeClass('is-invalid');
        }
        if($("form#addSubCategoria #id_categoria").val() == null || $("form#addSubCategoria #id_categoria").val() == 0){
          $("form#addSubCategoria #id_categoria").addClass('is-invalid');
          count++;
        }else{
          $("form#addSubCategoria #id_categoria").removeClass('is-invalid');
        }
      }else{
        url = '{{ route('updateSubCategoria') }}';
        data = $("form#editSubCategoria").serialize();
        if($("form#editSubCategoria #sc_name").val() == null || $("form#editSubCategoria #sc_name").val() == ''){
          $("form#editSubCategoria #sc_name").addClass('is-invalid');
          count++;
        }else{
          $("form#editSubCategoria #sc_name").removeClass('is-invalid');
        }
        if($("form#editSubCategoria #id_categoria").val() == null || $("form#editCategoria #id_categoria").val() == 0){
          $("form#editSubCategoria #id_categoria").addClass('is-invalid');
          count++;
        }else{
          $("form#editSubCategoria #id_categoria").removeClass('is-invalid');
        }
      }
      if(count == 0){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        $.ajax({
        	url: url,
          method: "PUT",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: data,
        	success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
        	}
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar todos los campos obligatorios para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })


  </script>
@endsection
