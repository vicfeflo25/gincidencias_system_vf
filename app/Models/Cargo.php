<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model{

    protected $connection = 'mysql';
    protected $table = 'cargo';
    protected $primaryKey = 'id_cargo';
    public $timestamps = false;


    public static function getListCargo(){
      $cargo = Cargo::select('cargo.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador')
                      ->join('usuario_aplicacion as u','u.id_usuario','cargo.cargo_user_create')
                      ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','cargo.cargo_user_modificate')
                      ->get();

      foreach ($cargo as $key => $value) {
        if($value->cargo_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else{
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }

        if(empty($value->cargo_user_modificate)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->cargo_fecha_create));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->cargo_fecha_create));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->cargo_fecha_modificate));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->cargo_fecha_modificate));
        }
      }
      return $cargo;
    }

    public static function cargoSave(){
      try {
        $cargo_existe = Cargo::where('cargo_nombre',strtoupper(request()->cargo_nombre))->first();
        if(empty($cargo_existe)){
          $cargo = new Cargo();
          $cargo->cargo_user_create = request()->cargo_user_create;
          $cargo->cargo_fecha_create = now();
          $cargo->cargo_nombre = strtoupper(request()->cargo_nombre);
          $cargo->cargo_estado = 1;
          $cargo->save();
          return array("error"=>0,"mensaje"=>"Se agregó exitosamente el Cargo ".$cargo->cargo_nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El Cargo ".strtoupper(request()->cargo_nombre)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function cargoUpdate(){
      try {
        $cargo_existe = Cargo::where('cargo_nombre',strtoupper(request()->cargo_nombre))->where('id_cargo','!=',request()->id_cargo)->first();
        if(empty($cargo_existe)){
          $cargo = Cargo::find(request()->id_cargo);
          $cargo->cargo_user_modificate = request()->cargo_user_modificate;
          $cargo->cargo_fecha_modificate = now();
          $cargo->cargo_nombre = strtoupper(request()->cargo_nombre);
          $cargo->save();
          return array("error"=>0,"mensaje"=>"Se modificó exitosamente el Cargo ".$cargo->cargo_nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El Cargo ".strtoupper(request()->cargo_nombre)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeStateJob(){
      try {
        $cargo = Cargo::find(request()->id_cargo);
        if($cargo->cargo_estado == 1){
          $cargo->cargo_estado = 0;
          $mensaje = 'Se inactivo exitosamente el Cargo '.$cargo->cargo_nombre;
        }else{
          $cargo->cargo_estado = 1;
          $mensaje = 'Se activo exitosamente el Cargo '.$cargo->cargo_nombre;
        }
        $cargo->cargo_user_modificate = request()->id_user;
        $cargo->cargo_fecha_modificate = now();
        $cargo->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function getCargo(){
      $cargo = Cargo::where('id_cargo',request()->id_cargo)->first();
      return $cargo;
    }

}
