<?php

namespace App\Imports;

use App\Models\Ticket;
use Carbon;
use App\Models\Comentario;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;

class TicketImport implements ToCollection,WithHeadingRow{
    /**
     * @param array $row
     *
     * @return User|null
     */

    public function collection(Collection $rows)
    {
      foreach ($rows as $row) {
        if($row['id_tipo'] != null){
          Ticket::create([
              'id_solicitud'   => $row['id_solicitud'],
              'id_tipo'        => $row['id_tipo'],
              'id_prioridad'   => $row['id_prioridad'],
              'id_area'        => $row['id_area'],
              'id_categoria'   => $row['id_categoria'],
              'id_subcategoria' => $row['id_subcategoria'],
              'solicitud_tema' => mb_strtoupper($row['solicitud_tema']),
              'solicitud_codigo' => $row['solicitud_codigo'],
              'solicitud_anio' => $row['solicitud_anio'],
              'solicitud_numero' => $row['solicitud_numero'],
              'solicitud_fecha' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['solicitud_fecha']),
              'solicitud_estado' => $row['solicitud_estado'],
              'solicitud_user_create' => $row['solicitud_user_create'],
              'solicitud_fecha_create' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['solicitud_fecha_create']),
              'solicitud_user_modificate' => $row['solicitud_user_modificate'],
              'solicitud_fecha_modificate' => $row['solicitud_fecha_modificate'],
              'solicitud_asigna_user' => $row['solicitud_asigna_user'],
              'solicitud_user_asigna' => $row['solicitud_user_asigna'],
              'solicitud_fecha_asigna' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['solicitud_fecha_asigna']),
              'solicitud_user_soluciona' => $row['solicitud_user_soluciona'],
              'solicitud_fecha_soluciona' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['solicitud_fecha_soluciona']),
              'solicitud_user_cerrado' => $row['solicitud_user_cerrado'],
              'solicitud_fecha_cerrado' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['solicitud_fecha_cerrado']),
              'solicitud_user_descarte' => $row['solicitud_user_descarte'],
              'solicitud_fecha_descarte' => $row['solicitud_fecha_descarte']
          ]);

          Comentario::create([
            'id_comentario'   => $row['id_solicitud'],
            'id_solicitud'   => $row['id_solicitud'],
            'comentario_descripcion'   => $row['comentario_descripcion'],
            'comentario_tipo' => $row['comentario_tipo'],
            'comentario_estado' => $row['comentario_estado'],
            'comentario_user_create' => $row['solicitud_user_create'],
            'comentario_fecha_create' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['solicitud_fecha_create'])
          ]);
        }
      }
    }

}
