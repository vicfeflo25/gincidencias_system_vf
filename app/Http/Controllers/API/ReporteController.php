<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Reporte;

class ReporteController extends Controller{
  //
  // public function __construct(){
  //     $this->middleware('auth');
  // }

  public function getResolucion(){
    $reporte = Reporte::getRRI();
    return view('reports.rri',compact('reporte'));
  }

  public function getResolucionRULI(){
    $reporte = Reporte::getRULI();
    return view('reports.ruli',compact('reporte'));
  }

  public function getResolucionINCIDENCIA(){
    $reporte = Reporte::getIncidencias();
    return view('reports.reportincidencia',compact('reporte'));
  }



}
