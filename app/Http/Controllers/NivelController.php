<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nivel;

class NivelController extends Controller{

  public function __construct(){
      $this->middleware('auth');
  }

  public function listNivel(){
    $nivel = Nivel::getListNivel();
    return view('setting.nivel.listNivel',compact('nivel'));
  }

  public function nivelNew(){
    return view('setting.nivel.newNivel');
  }

  public function viewNivel(){
    $nivel = Nivel::getNivel();
    return view('setting.nivel.viewNivel',compact('nivel'));
  }

  public function editNivel(){
    $nivel = Nivel::getNivel();
    return view('setting.nivel.editOldNivel',compact('nivel'));
  }

}
