@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="{{ Auth::user()->getConf()->letra_theme }}">Perfil</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Perfil del Usuario</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{ $user->usu_foto_perfil }}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ trim(Auth::user()->getFullnameAttribute()) }}</h3>

              <p class="text-muted text-center">{{ $user->rol_nombre }}</p>
              <hr>
              @if(!empty($user->usu_apellidop))
              <strong> APELLIDO PATERNO:</strong>
              <p> {{ $user->usu_apellidop }}</p>
              @endif
              @if(!empty($user->usu_apellidom))
              <strong> APELLIDO MATERNO:</strong>
              <p> {{ $user->usu_apellidom }}</p>
              @endif
              <strong> NOMBRES:</strong>
              <p> {{ $user->usu_nombre }}</p>
              <hr>
              <strong> FECHA NACIMIENTO:</strong>
              <p><i class="fas fa-calendar-alt mr-1"></i> {{ $user->usu_fechaN }}</p>
              <strong> USUARIO:</strong>
              <p> {{ $user->usuario }}</p>
              <strong> E-MAIL:</strong>
              <p> {{ $user->usu_email }}</p>
              <strong> CARGO:</strong>
              <p> {{ strtoupper($user->cargo_nombre) }}</p>
              <strong> PERFIL:</strong>
              <p> {{ $user->rol_nombre }}</p>
              <hr>
              <strong> TEMA:</strong>
              <p> {{ strtoupper($user->system_theme) }}</p>
              <button class="switch {{ ($user->system_theme != 'dark')?'active':'' }}" data-id="{{ $user->id_usuario }}" data-theme="{{ $user->system_theme }}" id="switch">
      					<span><i class="fas fa-sun"></i></span>
      					<span><i class="fas fa-moon"></i></span>
      				</button>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Configuración</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body ">
              <div class="tab-content"><div class="active tab-pane" id="settings">
                  <form class="form-horizontal" id="formPass">
                    <input type="hidden" name="id_usuario" id="id_usuario" value="{{ Auth::user()->id_usuario }}">
                    <div class="form-group row">
                      <label for="usu_password_new" class="col-sm-2 col-form-label">Contraseña Nueva</label>
                      <div class="col-sm-4">
                        <div class="input-group mb-3">
                          <input type="password" class="form-control req" id="usu_password_new" name="usu_password_new" placeholder="Escribir contraseña nueva">
                          <div class="input-group-append">
                            <div class="input-group-text" style="background-color: white;border: none;">
                              <a class="fas fa-eye mostrar mdi-eye"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="usu_password_repeat" class="col-sm-2 col-form-label">Contraseña Nueva</label>
                      <div class="col-sm-4">
                        <div class="input-group mb-3">
                          <input type="password" class="form-control req" id="usu_password_repeat" name="usu_password_repeat" placeholder="Escribir nuevamente contraseña nueva">
                          <div class="input-group-append">
                            <div class="input-group-text" style="background-color: white;border: none;">
                              <a class="fas fa-eye mostrar mdi-eye"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-10">
                        <a class="btn btn-danger text-white" id="restore">Cambiar Contraseña</a>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection

@section('script')
<script type="text/javascript">
$(document).ready( function () {
  $(".preloader-wrapper").fadeOut(500, 'swing');
  //var subcategorias = $.parseJSON($("#subcategorias").val());
});

const btnSwitch = document.querySelector('#switch');
$("#switch").on('click',function(){
  btnSwitch.classList.toggle('active');
  $.ajax({
    url: '{{ route('changeTheme') }}',
    method: "PUT",
    headers: {
      'X-CSRF-Token': $('input[name="csrfToken"]').attr('value')
    },
    data: {"id_usuario":$(this).data('id'),"theme":$(this).data('theme')},
    success: function(respuesta) {
      Swal.fire({
        icon: respuesta.tipo,
        title: respuesta.titulo,
        text: respuesta.mensaje,
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then((result) => {
        if (result.isConfirmed) {
          location.reload();
        }
      });
    }
  });
})

$('.mostrar').click(function(){
    //Comprobamos que la cadena NO esté vacía.
    if($(this).hasClass('mdi-eye') && ($(this).parent().parent().parent().children()[0].value != "")){
      $(this).parent().parent().parent().find('input').removeAttr('type');
      $(this).addClass('mdi-eye-off').removeClass('mdi-eye');
    }else{
      $(this).parent().parent().parent().find('input').attr('type','password');
      $(this).addClass('mdi-eye').removeClass('mdi-eye-off');
    }
});

$("#restore").on('click',function(){
  var count = 0;
  $.each($("input.req"),function(k,v){
    if($(this).val() == '' || $(this).val() == null){
      count++;
      $(this).addClass('is-invalid');
    }else{
      $(this).removeClass('is-invalid');
    }
  })
  if(count == 0){
    $(".preloader-wrapper").fadeIn(500, 'swing');
    $.ajax({
      url: 'api/changePassword',
      method: "PUT",
      headers: {
        'X-CSRF-Token': $('input[name="csrfToken"]').attr('value')
      },
      data: $("#formPass").serialize(),
      success: function(respuesta) {
        $(".preloader-wrapper").fadeOut(500, 'swing');
        Swal.fire({
          icon: respuesta.tipo,
          title: respuesta.titulo,
          text: respuesta.mensaje,
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        }).then((result) => {
          if (result.isConfirmed) {
            if(respuesta.error == 0){
              location.reload();
            }
          }
        });
      }
    });
  }else{
    Swal.fire({
      icon: 'warning',
      title: '¡Oops!',
      text: '¡Debe completar todos los campos obligatorios para continuar!',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    });
  }
})
</script>
@endsection
