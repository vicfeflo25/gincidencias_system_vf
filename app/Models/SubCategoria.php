<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategoria extends Model{

    protected $connection = 'mysql';
    protected $table = 'subcategoria';
    protected $primaryKey = 'id_subcategoria';
    public $timestamps = false;

    public static function getSCActivate(){
      $subcategorias = SubCategoria::where('sc_estado',1)
                              ->orderBy('sc_name','asc')
                              ->get();

      return $subcategorias;
    }

}
