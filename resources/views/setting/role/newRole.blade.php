<form id="addRole">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Nombre Perfil: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" id="rol_nombre" name="rol_nombre">
        <input type="hidden" maxlength="150" class="form-control req"  id="rol_user_create" name="rol_user_create" value="{{ Auth::user()->id_usuario }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Foto Perfil por Defecto: <span class="text-danger">*</span></label>
        <div class="row">
          @foreach ($list_iconos as $key => $value)
            <div class="col-sm-2">
              <a {{ ($value->en_uso == 0)?$value->items_href.' '.$value->items_toggle.' '.$value->items_title.' '.$value->items_gallery:''}} >
                <img src="{{ $value->ico_url }}" {{ ($value->en_uso == 0)?'':'style=\"filter:grayscale(100%);-webkit-filter:grayscale(100%);\"' }} class="img-fluid mb-2" alt="white sample"/>
              </a>
            </div>
          @endforeach

        </div>
        <input type="hidden" onkeypress="return /[a-z-ñ]/i.test(event.key)"  class="form-control input-sm req" id="rol_icono" name="rol_icono">
      </div>
    </div>
  </div>
</form>
