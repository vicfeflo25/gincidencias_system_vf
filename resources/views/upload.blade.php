@extends('layouts.index')

@section('content')
  <br>
<section class="content ">
  <div class="container-fluid">
    <form  method="post">
      <div class="row">
        <div class="col-md-12">
          <input type="file" name="ticket" id="ticket">
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <a class="btn btn-sm btn-danger" id="upload">SUBIR</a>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready( function () {
  $(".preloader-wrapper").fadeOut(500, 'swing');
});

$('#upload').on('click', function() {
    var property = document.getElementById('ticket').files[0];
    var form_data = new FormData();
    form_data.append('file', property);
    alert(form_data);
    $.ajax({
        url: '/excel', // point to server-side PHP script
        method:'POST',
        data:form_data,
        contentType:false,
        cache:false,
        processData:false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            alert(data); // display response from the PHP script, if any
        }
     });
});

</script>
@endsection
