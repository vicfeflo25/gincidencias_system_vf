@if(!empty($reporte))
<table class="table table-bordered table-striped display nowrap" id="table1" style="width:100%">
  <thead  style="background-color:#3B9FF5">
    <tr>
      <th>ITEMS</th>
      <th>FECHA DE REGISTRO</th>
      <th>N° INCIDENCIAS RESUELTAS CUMPLIENDO LOS SLA</th>
      <th>N° TOTAL INCIDENCIAS</th>
      <th>RATIO DE RESOLUCIÓN DE INCIDENCIAS</th>
    </tr>
  </thead>
  <tbody>
    @foreach($reporte as $key => $value)
    <tr>
      <td>{{ $key+1 }}</td>
      <td>{{ $value->solicitud_fecha }}</td>
      <td>{{ $value->TOTAL_RESUELTAS }}</td>
      <td>{{ $value->TOTAL_SOLICITADAS }}</td>
      <td>{{ $value->RRI }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@else
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">¡Alerta!</h4>
  <p>No se encontraron registros para las fechas Seleccionadas.</p>
</div>
@endif

<script type="text/javascript">
  $(document).ready( function () {
    $('#table1').DataTable({
      responsive: true,
      columnDefs: [
          { responsivePriority: 1, targets: 1 },
          { responsivePriority: 2, targets: 2 }
      ],
      order: [[ 0, 'asc' ]],
      dom: 'Bfrtip',
      buttons: [
        { extend: 'pdf', text:  'Exportar a PDF' },
        { extend: 'excel', text: 'Exportar a EXCEL' },
        'pageLength'
      ],
      lengthMenu: [
        [ 10, 25, 50, -1 ],
        [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar todo' ]
      ],

    });
  });
</script>
