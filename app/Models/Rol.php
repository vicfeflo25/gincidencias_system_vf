<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Rol extends Model{

    protected $connection = 'mysql';
    protected $table = 'rol_aplicacion';
    protected $primaryKey = 'id_rol';
    public $timestamps = false;

    protected $fillable = [
        'rol_nombre'
    ];

    public static function getListRol(){
      $list_rol = Rol::select('rol_aplicacion.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador')
                     ->join('usuario_aplicacion as u','u.id_usuario','rol_aplicacion.rol_user_create')
                     ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','rol_aplicacion.rol_user_update')
                     ->get();

      foreach($list_rol as $key => $value){
        if($value->rol_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else{
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }

        if(empty($value->rol_user_update)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->rol_fecha_create));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->rol_fecha_create));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->rol_fecha_update));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->rol_fecha_update));
        }

      }
      return $list_rol;
    }

    public static function roleSave(){
      try {
        $rol_existe = Rol::where('rol_nombre',strtoupper(request()->rol_nombre))->first();
        if(empty($rol_existe)){
          $rol = new Rol();
          $rol->rol_user_create = request()->rol_user_create;
          $rol->rol_fecha_create = now();
          $rol->rol_nombre = strtoupper(request()->rol_nombre);
          $rol->rol_icono = request()->rol_icono;
          $rol->rol_estado = 1;
          $rol->save();
          return array("error"=>0,"mensaje"=>"Se agregó exitosamente el Perfil ".$rol->rol_nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El Perfil ".strtoupper(request()->rol_nombre)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function updateRole(){
      try {
        $rol_existe = Rol::where('rol_nombre',strtoupper(request()->rol_nombre))->where('id_rol','!=',request()->id_rol)->first();
        if(empty($rol_existe)){
          $rol = Rol::find(request()->id_rol);
          $rol->rol_user_update = request()->rol_user_create;
          $rol->rol_fecha_update = now();
          $rol->rol_nombre = strtoupper(request()->rol_nombre);
          $rol->rol_icono = request()->rol_icono;
          $rol->rol_estado = 1;
          $rol->save();

          $usuarios = User::where('id_rol',request()->id_rol)
                          ->join('usuario_rol as r','r.id_usuario','usuario_aplicacion.id_usuario')
                          ->update(array('usu_foto_perfil' => request()->rol_icono));

          return array("error"=>0,"mensaje"=>"Se modificó exitosamente el Perfil ".$rol->rol_nombre,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"El nombre del Perfil ".strtoupper(request()->rol_nombre)." que intenta modificar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeRole(){
      try {
        $rol = Rol::find(request()->id_rol);
        if($rol->rol_estado == 1){
          $rol->rol_estado = 0;
          $mensaje = 'Se inactivo exitosamente el Perfil '.$rol->rol_nombre;
        }else{
          $rol->rol_estado = 1;
          $mensaje = 'Se activo exitosamente el Perfil '.$rol->rol_nombre;
        }
        $rol->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function visualizeRol(){
      $rol = Rol::where('id_rol',request()->id_rol)->first();
      return $rol;
    }

    public static function viewRole(){
      $rol = new Rol();
      $rol->rol = Rol::where('id_rol',request()->id_rol)->first();
      $rol->iconos = Icono::getListIconoUnico($rol->rol->rol_icono);
      return $rol;
    }
}
