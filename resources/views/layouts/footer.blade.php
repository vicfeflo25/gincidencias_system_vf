<!-- Main Footer -->
<footer class="main-footer {{ Auth::user()->getConf()->footer_theme }}">
  <strong>Copyright &copy; 2020 <a href="#">Sistema de Gestión de Incidencias</a>.</strong>
  Todos los derechos reservados.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 1.0.0
  </div>
</footer>
