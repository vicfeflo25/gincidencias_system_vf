/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `gestion_sys_incidencias` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `gestion_sys_incidencias`;

CREATE TABLE IF NOT EXISTS `cargo` (
  `id_cargo` bigint(20) NOT NULL AUTO_INCREMENT,
  `cargo_nombre` varchar(250) DEFAULT NULL,
  `cargo_estado` int(11) DEFAULT NULL,
  `cargo_user_create` int(11) DEFAULT NULL,
  `cargo_fecha_create` datetime DEFAULT NULL,
  `cargo_user_modificate` int(11) DEFAULT NULL,
  `cargo_fecha_modificate` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cargo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

DELETE FROM `cargo`;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` (`id_cargo`, `cargo_nombre`, `cargo_estado`, `cargo_user_create`, `cargo_fecha_create`, `cargo_user_modificate`, `cargo_fecha_modificate`) VALUES
	(1, 'PROFESOR', 1, 1, '2020-10-07 03:58:31', NULL, '2020-10-07 04:33:44'),
	(2, 'COORDINADOR', 1, 1, '2020-10-07 04:00:02', NULL, NULL),
	(3, 'JEFE OTI', 1, 1, '2020-10-07 04:00:23', 1, '2020-10-10 16:23:19'),
	(4, 'PRUEBA CARGOA', 1, 1, '2020-10-10 16:13:59', 1, '2020-10-10 16:15:51');
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `categoria_name` varchar(250) DEFAULT NULL,
  `categoria_estado` int(11) DEFAULT NULL,
  `categoria_user_create` int(11) DEFAULT NULL,
  `categoria_fecha_create` datetime DEFAULT NULL,
  `categoria_user_modificate` int(11) DEFAULT NULL,
  `categoria_fecha_modificate` datetime DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

DELETE FROM `categoria`;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`id_categoria`, `categoria_name`, `categoria_estado`, `categoria_user_create`, `categoria_fecha_create`, `categoria_user_modificate`, `categoria_fecha_modificate`) VALUES
	(1, 'SOFTWARE', 1, 1, '2020-10-09 05:04:21', 1, '2020-10-09 05:24:48'),
	(2, 'HARDWARE', 1, 1, '2020-10-09 05:05:23', NULL, NULL),
	(3, 'SERVICIO', 1, 1, '2020-10-09 05:05:36', 1, '2020-10-09 05:27:43');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `conf_usuario` (
  `id_configuracion` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuario` bigint(20) NOT NULL,
  `system_theme` varchar(250) DEFAULT NULL,
  `system_theme_fecha` datetime DEFAULT NULL,
  `navbar_theme` varchar(250) DEFAULT NULL,
  `body_theme` varchar(250) DEFAULT NULL,
  `letra_theme` varchar(250) DEFAULT NULL,
  `border_theme` varchar(250) DEFAULT NULL,
  `footer_theme` varchar(250) DEFAULT NULL,
  `status_change_password` int(11) DEFAULT NULL,
  `fecha_change_password` datetime DEFAULT NULL,
  PRIMARY KEY (`id_configuracion`),
  KEY `FK_usuario` (`id_usuario`),
  CONSTRAINT `FK_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario_aplicacion` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

DELETE FROM `conf_usuario`;
/*!40000 ALTER TABLE `conf_usuario` DISABLE KEYS */;
INSERT INTO `conf_usuario` (`id_configuracion`, `id_usuario`, `system_theme`, `system_theme_fecha`, `navbar_theme`, `body_theme`, `letra_theme`, `border_theme`, `footer_theme`, `status_change_password`, `fecha_change_password`) VALUES
	(1, 1, 'light', '2020-10-19 02:19:22', 'navbar-white navbar-light', '', 'text-dark', '', '', 0, '2020-09-28 12:03:31'),
	(2, 3, 'light', '2020-09-30 01:58:03', 'navbar-white navbar-light', '', 'text-dark', '', '', 0, '2020-09-28 12:03:48');
/*!40000 ALTER TABLE `conf_usuario` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `icono` (
  `id_icono` int(11) NOT NULL,
  `ico_categoria` varchar(50) DEFAULT NULL,
  `ico_url` varchar(50) DEFAULT NULL,
  `ico_estado` varchar(50) DEFAULT NULL,
  `ico_user_create` int(11) DEFAULT NULL,
  `ico_fecha_create` datetime DEFAULT NULL,
  `ico_user_modificate` int(11) DEFAULT NULL,
  `ico_fecha_modificate` datetime DEFAULT NULL,
  PRIMARY KEY (`id_icono`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DELETE FROM `icono`;
/*!40000 ALTER TABLE `icono` DISABLE KEYS */;
INSERT INTO `icono` (`id_icono`, `ico_categoria`, `ico_url`, `ico_estado`, `ico_user_create`, `ico_fecha_create`, `ico_user_modificate`, `ico_fecha_modificate`) VALUES
	(1, 'Roles', '/img/roles/avatar.png', '1', 1, '2020-09-16 22:37:47', NULL, NULL),
	(2, 'Roles', '/img/roles/avatar2.png', '1', 1, '2020-09-16 22:48:18', NULL, NULL),
	(3, 'Roles', '/img/roles/avatar3.png', '1', 1, '2020-09-16 22:48:38', NULL, NULL),
	(4, 'Roles', '/img/roles/avatar04.png', '1', 1, '2020-09-16 22:48:57', NULL, NULL),
	(5, 'Roles', '/img/roles/avatar5.png', '1', 1, '2020-09-16 22:49:14', NULL, NULL);
/*!40000 ALTER TABLE `icono` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `prioridad` (
  `id_prioridad` bigint(20) NOT NULL AUTO_INCREMENT,
  `prioridad_name` varchar(250) DEFAULT NULL,
  `prioridad_estado` int(11) DEFAULT NULL,
  `prioridad_user_create` int(11) DEFAULT NULL,
  `prioridad_fecha_create` datetime DEFAULT NULL,
  `prioridad_user_modificate` int(11) DEFAULT NULL,
  `prioridad_fecha_modificate` datetime DEFAULT NULL,
  `prioridad_color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_prioridad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

DELETE FROM `prioridad`;
/*!40000 ALTER TABLE `prioridad` DISABLE KEYS */;
INSERT INTO `prioridad` (`id_prioridad`, `prioridad_name`, `prioridad_estado`, `prioridad_user_create`, `prioridad_fecha_create`, `prioridad_user_modificate`, `prioridad_fecha_modificate`, `prioridad_color`) VALUES
	(1, 'BAJA', 1, 1, '2020-10-18 19:17:03', NULL, NULL, 'bg-info'),
	(2, 'NORMAL', 1, 1, '2020-10-18 19:17:16', NULL, NULL, 'bg-success'),
	(3, 'URGENTE', 1, 1, '2020-10-18 19:17:30', NULL, NULL, 'bg-danger');
/*!40000 ALTER TABLE `prioridad` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `rol_aplicacion` (
  `id_rol` bigint(20) NOT NULL AUTO_INCREMENT,
  `rol_nombre` varchar(250) DEFAULT NULL,
  `rol_estado` int(11) DEFAULT NULL,
  `rol_fecha_create` datetime DEFAULT NULL,
  `rol_icono` varchar(50) DEFAULT NULL,
  `rol_fecha_update` datetime DEFAULT NULL,
  `rol_user_create` int(11) DEFAULT NULL,
  `rol_user_update` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

DELETE FROM `rol_aplicacion`;
/*!40000 ALTER TABLE `rol_aplicacion` DISABLE KEYS */;
INSERT INTO `rol_aplicacion` (`id_rol`, `rol_nombre`, `rol_estado`, `rol_fecha_create`, `rol_icono`, `rol_fecha_update`, `rol_user_create`, `rol_user_update`) VALUES
	(1, 'ADMINISTRADOR', 1, '2020-09-16 21:42:18', '/img/roles/avatar.png', '2020-09-26 16:24:48', 1, 1),
	(2, 'SOLICITANTE', 1, '2020-09-17 05:09:00', '/img/roles/avatar5.png', NULL, 1, NULL),
	(3, 'SOPORTE', 1, '2020-09-17 05:11:03', '/img/roles/avatar04.png', NULL, 1, NULL);
/*!40000 ALTER TABLE `rol_aplicacion` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `sgi_adjunta_solicitud` (
  `id_adjunta` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_solicitud` bigint(20) DEFAULT NULL,
  `adjuntar_url` varchar(250) DEFAULT NULL,
  `adjuntar_user_create` int(11) DEFAULT NULL,
  `adjuntar_fecha_create` datetime DEFAULT NULL,
  `adjuntar_estado` int(11) DEFAULT NULL,
  `adjuntar_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_adjunta`),
  KEY `id_solicitud` (`id_solicitud`),
  CONSTRAINT `FK_solicitud_adjuntar` FOREIGN KEY (`id_solicitud`) REFERENCES `sgi_solicitud` (`id_solicitud`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

DELETE FROM `sgi_adjunta_solicitud`;
/*!40000 ALTER TABLE `sgi_adjunta_solicitud` DISABLE KEYS */;
/*!40000 ALTER TABLE `sgi_adjunta_solicitud` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `sgi_auditoria` (
  `id_auditoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_solicitud` bigint(20) DEFAULT NULL,
  `id_usuario` bigint(20) DEFAULT NULL,
  `auditoria_descripcion` varchar(250) DEFAULT NULL,
  `auditoria_date` date DEFAULT NULL,
  `auditoria_fecha` datetime DEFAULT NULL,
  `auditoria_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_auditoria`),
  KEY `id_solicitud` (`id_solicitud`),
  KEY `fk_auditoria_usuario` (`id_usuario`),
  CONSTRAINT `FK_solicitud_auditoria` FOREIGN KEY (`id_solicitud`) REFERENCES `sgi_solicitud` (`id_solicitud`),
  CONSTRAINT `fk_auditoria_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario_aplicacion` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

DELETE FROM `sgi_auditoria`;
/*!40000 ALTER TABLE `sgi_auditoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `sgi_auditoria` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `sgi_solicitud` (
  `id_solicitud` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_tipo` bigint(20) DEFAULT NULL,
  `id_prioridad` bigint(20) DEFAULT NULL,
  `id_categoria` bigint(20) DEFAULT NULL,
  `id_subcategoria` bigint(20) DEFAULT NULL,
  `solicitud_tema` varchar(250) DEFAULT NULL,
  `solicitud_descripcion` varchar(500) DEFAULT NULL,
  `solicitud_codigo` bigint(20) DEFAULT NULL,
  `solicitud_anio` year(4) DEFAULT NULL,
  `solicitud_numero` varchar(50) DEFAULT NULL,
  `solicitud_fecha` date DEFAULT NULL,
  `solicitud_estado` int(11) DEFAULT NULL,
  `solicitud_user_create` int(11) DEFAULT NULL,
  `solicitud_fecha_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id_solicitud`),
  KEY `id_categoria` (`id_categoria`),
  KEY `id_subcategoria` (`id_subcategoria`),
  CONSTRAINT `FK_sol_cat` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`),
  CONSTRAINT `FK_sol_sc` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id_subcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

DELETE FROM `sgi_solicitud`;
/*!40000 ALTER TABLE `sgi_solicitud` DISABLE KEYS */;
/*!40000 ALTER TABLE `sgi_solicitud` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `sgi_tipo` (
  `id_tipo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_name` varchar(50) DEFAULT NULL,
  `tipo_estado` int(11) DEFAULT NULL,
  `tipo_user_create` int(11) DEFAULT NULL,
  `tipo_fecha_create` datetime DEFAULT NULL,
  `tipo_user_modificate` int(11) DEFAULT NULL,
  `tipo_fecha_modificate` datetime DEFAULT NULL,
  `tipo_color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

DELETE FROM `sgi_tipo`;
/*!40000 ALTER TABLE `sgi_tipo` DISABLE KEYS */;
INSERT INTO `sgi_tipo` (`id_tipo`, `tipo_name`, `tipo_estado`, `tipo_user_create`, `tipo_fecha_create`, `tipo_user_modificate`, `tipo_fecha_modificate`, `tipo_color`) VALUES
	(1, 'INCIDENCIA', 1, 1, '2020-10-18 19:25:29', NULL, NULL, 'bg-info'),
	(2, 'PREGUNTA', 1, 1, '2020-10-18 19:25:48', NULL, NULL, 'bg-warning'),
	(3, 'SOLICITUD', 1, 1, '2020-10-18 19:26:00', NULL, NULL, 'bg-danger');
/*!40000 ALTER TABLE `sgi_tipo` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `subcategoria` (
  `id_subcategoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_categoria` bigint(20) NOT NULL,
  `sc_name` varchar(250) DEFAULT NULL,
  `sc_estado` int(11) DEFAULT NULL,
  `sc_create_user` int(11) DEFAULT NULL,
  `sc_create_fecha` datetime DEFAULT NULL,
  `sc_modificate_user` int(11) DEFAULT NULL,
  `sc_modificate_fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id_subcategoria`),
  KEY `FK_categoria` (`id_categoria`),
  CONSTRAINT `FK_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

DELETE FROM `subcategoria`;
/*!40000 ALTER TABLE `subcategoria` DISABLE KEYS */;
INSERT INTO `subcategoria` (`id_subcategoria`, `id_categoria`, `sc_name`, `sc_estado`, `sc_create_user`, `sc_create_fecha`, `sc_modificate_user`, `sc_modificate_fecha`) VALUES
	(1, 1, 'CORREO', 1, 1, '2020-10-09 06:07:59', 1, '2020-10-10 01:39:50'),
	(2, 1, 'NAVEGADOR', 1, 1, '2020-10-09 23:34:49', NULL, NULL),
	(3, 1, 'HEINRICH', 1, 1, '2020-10-09 23:36:09', NULL, NULL),
	(4, 1, 'IPHONE', 1, 1, '2020-10-09 23:36:22', NULL, NULL),
	(5, 1, 'ZOOM', 1, 1, '2020-10-09 23:37:02', NULL, NULL),
	(6, 1, 'CELULAR', 1, 1, '2020-10-09 23:38:03', NULL, NULL),
	(7, 1, 'FRESH SERVICE', 1, 1, '2020-10-09 23:38:17', 1, '2020-10-10 01:32:32');
/*!40000 ALTER TABLE `subcategoria` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `usuario_aplicacion` (
  `id_usuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_cargo` bigint(20) DEFAULT NULL,
  `usu_nombre` varchar(250) DEFAULT NULL,
  `usu_apellidop` varchar(250) DEFAULT NULL,
  `usu_apellidom` varchar(250) DEFAULT NULL,
  `usu_fechaN` date DEFAULT NULL,
  `usuario` varchar(250) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `usu_email` varchar(250) DEFAULT NULL,
  `provider` varchar(250) DEFAULT NULL,
  `provider_id` varchar(250) DEFAULT NULL,
  `remember_token` varchar(250) DEFAULT NULL,
  `refresh_token` varchar(250) DEFAULT NULL,
  `usu_foto_perfil` varchar(250) DEFAULT NULL,
  `usu_usuario_creacion` bigint(20) DEFAULT NULL,
  `usu_fecha_creacion` datetime DEFAULT NULL,
  `usu_usuario_modificacion` bigint(20) DEFAULT NULL,
  `usu_fecha_modificacion` datetime DEFAULT NULL,
  `usu_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_cargo` (`id_cargo`),
  CONSTRAINT `FKcargo_user` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id_cargo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

DELETE FROM `usuario_aplicacion`;
/*!40000 ALTER TABLE `usuario_aplicacion` DISABLE KEYS */;
INSERT INTO `usuario_aplicacion` (`id_usuario`, `id_cargo`, `usu_nombre`, `usu_apellidop`, `usu_apellidom`, `usu_fechaN`, `usuario`, `password`, `usu_email`, `provider`, `provider_id`, `remember_token`, `refresh_token`, `usu_foto_perfil`, `usu_usuario_creacion`, `usu_fecha_creacion`, `usu_usuario_modificacion`, `usu_fecha_modificacion`, `usu_estado`) VALUES
	(1, 1, 'ADMIN', '', '', '1900-01-01', 'ADMIN', '$2y$10$2aPKfhgHKvza15Q8X0zgv..yZ6v2pjCTd78fUtBivuZKXBMEDGAs2', 'admin@gmail.com', NULL, NULL, 'UySqspvkwivMQAN07LZtqmWKViWxppyi1sczekgvAwnXQCtyYNkONdrLyeEb', NULL, '/img/roles/avatar.png', 1, '2020-09-26 17:15:43', 1, '2020-10-08 01:49:24', 1),
	(3, 2, 'Victor', 'Flores', 'Diaz', '1995-04-25', 'VFLORES', '$2y$10$yHnCLWZNkZJw5QLOD.SD4uYae7/mCH1FYc2Gnosbq8JJ0eh2B.kNG', 'vicfeflo25@gmail.com', NULL, NULL, NULL, NULL, '/img/roles/avatar5.png', 1, '2020-09-26 15:43:36', 1, '2020-10-10 15:46:30', 1);
/*!40000 ALTER TABLE `usuario_aplicacion` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `usuario_rol` (
  `id_usuario_rol` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuario` bigint(20) DEFAULT NULL,
  `id_rol` bigint(20) DEFAULT NULL,
  `ur_usuario_creacion` bigint(20) DEFAULT NULL,
  `ur_fecha_creacion` datetime DEFAULT NULL,
  `ur_usuario_modificacion` bigint(20) DEFAULT NULL,
  `ur_fecha_modificacion` datetime DEFAULT NULL,
  `ur_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario_rol`),
  KEY `FK_usuario_aplicacion` (`id_usuario`),
  KEY `FK_rol` (`id_rol`),
  CONSTRAINT `FK_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol_aplicacion` (`id_rol`),
  CONSTRAINT `FK_usuario_aplicacion` FOREIGN KEY (`id_usuario`) REFERENCES `usuario_aplicacion` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

DELETE FROM `usuario_rol`;
/*!40000 ALTER TABLE `usuario_rol` DISABLE KEYS */;
INSERT INTO `usuario_rol` (`id_usuario_rol`, `id_usuario`, `id_rol`, `ur_usuario_creacion`, `ur_fecha_creacion`, `ur_usuario_modificacion`, `ur_fecha_modificacion`, `ur_estado`) VALUES
	(1, 1, 1, NULL, '2020-10-08 01:49:24', NULL, NULL, 1),
	(2, 3, 2, NULL, '2020-09-27 22:20:16', NULL, NULL, 1);
/*!40000 ALTER TABLE `usuario_rol` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
