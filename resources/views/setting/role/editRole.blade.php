<form id="updatedRole">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Nombre Perfil: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)"  class="form-control input-sm req" value="{{ $rol->rol->rol_nombre }}" id="rol_nombre" name="rol_nombre">
        <input type="hidden" maxlength="150" class="form-control req"  id="id_rol" name="id_rol" value="{{ $rol->rol->id_rol }}">
        <input type="hidden" maxlength="150" class="form-control req"  id="rol_user_create" name="rol_user_create" value="{{ Auth::user()->id_usuario }}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Foto Perfil por Defecto: <span class="text-danger">*</span></label>
        <div class="row">
          @foreach ($rol->iconos as $key => $value)
            <div class="col-sm-2">
              <pre style="display:none">
                {{ print_r($value->en_uso) }}
                <br>
                {{ print_r($rol->rol->rol_icono) }}
              </pre>
              <a {{ ($value->en_uso == 0 || ($value->en_uso ==1 && $value->ico_url == $rol->rol->rol_icono))?$value->items_href.' '.$value->items_toggle.' '.$value->items_title.' '.$value->items_gallery:''}} >
                <img  src="{{ $value->ico_url }}" {{ ($value->en_uso == 0 || $value->ico_url == $rol->rol->rol_icono)?'':'style=\"filter:grayscale(100%);-webkit-filter:grayscale(100%);\"' }} class="img-fluid mb-2 {{ ($value->ico_url == $rol->rol->rol_icono)?'border border-dark':'' }}" alt="white sample"/>
              </a>
            </div>
          @endforeach

        </div>
        <input type="hidden" onkeypress="return /[a-z-ñ]/i.test(event.key)" value="{{ $rol->rol->rol_icono }}" class="form-control input-sm req" id="rol_icono" name="rol_icono">
      </div>
    </div>
  </div>
</form>
