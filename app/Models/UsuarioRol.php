<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioRol extends Model{

    protected $connection = 'mysql';
    protected $table = 'usuario_rol';
    protected $primaryKey = 'id_usuario_rol';
    public $timestamps = false;

    public function rolA(){
      return $this->belongsTo(Rol::class,'id_rol');
    }

    public function getRol(){
      $conf = Rol::where('id_rol',$this->id_rol)->first();
      return $conf;
    }

}
