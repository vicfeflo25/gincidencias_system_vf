<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Prioridad;

class PrioridadController extends Controller{

  public function __construct(){
      $this->middleware('auth');
  }

  public function listPrioridad(){
    $prioridad = Prioridad::getListPrioridad();
    return view('setting.prioridad.listPrioridad',compact('prioridad'));
  }

  public function prioridadNew(){
    return view('setting.prioridad.newPrioridad');
  }

  public function viewPrioridad(){
    $prioridad = Prioridad::getPrioridadX();
    return view('setting.prioridad.viewPrioridad',compact('prioridad'));
  }

  public function editPrioridad(){
    $prioridad = Prioridad::getPrioridadX();
    return view('setting.prioridad.editOldPrioridad',compact('prioridad'));
  }

}
