<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Icono extends Model{

    protected $connection = 'mysql';
    protected $table = 'icono';
    protected $primaryKey = 'id_icono';
    public $timestamps = false;

    public static function getListIcono(){
      $list_iconos = Icono::where('ico_estado',1)->get();

      $roles = Rol::where('rol_estado',1)->get()->toArray();
      $title = "sample 1 - white";

      foreach($list_iconos as $key => $value){
        if(array_search($value->ico_url,array_column($roles,'rol_icono')) !== FALSE){
          $k = array_search($value,array_column($roles,'rol_icono'));
          $value->en_uso = 1;
          $value->items = '';
          $value->items_toggle = '';
          $value->items_title = '';
          $value->items_gallery = '';
        }else{
          $value->en_uso = 0;
          $value->items_href = 'href='.$value->ico_url;
          $value->items_toggle = 'data-toggle=lightbox';
          $value->items_title = 'data-title=Opción'.($key+1);
          $value->items_gallery = 'data-gallery=gallery';
        }
      }
      return $list_iconos;
    }

    public static function getListIconoUnico($icon_url){
      $list_iconos = Icono::where('ico_estado',1)->get();

      $roles = Rol::where('rol_estado',1)->get()->toArray();
      $title = "sample 1 - white";

      foreach($list_iconos as $key => $value){
        if(array_search($value->ico_url,array_column($roles,'rol_icono')) !== FALSE){
          $k = array_search($value,array_column($roles,'rol_icono'));
          $value->en_uso = 1;
          $value->items = '';
          $value->items_toggle = '';
          $value->items_title = '';
          $value->items_gallery = '';
        }else{
          $value->en_uso = 0;
          $value->items_href = 'href='.$value->ico_url;
          $value->items_toggle = 'data-toggle=lightbox';
          $value->items_title = 'data-title=Opción'.($key+1);
          $value->items_gallery = 'data-gallery=gallery';
        }

        if($icon_url == $value->ico_url){
          $value->en_uso = 1;
          $value->items_href = 'href='.$value->ico_url;
          $value->items_toggle = 'data-toggle=lightbox';
          $value->items_title = 'data-title=Opción'.($key+1);
          $value->items_gallery = 'data-gallery=gallery';
        }
      }
      return $list_iconos;
    }

}
