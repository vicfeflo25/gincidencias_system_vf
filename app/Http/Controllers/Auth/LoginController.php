<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{

    use AuthenticatesUsers;
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo2 = RouteServiceProvider::BANDEJA;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('guest')->except('logout');
    }


    public function username(){
      return 'usuario';
    }

    public function redirectPath(){
        if (Auth::user()->rol[0]->id_rol == env('ID_SOLICITANTE')) {
            return $this->redirectTo2;
        }else{
          return $this->redirectTo;
        }

    }

}
