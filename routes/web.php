<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*RUTAS DE LOGIN Y LOGOUT*/
Auth::routes();

/*RUTAS DE MANTENIMIENTO DE ADMINISTRACIÖN CARGOS*/
Route::get('/listArea','AreaController@listArea')->name('listArea');
Route::get('/newArea','AreaController@areaNew')->name('newArea');
Route::post('/viewArea','AreaController@viewArea')->name('viewArea');
Route::post('/editArea','AreaController@editArea')->name('editArea');

/*RUTAS DE MANTENIMIENTO DE ADMINISTRACIÖN PERFILES O ROLES*/
Route::get('/listPerfil','RolController@listRol')->name('listRole');
Route::get('/newRole','RolController@addRole')->name('newRole');
Route::post('/viewRole','RolController@viewRole')->name('viewRole');
Route::post('/editRole','RolController@editRole')->name('editRole');

/*RUTAS DE MANTENIMIENTO DE ADMINISTRACIÖN CARGOS*/
Route::get('/listJob','JobController@listJob')->name('listJob');
Route::get('/newJob','JobController@jobNew')->name('newJob');
Route::post('/viewJob','JobController@viewJob')->name('viewJob');
Route::post('/editJob','JobController@editJob')->name('editJob');

/*RUTAS DE MANTENIMIENTO DE ADMINISTRACIÖN NIVEL*/
Route::get('/listNivel','NivelController@listNivel')->name('listNivel');
Route::get('/newNivel','NivelController@nivelNew')->name('newNivel');
Route::post('/viewNivel','NivelController@viewNivel')->name('viewNivel');
Route::post('/editNivel','NivelController@editNivel')->name('editNivel');

/*RUTAS DE MANTENIMIENTO DE ADMINISTRACIÖN NIVEL*/
Route::get('/listPrioridad','PrioridadController@listPrioridad')->name('listPrioridad');
Route::get('/newPrioridad','PrioridadController@prioridadNew')->name('newPrioridad');
Route::post('/viewPrioridad','PrioridadController@viewPrioridad')->name('viewPrioridad');
Route::post('/editPrioridad','PrioridadController@editPrioridad')->name('editPrioridad');

/*RUTAS DE MANTENIMIENTO DE ADMINISTRACIÖN CATEGORIA Y SUBCATEGORIA*/
Route::get('/listCategoria','CategoriaController@listCategoria')->name('listCategoria');
Route::get('/newCategoria','CategoriaController@categoriaNew')->name('newCategoria');
Route::post('/viewCategoria','CategoriaController@viewCategoria')->name('viewCategoria');
Route::post('/editCategoria','CategoriaController@editCategoria')->name('editCategoria');
Route::get('/listSubCategoria','CategoriaController@listSubCategoria')->name('listSubCategoria');
Route::get('/newSubCategoria','CategoriaController@categoriaSubNew')->name('newSubCategoria');
Route::post('/viewSubCategoria','CategoriaController@viewSubCategoria')->name('viewSubCategoria');
Route::post('/editSubCategoria','CategoriaController@editSubCategoria')->name('editSubCategoria');

/*RUTAS DE MANTENIMIENTO DE ADMINISTRACIÖN DE USUARIOS*/
Route::get('/listUsers','UserController@getListUsers')->name('listUsers');
Route::get('/newUser','UserController@newUser')->name('newUser');
Route::post('/viewUser','UserController@viewUser')->name('viewUser');
Route::post('/editUser','UserController@editUser')->name('editUser');


Route::get('/listTickets','TicketController@listTickets')->name('bandeja');
Route::get('/newTicket','TicketController@newTicket')->name('newTicket');
Route::get('/viewTicket/{numero}','TicketController@viewTicket')->name('viewTicket');

Route::get('/generateReport/{type}','ReporteController@generateReport')->name('generateReport');

Route::post('/infoUser','UserController@viewInfo')->name('infoUser');
Route::get('/home', 'ReporteController@dashboard')->name('home');

Route::get('/upload','TestController@import');
Route::post('/excel','TestController@excel');
Route::get('/test','TestController@test');
