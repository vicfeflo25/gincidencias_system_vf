<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reporte;

class ReporteController extends Controller{

  public function __construct(){
      $this->middleware('auth');
  }

  public function generateReport($type){
    if($type == 1){
      $texto = 'Reporte Ratio Resolución de Incidencias';
      $subtexto = 'RRI';
    }else if($type == 2){
      $texto = 'Reporte Ratio Utilización Laboral en Incidencias';
      $subtexto = 'RULI';
    }else if($type == 3){
      $texto = 'Reporte Incidencias';
      $subtexto = 'INCIDENCIAS';
    }
    return view('reports.reportrri',compact('texto','type','subtexto'));
  }

  public function dashboard(){
    $reporte = Reporte::getQueryDashboard();
    return view('pages.home',compact('reporte'));
  }

}
