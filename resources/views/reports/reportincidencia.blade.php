@if(!empty($reporte))
<table class="table table-bordered table-striped" id="table1">
  <thead  style="background-color:#3B9FF5">
    <tr>
      <th>NRO</th>
      <th>CATEGORIA</th>
      <th>FECHA CREACIÓN</th>
      <th>FECHA CIERRE</th>
      <th>PRIORIDAD</th>
      <th>SOLICITANTE</th>
      <th>AGENTE</th>
      <th>ESTADO</th>
    </tr>
  </thead>
  <tbody>
    @foreach($reporte as $key => $value)
    <tr>
      <td>{{ $value->solicitud_numero }}</td>
      <td>{{ $value->categoria_name }}</td>
      <td>{{ $value->solicitud_fecha_create }}</td>
      <td>{{ $value->solicitud_fecha_soluciona }}</td>
      <td>{{ $value->prioridad_name }}</td>
      <td>{{ $value->solicitante }}</td>
      <td>{{ $value->agente }}</td>
      <td>{{ $value->estado }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@else
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">¡Alerta!</h4>
  <p>No se encontraron registros para las fechas Seleccionadas.</p>
</div>
@endif

<script type="text/javascript">
  $(document).ready( function () {
    $('#table1').DataTable({
      responsive: true,
      columnDefs: [
          { responsivePriority: 1, targets: 1 },
          { responsivePriority: 2, targets: 2 }
      ],
      order: [[ 0, 'desc' ]],
      dom: 'Bfrtip',
      buttons: [
        { extend: 'pdf', text:  'Exportar a PDF' },
        { extend: 'excel', text: 'Exportar a EXCEL' },
        'pageLength'
      ],
      lengthMenu: [
        [ 10, 25, 50, -1 ],
        [ '10 Filas', '25 Filas', '50 Filas', 'Mostrar todo' ]
      ],
    });
  });
</script>
