<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Nivel;

class CategoriaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function listCategoria(){
      $categoria = Categoria::getListCategoria();
      return view('setting.categoria.listCategoria',compact('categoria'));
    }

    public function categoriaNew(){
      return view('setting.categoria.newCategoria');
    }

    public function viewCategoria(){
      $categoria = Categoria::getCategoria();
      return view('setting.categoria.viewCategoria',compact('categoria'));
    }

    public function editCategoria(){
      $categoria = Categoria::getCategoria();
      return view('setting.categoria.editOldCategoria',compact('categoria'));
    }

    public function listSubCategoria(){
      $subcategoria = Categoria::listSubCategoria();
      if(empty($subcategoria->categoria)){
        return redirect('/listCategoria');
      }
      return view('setting.subcategoria.listSubCategoria',compact('subcategoria'));
    }

    public function categoriaSubNew(){
      $categoria = Categoria::where('categoria_estado',1)
                            ->orderBy('id_categoria','asc')
                            ->get();

      $nivel = Nivel::where('nivel_estado',1)
                    ->orderBy('nivel_descripcion','asc')
                    ->get();

      return view('setting.subcategoria.newSubCategoria',compact('categoria','nivel'));
    }

    public function editSubCategoria(){
      $subcategoria = Categoria::getSubCategoria();
      $categoria = Categoria::where('categoria_estado',1)
                            ->orderBy('id_categoria','asc')
                            ->get();
      $nivel = Nivel::where('nivel_estado',1)
                    ->orderBy('nivel_descripcion','asc')
                    ->get();
      return view('setting.subcategoria.editSubCategoria',compact('subcategoria','categoria','nivel'));
    }

    public function viewSubCategoria(){
      $subcategoria = Categoria::getSubCategoria();
      $categoria = Categoria::where('categoria_estado',1)
                            ->orderBy('id_categoria','asc')
                            ->get();
      $nivel = Nivel::where('nivel_estado',1)
                    ->orderBy('nivel_descripcion','asc')
                    ->get();
      return view('setting.subcategoria.viewSubCategoria',compact('subcategoria','categoria','nivel'));
    }

}
