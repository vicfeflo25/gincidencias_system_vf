<form id="viewCategoria">
  <meta type="hidden" name="csrf-token" content="{{ csrf_token() }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <div class="form-group">
        <label>Prioridad: <span class="text-danger">*</span></label>
        <input type="text" onkeypress="return /[a-z-ñ ]/i.test(event.key)" disabled class="form-control input-sm req" id="prioridad_name" value="{{ $prioridad->prioridad_name }}" name="prioridad_name">
      </div>
    </div>
  </div>
</form>
<script>

</script>
