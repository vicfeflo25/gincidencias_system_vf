@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endsection

@section('content')
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 {{ Auth::user()->getConf()->letra_theme }}">Nuevo Ticket</h1>
          @php($subcategorias = json_encode($solicitud->subcategorias))
          <input type="hidden" name="subcategorias" id="subcategorias" value="{{ $subcategorias }}">
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Solicitar Ticket</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content mb-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card card-default {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-header {{ Auth::user()->getConf()->border_theme }}">
              <h3 class="card-title">¿Necesita ayuda con algo? Crear una nueva solicitud aquí. Para un servicio más rápido agregue tanta información como sea posible.</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <label class="mb-0 text-gray">Datos del Ticket</label>
                </div>
              </div>
              <hr>
              <div class="row">
                <form enctype="multipart/form-data" id="formSolicitud" method="POST">
                  <div class="col-md-6 mr-1">
                    <div class="form-group">
                      <label>Área: <a title="Este campo es obligatorio" class="text-sm text-warning"><i class="fas fa-info-circle"></i> </a></label>
                      <input type="hidden" class="form-control inut-sm req" readonly name="id_tipo" id="id_tipo" value="{{ $solicitud->tipo[0]->id_tipo }}">
                      <select class="form-control input-sm req" id="id_area" name="id_area">
                        <option value="0">Seleccione</option>
                        @foreach($solicitud->areas as $key => $value)
                          <option value="{{ $value->id_area }}">{{ $value->area_nombre }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6 mr-1">
                    <div class="form-group">
                      <label>Prioridad: <a title="Este campo es obligatorio" class="text-sm text-warning"><i class="fas fa-info-circle"></i> </a></label>
                      <select class="form-control input-sm req" id="id_prioridad" name="id_prioridad">
                        <option value="0">Seleccione</option>
                        @foreach($solicitud->prioridades as $key => $value)
                          <option value="{{ $value->id_prioridad }}">{{ $value->prioridad_name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6 mr-1">
                    <div class="form-group">
                      <label>Categoria: <a title="Este campo es obligatorio" class="text-sm text-warning"><i class="fas fa-info-circle"></i> </a></label>
                      <select class="form-control input-sm req" id="id_categoria" name="id_categoria">
                        <option value="0">Seleccione</option>
                        @foreach($solicitud->categorias as $key => $value)
                          <option value="{{ $value->id_categoria }}">{{ $value->categoria_name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Sub Categoria: <a title="Este campo es obligatorio" class="text-sm text-warning"><i class="fas fa-info-circle"></i> </a></label>
                      <select class="form-control input-sm req" id="id_subcategoria" name="id_subcategoria">
                        <option value="0">Seleccione</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Asunto: <a title="Este campo es obligatorio" class="text-sm text-warning"><i class="fas fa-info-circle"></i> </a></label>
                      <input type="text"  class="form-control input-sm req" name="solicitud_tema" id="solicitud_tema" maxlength="250">
                      <input type="hidden"  class="form-control input-sm" name="id_usuario" id="id_usuario" value="{{ Auth::user()->id_usuario }}">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Descripción: <a title="Este campo es obligatorio" class="text-sm text-warning"><i class="fas fa-info-circle"></i> </a></label>
                      <textarea class="form-control input-sm req" name="comentario_descripcion" id="comentario_descripcion" rows="8" cols="500"></textarea>
                    </div>
                  </div>
                  <div class="col-md-6 mr-1">
                    <div class="form-group">
                      <a class="btn btn-sm btn-default text-dark" id="btnAdjuntar" ><i class="fas fa-upload"></i> Adjuntar</a><input type="hidden" name="tipo" id="tipo" value="0">
                    </div>
                  </div>
                  <div class="col-md-12" id="div-adjuntar" hidden>
                    <div class="form-group alert alert-info">
                      <span class="text-sm">Tamaño Máximo 2MB:</span>
                      @for($i=0;$i<3;$i++)
                      <input class="form-control input-sm col-md-6 mr-1 border-0 bg-info" type="file" id="file_adjuntar[{{ $i }}]" name="file_adjuntar[]" accept="image/png, image/jpeg, image/jpg">
                      @endfor
                    </div>
                  </div>
                </form>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <a class="btn btn-sm btn-success text-white" id="saveSolicitud"><i class="fas fa-save"></i> GUARDAR</a>
                </div>
              </div>
              <hr>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        {{-- <div class="col-md-4">
          <div class="card card-default {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-header border">
              Mis Tickets
            </div>
            <div class="card-body">
              <ul class="nav flex-column nav-pills bs-docs-sidenav">
                <li class="nav-link border"><a href="#" class="{{ Auth::user()->getConf()->letra_theme }}"><span class="fas fa-chevron-right chevron-navbar" style="float:right"></span> Tickets Abiertos</a></li>
                <li class="nav-link border"><a href="#"  class="{{ Auth::user()->getConf()->letra_theme }}"><span class="fas fa-chevron-right chevron-navbar" style="float:right"></span> Todos los Tickets</a></li>
                <li class="nav-link border"><a href="#"  class="{{ Auth::user()->getConf()->letra_theme }}"><span class="fas fa-chevron-right chevron-navbar" style="float:right"></span> Tickets Cerrados</a></li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
        </div> --}}
      </div>
    </div>
  </section>
@endsection

@section('script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      var subcategorias = $.parseJSON($("#subcategorias").val());
    });

    $("#id_categoria").on('change',function(){
      let list_subcategorias = $.parseJSON($("#subcategorias").val());
      let id_categoria = $(this).val();
      if(id_categoria != 0){
        list_subcategorias = list_subcategorias.filter( item => item.id_categoria ==  id_categoria);
        if(list_subcategorias.length > 0){
          $.each(list_subcategorias,function(k,v){
            $("#id_subcategoria").append('<option value="'+v.id_subcategoria+'">'+v.sc_name+'</option>');
          })
        }else{
          $("#id_subcategoria").html('<option value="0">Seleccione</option>');
        }
      }else {
        $("#id_subcategoria").html('<option value="0">Seleccione</option>');
      }
    });

    $("#btnAdjuntar").on('click',function(){
      if($("#tipo").val() == 0){
        $("#div-adjuntar").removeAttr('hidden');
        $("#tipo").val(1);
      }else if($("#tipo").val() == 1){
        $("#div-adjuntar").attr('hidden','hidden');
        $("#tipo").val(0);
      }
    })

    $("#saveSolicitud").on('click',function(){
      let count = 0;
      $.each($("select.req"),function(key,value){
        if($(this).val() == 0){
          $(this).addClass('is-invalid')
          count++;
        }else{
          $(this).removeClass('is-invalid')
        }
      })
      $.each($("input.req"),function(key,value){
        if($(this).val() == 0){
          $(this).addClass('is-invalid')
          count++;
        }else{
          $(this).removeClass('is-invalid')
        }
      })
      $.each($("textarea.req"),function(key,value){
        if($(this).val() == 0){
          $(this).addClass('is-invalid')
          count++;
        }else{
          $(this).removeClass('is-invalid')
        }
      })
      if(count == 0){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        var form = new FormData(document.getElementById("formSolicitud"));
        $.ajax({
        	url: 'api/saveSolicitud',
          method: "POST",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: form,
          dataType:"json",
          contentType:false,
          processData:false,
          cache:false,
        	success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
        	}
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar todos los campos obligatorios para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })

    $(function () {
      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $('a img.img-fluid').removeClass('border border-dark')
        $(this).children().addClass('border border-dark')
        $("#rol_icono").val($(this).attr('href'))
      });
    })

  </script>
@endsection
