<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adjuntar extends Model{

    protected $connection = 'mysql';
    protected $table = 'sgi_adjunta_solicitud';
    protected $primaryKey = 'id_adjuntar';
    public $timestamps = false;

}
