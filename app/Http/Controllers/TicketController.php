<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;


class TicketController extends Controller{

  public function __construct(){
    $this->middleware('auth');
  }

  public function listTickets(){
    $list_ticket = Ticket::getList();
    return view('bandeja.listado',compact('list_ticket'));
  }

  public function viewTicket($numero){
    $solicitud = Ticket::getTicket($numero);
    return view('tickets.viewTicket',compact('solicitud'));
  }

  public function newTicket(){
    $solicitud = Ticket::newTicket();
    return view('tickets.newTicket',compact('solicitud'));
  }

}
