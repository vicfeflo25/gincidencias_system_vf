<?php

namespace App\Http\Controllers;

use App\Imports\TicketImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Models\Ticket;

class TestController extends Controller
{
    public function import(){
      return view('upload');
    }

    public function excel(){
        Excel::import(new TicketImport, request()->file('file'));
        return redirect('/home')->with('success', 'SUBIO TODO PERROS!!!');
    }

    public function test(){
      $ticket = Ticket::where('solicitud_estado',4)->get();

      foreach ($ticket as $key => $value) {
        $value->solicitud_fecha_cerrado = date('Y-m-d H:i:s',strtotime('+3 hour',strtotime($value->solicitud_fecha_soluciona)));
        $value->save();
      }

      return $ticket;
    }
}
