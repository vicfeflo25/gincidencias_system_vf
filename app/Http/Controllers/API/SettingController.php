<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Cargo;
use App\Models\Rol;
use App\Models\Area;
use App\Models\Nivel;
use App\Models\Prioridad;
use App\User;

class SettingController extends Controller{

  // public function __construct(){
  //     $this->middleware('auth');
  // }

  /*MODULO CATEGORIA Y SUBCATEGORIA*/
  public function saveCategoria(){
    $categoria = Categoria::categoriaSave();
    return $categoria;
  }

  public function updateCategoria(){
    $categoria = Categoria::categoriaUpdate();
    return $categoria;
  }

  public function changeCategoria(){
    $categoria = Categoria::changeStateCategoria();
    return $categoria;
  }

  public function saveSubCategoria(){
    $subcategoria = Categoria::subcategoriaSave();
    return $subcategoria;
  }

  public function updateSubCategoria(){
    $categoria = Categoria::subcategoriaUpdate();
    return $categoria;
  }

  public function changeSubCategoria(){
    $subcategoria = Categoria::changeStateSubCategoria();
    return $subcategoria;
  }
  /*END MODULO CATEGORIA Y SUBCATEGORIA*/

  /*MODULO CARGO*/
  public function saveCargo(){
    $cargo = Cargo::cargoSave();
    return $cargo;
  }

  public function updateCargo(){
    $cargo = Cargo::cargoUpdate();
    return $cargo;
  }

  public static function changeJob(){
    $cargo = Cargo::changeStateJob();
    return $cargo;
  }
  /*END MODULO CARGO*/

  /*MODULO AREA*/
  public function saveArea(){
    $area = Area::areaSave();
    return $area;
  }

  public function updateArea(){
    $area = Area::areaUpdate();
    return $area;
  }

  public static function changeArea(){
    $area = Area::changeStateArea();
    return $area;
  }
  /*END MODULO CARGO*/

  /*MODULO ROL*/
  public function saveRole(){
    $rol = Rol::roleSave();
    return $rol;
  }

  public function updateRole(){
    $rol = Rol::updateRole();
    return $rol;
  }

  public function changeRole(){
    $rol = Rol::changeRole();
    return $rol;
  }
  /*END MODULO ROL*/

  /*MODULO USUARIO*/
  public function saveUser(){
    $user = User::userSave();
    return $user;
  }

  public function updateUser(){
    $user = User::userUpdate();
    return $user;
  }

  public function changeUser(){
    $user = User::changeUser();
    return $user;
  }

  public function changeTheme(){
    $user = User::changeTheme();
    return $user;
  }

  public function changePassword(){
    $user = User::changePassword();
    return $user;
  }

  public function restaurarPassword(){
    $user = User::restaurarPassword();
    return $user;
  }


  public function saveNivel(){
    $nivel = Nivel::NivelSave();
    return $nivel;
  }

  public function updateNivel(){
    $nivel = Nivel::NivelUpdate();
    return $nivel;
  }

  public function changeNivel(){
    $nivel = Nivel::changeStateNivel();
    return $nivel;
  }

  public function savePrioridad(){
    $prioridad = Prioridad::PrioridadSave();
    return $prioridad;
  }

  public function updatePrioridad(){
    $prioridad = Prioridad::PrioridadUpdate();
    return $prioridad;
  }

  public function changePrioridad(){
    $prioridad = Prioridad::changeStatePrioridad();
    return $prioridad;
  }
  /*END MODULO USUARIO*/

}
