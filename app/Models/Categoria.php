<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model{

    protected $connection = 'mysql';
    protected $table = 'categoria';
    protected $primaryKey = 'id_categoria';
    public $timestamps = false;

    public static function getListCategoria(){
      $categoria = Categoria::select('categoria.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador')
                      ->join('usuario_aplicacion as u','u.id_usuario','categoria.categoria_user_create')
                      ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','categoria.categoria_user_modificate')
                      ->get();

      foreach($categoria as $key => $value){
        if($value->categoria_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else{
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }

        if(empty($value->categoria_user_modificate)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->categoria_fecha_create));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->categoria_fecha_create));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->categoria_fecha_modificate));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->categoria_fecha_modificate));
        }
      }

      return $categoria;
    }

    public static function getCategoria(){
      $categoria = Categoria::where('id_categoria',request()->id_categoria)->first();
      return $categoria;
    }

    public static function categoriaSave(){
      try {
        $categoria_existe = Categoria::where('categoria_name',strtoupper(request()->categoria_name))->first();
        if(empty($categoria_existe)){
          $categoria = new Categoria();
          $categoria->categoria_user_create = request()->categoria_user_create;
          $categoria->categoria_fecha_create = now();
          $categoria->categoria_name = strtoupper(request()->categoria_name);
          $categoria->categoria_estado = 1;
          $categoria->save();
          return array("error"=>0,"mensaje"=>"Se agregó exitosamente la Categoria ".$categoria->categoria_name,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"La Categoria ".strtoupper(request()->categoria_name)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function categoriaUpdate(){
      try {
        $categoria_existe = Categoria::where('categoria_name',strtoupper(request()->categoria_name))->where('id_categoria','!=',request()->id_categoria)->first();
        if(empty($categoria_existe)){
          $categoria = Categoria::find(request()->id_categoria);
          $categoria->categoria_user_modificate = request()->categoria_user_modificate;
          $categoria->categoria_fecha_modificate = now();
          $categoria->categoria_name = strtoupper(request()->categoria_name);
          $categoria->save();
          return array("error"=>0,"mensaje"=>"Se actualizó exitosamente la Categoria ".$categoria->categoria_name,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"La Categoria ".strtoupper(request()->categoria_name)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeStateCategoria(){
      try {
        $categoria = Categoria::find(request()->id_categoria);
        if($categoria->categoria_estado == 1){
          $categoria->categoria_estado = 0;
          $mensaje = 'Se inactivo exitosamente la Categoria '.$categoria->categoria_name;
        }else{
          $categoria->categoria_estado = 1;
          $mensaje = 'Se activo exitosamente la Categoria '.$categoria->categoria_name;
        }
        $categoria->categoria_user_modificate = request()->id_user;
        $categoria->categoria_fecha_modificate = now();
        $categoria->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function listSubCategoria(){
      $subcategoria = new SubCategoria();
      $subcategoria->categoria = Categoria::where('categoria_estado',1)->get();
      $subcategoria->subcategoria = SubCategoria::select('subcategoria.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador','c.categoria_name','n.nivel_descripcion')
                      ->join('categoria as c','c.id_categoria','subcategoria.id_categoria')
                      ->join('usuario_aplicacion as u','u.id_usuario','subcategoria.sc_create_user')
                      ->join('sgi_nivel as n','n.id_nivel','subcategoria.id_nivel')
                      ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','subcategoria.sc_modificate_user')
                      ->get();

      foreach($subcategoria->subcategoria as $key => $value){
        if($value->sc_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else{
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }

        if(empty($value->sc_modificate_user)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->sc_create_fecha));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->sc_create_fecha));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->sc_modificate_fecha));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->sc_modificate_fecha));
        }
      }
      return $subcategoria;
    }

    public static function subcategoriaSave(){
      try {
        $subcategoria_existe = SubCategoria::where('sc_name',strtoupper(request()->sc_name))->first();
        if(empty($subcategoria_existe)){
          $subcategoria = new SubCategoria();
          $subcategoria->id_categoria = request()->id_categoria;
          $subcategoria->id_nivel = request()->id_nivel;
          $subcategoria->sc_create_user = request()->sc_create_user;
          $subcategoria->sc_create_fecha = now();
          $subcategoria->sc_name = strtoupper(request()->sc_name);
          $subcategoria->sc_estado = 1;
          $subcategoria->save();
          return array("error"=>0,"mensaje"=>"Se agregó exitosamente la Sub Categoria ".$subcategoria->sc_name,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"La Sub Categoria ".strtoupper(request()->sc_name)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function subcategoriaUpdate(){
      try {
        $subcategoria_existe = SubCategoria::where('sc_name',strtoupper(request()->sc_name))->where('id_subcategoria','!=',request()->id_subcategoria)->first();
        if(empty($subcategoria_existe)){
          $subcategoria = SubCategoria::find(request()->id_subcategoria);
          $subcategoria->id_categoria = request()->id_categoria;
          $subcategoria->id_nivel = request()->id_nivel;
          $subcategoria->sc_modificate_user = request()->sc_modificate_user;
          $subcategoria->sc_modificate_fecha = now();
          $subcategoria->sc_name = strtoupper(request()->sc_name);
          $subcategoria->save();
          return array("error"=>0,"mensaje"=>"Se actualizó exitosamente la Sub Categoria ".$subcategoria->sc_name,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"La Sub Categoria ".strtoupper(request()->sc_name)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
       } catch (\Exception $e) {
         return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
       }
    }

    public static function getSubCategoria(){
      $subcategoria = SubCategoria::where('id_subcategoria',request()->id_subcategoria)->first();
      return $subcategoria;
    }

    public static function changeStateSubCategoria(){
      try {
        $subcategoria = SubCategoria::find(request()->id_subcategoria);
        if($subcategoria->sc_estado == 1){
          $subcategoria->sc_estado = 0;
          $mensaje = 'Se inactivo exitosamente la Sub Categoria '.$subcategoria->sc_name;
        }else{
          $subcategoria->sc_estado = 1;
          $mensaje = 'Se activo exitosamente la Sub Categoria '.$subcategoria->sc_name;
        }
        $subcategoria->sc_modificate_user = request()->id_user;
        $subcategoria->sc_modificate_fecha = now();
        $subcategoria->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function getCategoriaActivate(){
      $categorias = Categoria::where('categoria_estado',1)
                              ->orderBy('categoria_name','asc')
                              ->get();

      return $categorias;
    }
}
