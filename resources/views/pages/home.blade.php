@extends('layouts.index')
@section('style')
<link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Montserrat"
      rel="stylesheet"
    />

    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />
<link rel="stylesheet" href="{{ asset('dashboards/style.css') }}" />
@endsection

@section('content')
  <br>
  <div class="content-area">
    <div class="container-fluid">
      <div class="main">
        <div class="row sparkboxes mt-4">
          <div class="col-md-3">
            <div class="box box1">
              <div class="details">
                <h3></h3>
                <h4 id="title_spark0"></h4>
              </div>
              <div id="spark0"></div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="box box2">
              <div class="details">
                <h3></h3>
                <h4 id="title_spark1"></h4>
              </div>
              <div id="spark1"></div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="box box3">
              <div class="details">
                <h3></h3>
                <h4 id="title_spark2"></h4>
              </div>
              <div id="spark2"></div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="box box4">
              <div class="details">
                <h3></h3>
                <h4 id="title_spark3"></h4>
              </div>
              <div id="spark3"></div>
            </div>
          </div>
        </div>

        <div class="row mt-4">
          <div class="col-md-5">
            <div class="box shadow mt-4">
              <div id="radialBarBottom"></div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="box shadow mt-4">
              <div id="line-adwords" class=""></div>
            </div>
          </div>
        </div>

        <div class="row mt-4">
          <div class="col-md-5">
            <div class="box shadow mt-4">
              <div id="barchart"></div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="box shadow mt-4">
              <div id="areachart"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
@endsection

@section('script_cabecera')

@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      dashboardfunction(JSON.parse(JSON.stringify({!!$reporte!!})));

    });
  </script>
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.slim.min.js"></script> --}}
  <script src="{{ asset('dashboards/apexcharts.js') }}"></script>
  <!-- installing few libraries -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/downloadjs/1.4.8/download.min.js"></script>
  <script src="{{ asset('dashboards/script.js') }}" async="async"></script>

@endsection
