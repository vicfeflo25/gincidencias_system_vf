@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endsection

@section('content')
  <br>
  <section class="content mb-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card card-default {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <input type="hidden" name="id_solicitud" id="id_solicitud" value="{{ $solicitud->id_solicitud }}">
            <input type="hidden" name="id_usuario" id="id_usuario" value="{{ Auth::user()->id_usuario }}">
            <div class="card-body">
              <div class="row" id="divLabelTema">
                <div class="col-md-6">
                  <label class="mb-0 mr-2 {{ Auth::user()->getConf()->letra_theme }}">{{ $solicitud->solicitud_tema }}</label> @if(($solicitud->solicitud_estado == 1 || $solicitud->solicitud_estado == 2) && (Auth::user()->id_usuario == $solicitud->solicitud_user_create || Auth::user()->id_usuario == $solicitud->solicitud_user_asigna || Auth::user()->rol[0]->id_rol == env('ID_ADMINISTRADOR')))<a href="#" id="editTema"><i class="fas fa-pen"></i></a>@endif
                </div>
              </div>
              <div class="row" id="divInputTema" hidden>
                <div class="col-md-6">
                  <input type="text" class="form-control input-sm" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="solicitud_tema" id="solicitud_tema" value="{{ $solicitud->solicitud_tema }}">
                </div>
                <div class="col-sm-2">
                  <a id="saveTema" class="btn btn-sm m-1 btn-primary"><i class="fas fa-check"></i></a>
                  <a id="CancelarTema" class="btn btn-sm m-1 btn-secondary"><i class="fas fa-times"></i></a>
                </div>
              </div>
              <hr>
              <div class="row">
                @foreach($solicitud->comentarios as $key => $value)
                  @if(in_array(Auth::user()->rol[0]->id_rol,$value->comentario_permisos))
                  <div class="col-md-1 mt-2">
                    <div class="form-group">
                      <span class="border rounded border-success p-2 bg-gradient-success">{{ (empty($value->usuario_apellidop[0]))?$value->usuario_nombre[0]:$value->usuario_nombre[0].$value->usuario_apellidop[0] }}</span>
                    </div>
                  </div>
                  <div class="col-md-11 mt-1">
                    <div class="form-group">
                      <strong>{{ $value->comentario_usuario }}</strong> - <span>{{ $value->comentario_correo }}</span>
                    </div>
                  </div>
                  <div class="col-md-12 mb-1">
                    <textarea class="form-control input-sm" readonly name="name" rows="4" cols="500">{{ $value->comentario_descripcion }}</textarea>
                  </div>
                  @foreach($value->imagenes as $k => $val)
                  <div class="col-md-12 mt-2 mb-2 text-center mb-1">
                    <img src="{{ asset($val->adjuntar_url) }}" width="100%" alt="{{ $solicitud->solicitud_numero.'-'.($key+1) }}">
                  </div>
                  @endforeach
                  @endif
                @endforeach
                @if($solicitud->solicitud_estado == 1 || $solicitud->solicitud_estado == 2)
                <div class="col-md-1 mt-2">
                  <div class="form-group">
                    <span class="border rounded border-primary p-2  bg-gradient-primary">{{ (empty(Auth::user()->usu_apellidop[0]))?Auth::user()->usu_nombre[0]:Auth::user()->usu_nombre[0].Auth::user()->usu_apellidop[0] }}</span>
                  </div>
                </div>
                <div class="col-md-11 mt-2">
                  <div class="form-group">
                    <strong>{{ Auth::user()->getFullnameAttribute() }}</strong> - <span>{{ Auth::user()->usu_email }}</span>
                  </div>
                </div>
                <div class="col-md-12 mt-2">
                  <a id="addComentario" class="btn btn-sm btn-info text-white"> Agregar Comentario</a>
                  @if(Auth::user()->rol[0]->id_rol == env('ID_ADMINISTRADOR'))
                  <a id="addNota" class="btn btn-sm btn-secondary text-white"> Agregar Nota al Personal</a>
                  @endif
                  <form enctype="multipart/form-data" id="formComentario" method="POST">
                    <textarea id="comentario_descripcion" hidden class="form-control input-sm mb-2" name="comentario_descripcion" rows="4" cols="500" placeholder="Mensaje...."></textarea>

                    <div class="form-group alert alert-info" hidden id="div-Adjuntar">
                      <span class="text-sm">Tamaño Máximo 2MB:</span>
                      @for($i=0;$i<3;$i++)
                      <input class="form-control input-sm col-md-6 mr-1 border-0 bg-info" type="file" id="file_adjuntar[{{ $i }}]" name="file_adjuntar[]" accept="image/png, image/jpeg, image/jpg">
                      @endfor
                    </div>

                    <input type="hidden" name="comentario_tipo" id="comentario_tipo">
                    <input type="hidden" name="comentario_user_create" id="comentario_user_create" value="{{ Auth::user()->id_usuario }}">
                    <a id="cancelComentario" hidden class="btn btn-sm btn-secondary text-white mt-1"> Cancelar</a>
                    <a id="uptComentario" hidden class="btn btn-sm btn-primary text-white mt-1 addComentario" data-tipo="2"> Agregar</a>
                    @if(in_array(Auth::user()->rol[0]->id_rol,array(env('ID_ADMINISTRADOR'),env('ID_SOPORTE'))))
                    <a id="uptFinComentario" hidden class="btn btn-sm btn-success text-white mt-1 addComentario" data-tipo="3"> Agregar y Solucionar</a>
                    @endif
                  </form>
                </div>
                @endif
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-default {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
            <div class="card-header">
              <div class="row align-items-end">
                <div class="col-md-6">
                  <span class="badge bg-secondary"><label class="h6 m-1">Ticket #{{ $solicitud->solicitud_numero }}</label></span>
                </div>
                {{-- @if(($solicitud->solicitud_estado == 1 || $solicitud->solicitud_estado == 2) && (Auth::user()->id_usuario == $solicitud->solicitud_user_create || Auth::user()->id_usuario == $solicitud->solicitud_user_asigna || Auth::user()->rol[0]->id_rol == env('ID_ADMINISTRADOR')))
                <div class="d-flex align-items-end flex-column col-md-6">
                  <a id="btnDescartar" class="btn btn-sm btn-danger text-white"><i class="fas fa-times-circle"></i> Descartar</a>
                </div>
                @endif --}}
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td><span class="badge {{ $solicitud->color_estado }} badge-status" title="ESTADO: {{ $solicitud->nombre_estado }}"><label class="h6">{{ $solicitud->nombre_estado[0] }}</label></span> {{$solicitud->nombre_estado}}</td>
                      </tr>
                      <tr>
                        <td><span class="badge {{ $solicitud->prioridad_color }} badge-status" title="ESTADO: {{ $solicitud->prioridad }}"><label class="h6">{{ $solicitud->prioridad[0] }}</label></span> {{$solicitud->prioridad}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m-1"><i class="fas fa-bookmark"></i> Tipo</label>
                            </div>
                            <div class="col-md-8">
                              <p class="m-1"> {{$solicitud->tipo}}</p>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m-1"><i class="fas fa-user-tag"></i> Asignado</label>
                            </div>
                            <div class="col-md-8">
                              <select class="form-control input-sm" {{ (!in_array(Auth::user()->rol[0]->id_rol,array(env('ID_ADMINISTRADOR'),env('ID_SOPORTE'))) || in_array($solicitud->solicitud_estado,array(3,4)))?'disabled':'' }} name="id_asignado" id="id_asignado">
                                <option value="0">SELECCIONE</option>
                                @foreach($solicitud->usuarios as $key => $value)
                                <option value="{{ $value->id_usuario }}" {{ ($value->id_usuario == $solicitud->solicitud_asigna_user)?'selected':'' }}>{{ mb_strtoupper($value->usu_nombre.' '.$value->usu_apellidop.' '.$value->usu_apellidom) }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m-1"><i class="fas fa-asterisk"></i> Estado</label>
                            </div>
                            <div class="col-md-8">
                              <select class="form-control input-sm" {{ ((Auth::user()->rol[0]->id_rol == env('ID_SOLICITANTE')) || in_array($solicitud->solicitud_estado,array(4,5)))?'disabled':'' }} name="id_resolucion" id="id_resolucion">
                                <option value="0">SELECCIONE</option>
                                <option value="0" {{ ($solicitud->solicitud_estado == 1)?'selected':'' }}>ABIERTO</option>
                                <option value="0" {{ ($solicitud->solicitud_estado == 2)?'selected':'' }}>EN PROGRESO</option>
                                <option value="1" {{ ($solicitud->solicitud_estado == 3)?'selected':'' }}>SOLUCIONADO</option>
                                <option value="3" {{ ($solicitud->solicitud_estado == 4)?'selected':'' }}>CERRADO</option>
                              </select>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m-1"><i class="far fa-calendar-alt"></i> Creación</label>
                            </div>
                            <div class="col-md-8">
                              <p class="m-1"> {{$solicitud->mes_creacion}}</p>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m-1"><i class="far fa-calendar-alt"></i> Asignado el</label>
                            </div>
                            <div class="col-md-8">
                              <p class="m-1"> {{ $solicitud->mes_asignacion }}</p>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m-1" style="white-space: nowrap;"><i class="far fa-calendar-alt"></i> Actualizado en</label>
                            </div>
                            <div class="col-md-8">
                              <p class="m-1"> {{ $solicitud->mes_modificate }}</p>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td>
                          <label>Área</label>
                          <p>{{ $solicitud->area_nombre }}</p>
                          <label>Categoria</label>
                          <p>{{ $solicitud->categoria }}</p>
                          <label>Sub Categoria</label>
                          <p>{{ $solicitud->subcategoria }}</p>
                          <label>Nombre Solicitante</label>
                          <p>{{ $solicitud->nombre_creador.' '.$solicitud->apellidop_creador.' '.$solicitud->apellidom_creador }}</p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m-1"><i class="fas fa-paperclip"></i> Adjuntos</label>
                            </div>
                            <div class="col-md-8 ">
                              <span class="badge badge-secondary">{{ $solicitud->contador }}</span>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      //var subcategorias = $.parseJSON($("#subcategorias").val());
    });

    $("#editTema").on('click',function(){
      $("#divLabelTema").attr('hidden','hidden')
      $("#divInputTema").removeAttr('hidden')
    })

    $("#CancelarTema").on('click',function(){
      $("#divInputTema").attr('hidden','hidden')
      $("#divLabelTema").removeAttr('hidden')
    })

    $("#saveTema").on('click',function(){
      if($("#solicitud_tema").val() != null || $("#solicitud_tema").val() != ''){
        Swal.fire({
          title: '¿Está usted seguro de cambiar el Nombre del Tema de la Solicitud?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
        }).then((result) => {
          if (result.isConfirmed) {
            $(".preloader-wrapper").fadeIn(500, 'swing');
            $.ajax({
            	url: '../api/changeTema',
              method: "POST",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {
                      'id_solicitud': $("#id_solicitud").val(),
                      'solicitud_tema': $("#solicitud_tema").val(),
                      'id_usuario': $("#id_usuario").val()
                    },
              success: function(respuesta) {
                $(".preloader-wrapper").fadeOut(500, 'swing');
                Swal.fire({
                  icon: respuesta.tipo,
                  title: respuesta.titulo,
                  text: respuesta.mensaje,
                  showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
                }).then((result) => {
                  if (result.isConfirmed) {
                    if(respuesta.error == 0){
                      $("#divLabelTema label").html(respuesta.solicitud_tema)
                      $("#divInputTema").attr('hidden','hidden')
                      $("#divLabelTema").removeAttr('hidden')
                    }
                  }
                });
            	}
            });
          }else if (result.dismiss === Swal.DismissReason.cancel) {
            $("#divLabelTema")
            $("#divInputTema").attr('hidden','hidden')
            $("#divLabelTema").removeAttr('hidden')

          }
        })
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar todos los campos obligatorios para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })

    $("#addComentario").on('click',function(){
      $("#addComentario").attr('hidden','hidden');
      $("#addNota").attr('hidden','hidden');
      $("#cancelComentario").removeAttr('hidden');
      $("#uptComentario").removeAttr('hidden');
      $("#uptFinComentario").removeAttr('hidden');
      $("#comentario_descripcion").removeAttr('hidden');
      $("#div-Adjuntar").removeAttr('hidden');
      $("#comentario_tipo").val(2);
      $("#uptComentario").attr('data-tipo',2)
      $("#comentario_descripcion").focus();
    });

    $("#addNota").on('click',function(){
      $("#addComentario").attr('hidden','hidden');
      $("#addNota").attr('hidden','hidden');
      $("#cancelComentario").removeAttr('hidden');
      $("#uptComentario").removeAttr('hidden');
      $("#comentario_descripcion").removeAttr('hidden');
      $("#div-Adjuntar").removeAttr('hidden');
      $("#comentario_tipo").val(3);
      $("#comentario_descripcion").focus();
      $("#uptComentario").attr('data-tipo',4)
    });

    $("#cancelComentario").on('click',function(){
      $("#addComentario").removeAttr('hidden');
      $("#addNota").removeAttr('hidden');
      $("#cancelComentario").attr('hidden','hidden');
      $("#uptComentario").attr('hidden','hidden');
      $("#uptFinComentario").attr('hidden','hidden');
      $("#comentario_descripcion").attr('hidden','hidden');
      $("#div-Adjuntar").attr('hidden','hidden');
      $("#comentario_descripcion").val('')
      $("#comentario_tipo").val('');
    });

    $("#id_asignado").on('change',function(){
      if($(this).val() != 0){
        Swal.fire({
          title: '¿Está usted seguro de asignar al usuario '+$("#id_asignado option:selected").text()+' la Solicitud?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
        }).then((result) => {
          if (result.isConfirmed) {
            $(".preloader-wrapper").fadeIn(500, 'swing');
            $.ajax({
            	url: '../api/asignaUserSolicitud',
              method: "POST",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {
                      'id_solicitud': $("#id_solicitud").val(),
                      'id_asignado': $("#id_asignado").val(),
                      'id_usuario': $("#id_usuario").val()
                    },
              success: function(respuesta) {
                $(".preloader-wrapper").fadeOut(500, 'swing');
                Swal.fire({
                  icon: respuesta.tipo,
                  title: respuesta.titulo,
                  text: respuesta.mensaje,
                  showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
                }).then((result) => {
                  if (result.isConfirmed) {
                    if(respuesta.error == 0){
                      window.location.reload();
                    }
                  }
                });
            	}
            });
          }else if (result.dismiss === Swal.DismissReason.cancel) {
            $(this).val(0)
          }
        })
      }
    });

    // $("#btnDescartar").on('click',function(){
    //   Swal.fire({
    //     title: '¿Está usted seguro de marcar como Descartada la Solicitud?',
    //     icon: 'question',
    //     showCancelButton: true,
    //     confirmButtonColor: '#3085d6',
    //     cancelButtonColor: '#d33',
    //     confirmButtonText: 'Si',
    //     cancelButtonText: 'No',
    //   }).then((result) => {
    //     if (result.isConfirmed) {
    //       $.ajax({
    //         url: '../api/descartSolicitud',
    //         method: "POST",
    //         headers: {
    //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         data: {
    //                 'id_solicitud': $("#id_solicitud").val(),
    //                 'id_usuario': $("#id_usuario").val()
    //               },
    //         success: function(respuesta) {
    //           Swal.fire({
    //             icon: respuesta.tipo,
    //             title: respuesta.titulo,
    //             text: respuesta.mensaje,
    //             showClass: {
    //               popup: 'animate__animated animate__fadeInDown'
    //             },
    //             hideClass: {
    //               popup: 'animate__animated animate__fadeOutUp'
    //             }
    //           }).then((result) => {
    //             if (result.isConfirmed) {
    //               if(respuesta.error == 0){
    //                 window.location.reload();
    //               }
    //             }
    //           });
    //         }
    //       });
    //     }else if (result.dismiss === Swal.DismissReason.cancel) {
    //       $(this).val(0)
    //     }
    //   })
    // });

    $("#id_resolucion").on('change',function(){
      if($(this).val() != 0){
        Swal.fire({
          title: '¿Está usted seguro de marcar como '+$("#id_resolucion option:selected").text()+' la Solicitud?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
        }).then((result) => {
          if (result.isConfirmed) {
            $(".preloader-wrapper").fadeIn(500, 'swing');
            $.ajax({
            	url: '../api/changeStatusSolicitud',
              method: "POST",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {
                      'id_solicitud': $("#id_solicitud").val(),
                      'id_resolucion': $("#id_resolucion").val(),
                      'id_usuario': $("#id_usuario").val()
                    },
              success: function(respuesta) {
                $(".preloader-wrapper").fadeOut(500, 'swing');
                Swal.fire({
                  icon: respuesta.tipo,
                  title: respuesta.titulo,
                  text: respuesta.mensaje,
                  showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                  hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                  }
                }).then((result) => {
                  if (result.isConfirmed) {
                    if(respuesta.error == 0){
                      window.location.reload();
                    }
                  }
                });
            	}
            });
          }else if (result.dismiss === Swal.DismissReason.cancel) {
            $(this).val(0)
          }
        })
      }
    });

    $(".addComentario").on('click',function(){
      if($("#comentario_descripcion").val().length != 0){
        $("#comentario_tipo").val($(this).data('tipo'))
        var form = new FormData(document.getElementById("formComentario"));
        form.append('id_solicitud',$("#id_solicitud").val())
        $(".preloader-wrapper").fadeIn(500, 'swing');
        $.ajax({
          url: '../api/addComentario',
          method: "POST",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: form,
          dataType:"json",
          contentType:false,
          processData:false,
          cache:false,
          success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  window.location.reload();
                }
              }
            });
          }
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar el comentario a agregar para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })


    $("#saveSolicitud").on('click',function(){
      let count = 0;
      $.each($("select.req"),function(key,value){
        if($(this).val() == 0){
          $(this).addClass('is-invalid')
          count++;
        }else{
          $(this).removeClass('is-invalid')
        }
      })
      $.each($("input.req"),function(key,value){
        if($(this).val() == 0){
          $(this).addClass('is-invalid')
          count++;
        }else{
          $(this).removeClass('is-invalid')
        }
      })
      $.each($("textarea.req"),function(key,value){
        if($(this).val() == 0){
          $(this).addClass('is-invalid')
          count++;
        }else{
          $(this).removeClass('is-invalid')
        }
      })
      if(count == 0){
        $(".preloader-wrapper").fadeIn(500, 'swing');
        var form = new FormData(document.getElementById("formSolicitud"));
        $.ajax({
        	url: 'api/saveSolicitud',
          method: "POST",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: form,
          dataType:"json",
          contentType:false,
          processData:false,
          cache:false,
        	success: function(respuesta) {
            $(".preloader-wrapper").fadeOut(500, 'swing');
            Swal.fire({
              icon: respuesta.tipo,
              title: respuesta.titulo,
              text: respuesta.mensaje,
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            }).then((result) => {
              if (result.isConfirmed) {
                if(respuesta.error == 0){
                  location.reload();
                }
              }
            });
        	}
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: '¡Oops!',
          text: '¡Debe completar todos los campos obligatorios para continuar!',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        });
      }
    })

    $(function () {
      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $('a img.img-fluid').removeClass('border border-dark')
        $(this).children().addClass('border border-dark')
        $("#rol_icono").val($(this).attr('href'))
      });
    })

  </script>
@endsection
