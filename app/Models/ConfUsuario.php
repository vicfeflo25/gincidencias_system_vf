<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfUsuario extends Model{

    protected $connection = 'mysql';
    protected $table = 'conf_usuario';
    protected $primaryKey = 'id_configuracion';
    public $timestamps = false;

}
