<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;
use App\Models\Icono;

class RolController extends Controller{

  public function __construct(){
      $this->middleware('auth');
  }

  public function listRol(){
    $list_rol = Rol::getListRol();
    return view('setting.role.listRole',compact('list_rol'));
  }

  public function addRole(){
    $list_iconos = Icono::getListIcono();
    return view('setting.role.newRole',compact('list_iconos'));
  }

  public function viewRole(){
    $rol = Rol::visualizeRol();
    return view('setting.role.viewRole',compact('rol'));
  }

  public function editRole(){
    $rol = Rol::viewRole();
    return view('setting.role.editRole',compact('rol'));
  }

}
