<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class Prioridad extends Model{

    protected $connection = 'mysql';
    protected $table = 'prioridad';
    protected $primaryKey = 'id_prioridad';
    public $timestamps = false;

    public static function getPrioridad(){
      $list = Prioridad::where('prioridad_estado',1)->orderBy('id_prioridad','asc')->get();
      return $list;
    }

    public static function getListPrioridad(){
      $prioridad = Prioridad::select('prioridad.*','u.usu_email as correo_creador','u.usu_nombre as nombre_creador','u.usu_apellidop as ap_creador','u.usu_apellidom as am_creador','ua.usu_email as correo_actualizador','ua.usu_nombre as nombre_actualizador','ua.usu_apellidop as ap_actualizador','ua.usu_apellidom as am_actualizador')
                      ->join('usuario_aplicacion as u','u.id_usuario','prioridad.prioridad_user_create')
                      ->leftJoin('usuario_aplicacion as ua','ua.id_usuario','prioridad.prioridad_user_modificate')
                      ->get();

      foreach ($prioridad as $key => $value) {
        if($value->prioridad_estado == 1){
          $value->nombre_estado = 'ACTIVO';
          $value->color_estado = 'success';
        }else{
          $value->nombre_estado = 'INACTIVO';
          $value->color_estado = 'danger';
        }

        if(empty($value->prioridad_user_modificate)){
          $value->nombre_auditoria = $value->nombre_creador.' '.$value->ap_creador.' '.$value->am_creador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->prioridad_fecha_create));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->prioridad_fecha_create));
        }else{
          $value->nombre_auditoria = $value->nombre_actualizador.' '.$value->ap_actualizador.' '.$value->am_actualizador;
          $value->fecha_auditoria =  date("d-m-Y",strtotime($value->prioridad_fecha_modificate));
          $value->hora_auditoria =  date("H:i:s",strtotime($value->prioridad_fecha_modificate));
        }
      }
      return $prioridad;
    }

    public static function PrioridadSave(){
      try {
        $prioridad_existe = Prioridad::where('prioridad_name',strtoupper(request()->prioridad_name))->first();
        if(empty($prioridad_existe)){
          $prioridad = new Nivel();
          $prioridad->prioridad_user_create = request()->prioridad_user_create;
          $prioridad->prioridad_fecha_create = now();
          $prioridad->prioridad_name = strtoupper(request()->prioridad_name);
          $prioridad->prioridad_estado = 1;
          $prioridad->save();
          return array("error"=>0,"mensaje"=>"Se agregó exitosamente la Prioridad ".$prioridad->prioridad_name,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"La Prioridad ".strtoupper(request()->prioridad_name)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function PrioridadUpdate(){
      try {
        $prioridad_existe = Prioridad::where('prioridad_name',strtoupper(request()->prioridad_name))->where('id_prioridad','!=',request()->id_prioridad)->first();
        if(empty($prioridad_existe)){
          $prioridad = Prioridad::find(request()->id_prioridad);
          $prioridad->prioridad_user_modificate = request()->prioridad_user_modificate;
          $prioridad->prioridad_fecha_modificate = now();
          $prioridad->prioridad_name = strtoupper(request()->prioridad_name);
          $prioridad->save();
          return array("error"=>0,"mensaje"=>"Se modificó exitosamente la Prioridad ".$prioridad->prioridad_name,"tipo"=>"success","title"=>"¡Bien!");
        }else{
          return array("error"=>1,"mensaje"=>"La Prioridad ".strtoupper(request()->prioridad_name)." que intenta registrar ya existe","tipo"=>"warning","title"=>"¡Oops!");
        }
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function changeStatePrioridad(){
      try {
        $prioridad = Prioridad::find(request()->id_prioridad);
        if($prioridad->prioridad_estado == 1){
          $prioridad->prioridad_estado = 0;
          $mensaje = 'Se inactivo exitosamente la Prioridad '.$prioridad->prioridad_name;
        }else{
          $prioridad->prioridad_estado = 1;
          $mensaje = 'Se activo exitosamente la Prioridad '.$prioridad->prioridad_name;
        }
        $prioridad->prioridad_user_modificate = request()->id_user;
        $prioridad->prioridad_fecha_modificate = now();
        $prioridad->save();
        return array("error"=>0,"mensaje"=>$mensaje,"tipo"=>"success","title"=>"¡Bien!");
      } catch (\Exception $e) {
        return array("error"=>1,"mensaje"=>"Existe un problema de conexión, inténtelo nuevamente","tipo"=>"error","titulo"=>"¡Oops!");
      }
    }

    public static function getPrioridadX(){
      $prioridad = Prioridad::where('id_prioridad',request()->id_prioridad)->first();
      return $prioridad;
    }

}
