<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;
use Carbon\Carbon;

class Reporte extends Model{

  public static function getRRI(){
    $fecha = explode(' - ',request()->fecha);
    $fecha1 =  str_replace('/','-',$fecha[0]);
    $fecha2 =  str_replace('/','-',$fecha[1]);

    $query = DB::SELECT("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(HORAS))) AS HORAS,
                          COUNT(*) AS TOTAL_RESUELTAS,
                          (SELECT
                          	COUNT(*)
                          	FROM sgi_solicitud
                          	WHERE solicitud_fecha = A.solicitud_fecha
                          	) AS TOTAL_SOLICITADAS,
                          (COUNT(*)/(SELECT
                          	COUNT(*)
                          	FROM sgi_solicitud
                          	WHERE solicitud_fecha = A.solicitud_fecha
                          	) ) AS RRI,
                          solicitud_fecha
                          FROM
                          (
                          	SELECT
                          	SEC_TO_TIME(TIMESTAMPDIFF(SECOND, solicitud_fecha_create, solicitud_fecha_soluciona)) HORAS,
                          	solicitud_fecha_create,solicitud_fecha
                          	FROM sgi_solicitud s
                          	WHERE solicitud_estado = 3 OR solicitud_estado = 4
                          ) A
                          WHERE A.solicitud_fecha BETWEEN '$fecha1' AND '$fecha2'
                          GROUP BY A.solicitud_fecha");

    foreach($query as $key => $val){
      $val->RRI = round(($val->RRI),2, PHP_ROUND_HALF_DOWN);
      //$val->RRI = 0.01 * (int)($val->RRI*100);
    }


    return $query;
  }

  public static function getRULI(){
    $fecha = explode(' - ',request()->fecha);
    $fecha1 =  str_replace('/','-',$fecha[0]);
    $fecha2 =  str_replace('/','-',$fecha[1]);

    $query = DB::SELECT("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(HORAS))) AS HORAS,
                          ((SELECT COUNT(*)
                            FROM usuario_aplicacion u
                            INNER JOIN usuario_rol ur ON ur.id_usuario = u.id_usuario
                            WHERE id_rol = 3
                            AND usu_estado = 1)*7) HORAS_DISPONIBLES,
                          solicitud_fecha
                          FROM
                          (
                          	SELECT
                          	SEC_TO_TIME(TIMESTAMPDIFF(SECOND, solicitud_fecha_create, solicitud_fecha_soluciona)) HORAS,
                          	solicitud_fecha_create,solicitud_fecha
                          	FROM sgi_solicitud s
                          	WHERE solicitud_estado = 3 OR solicitud_estado = 4
                          ) A
                          WHERE A.solicitud_fecha BETWEEN '$fecha1' AND '$fecha2'
                          GROUP BY A.solicitud_fecha");

    foreach($query as $key => $val){
      $HORAS = explode(':',$val->HORAS);
      $val->HORAS = $HORAS[0].'.'.$HORAS[1];
      //$val->RULI = 0.01 * (int)($val->RULI*100);
      $val->RULI = round(($val->HORAS/$val->HORAS_DISPONIBLES),2);
    }

    return $query;
  }

  public static function getIncidencias(){
    $fecha = explode(' - ',request()->fecha);
    $fecha1 =  str_replace('/','-',$fecha[0]);
    $fecha2 =  str_replace('/','-',$fecha[1]);

    $ticket = Ticket::select('solicitud_numero','categoria_name','solicitud_fecha_create','solicitud_fecha_soluciona','prioridad_name',
                              'u.usu_nombre as nombre_creador','u.usu_apellidop as apellidop_creador','u.usu_apellidom as apellidom_creador',
                              'u2.usu_nombre as nombre_agente','u2.usu_apellidop as apellidop_agente','u2.usu_apellidom as apellidom_agente','solicitud_estado')
                    ->join('categoria as c','c.id_categoria','sgi_solicitud.id_categoria')
                    ->join('prioridad as p','p.id_prioridad','sgi_solicitud.id_prioridad')
                    ->join('usuario_aplicacion as u','u.id_usuario','sgi_solicitud.solicitud_user_create')
                    ->join('usuario_aplicacion as u2','u2.id_usuario','sgi_solicitud.solicitud_asigna_user')
                    ->whereBetween('solicitud_fecha', [$fecha1, $fecha2])
                    ->get();

    foreach($ticket as $key => $value){
      $value->solicitante = $value->nombre_creador.' '.$value->apellidop_creador.' '.$value->apellidom_creador;
      $value->agente = $value->nombre_agente.' '.$value->apellidop_agente.' '.$value->apellidom_agente;
      if($value->solicitud_estado == 1){
        $value->estado = 'ABIERTO';
      }else if($value->solicitud_estado == 2){
        $value->estado = 'EN PROGRESO';
      }else if($value->solicitud_estado == 3){
        $value->estado = 'RESUELTA';
      }else if($value->solicitud_estado == 4){
        $value->estado = 'CERRADO';
      }
    }

    return $ticket;
  }

  public static function getQueryDashboard(){
    $reporte = new Ticket();

    $array_meses = array();
    $array_porc = array();
    $array_fecha = array();
    $mes_actual = date("m");


    $start = Carbon::now()->startOfMonth()->format('d-m-Y');
    $dia = date("d-m-Y",strtotime($start."- 3 month"));

    $statement = DB::statement("SET lc_time_names = 'es_ES'");
    $ultimos_meses = Ticket::select(DB::raw('UPPER(MONTHname(solicitud_fecha)) as mes'))
                                    ->where('solicitud_fecha_create','>=',$dia)
                                    ->groupby(DB::raw('YEAR(solicitud_fecha_create)'))
                                    ->groupby(DB::raw('MONTH(solicitud_fecha_create)'))
                                    ->orderBy('mes','desc')
                                    ->get();

    foreach($ultimos_meses as $key => $value){
      if(!in_array($value->mes, $array_meses)){
        array_push($array_meses,$value->mes);
      }
    }
    $reporte->ultimos_meses = $array_meses;

    $ultimos_porc = DB::select('SELECT *,total,
                                  (select COUNT(*)
                                    from sgi_solicitud
                                   where year(solicitud_fecha_create) = YEAR(A.MAX)
                                   AND month(solicitud_fecha_create) = month(A.MAX)
                                   AND (solicitud_estado = 3 OR solicitud_estado = 4)
                                   group by MonthName(solicitud_fecha_create)) AS contador
                                  FROM
                                  (select COUNT(*) AS total,MAX(solicitud_fecha_create) AS MAX,
                                  LAST_DAY(MAX(solicitud_fecha_create)) + interval 1 DAY - INTERVAL 1 MONTH AS fecha
                                  from sgi_solicitud
                                  where year(solicitud_fecha_create) = year(curdate())
                                  AND (`solicitud_fecha_create` >= last_day(now()) + interval 1 day - INTERVAL 3 MONTH
                                  AND CURDATE() >= solicitud_fecha)
                                  group by MonthName(solicitud_fecha_create)) AS A
                                  order by max asc');

    foreach($ultimos_porc as $key => $value){
      $value->porc = (100 * $value->contador)/$value->total;
      array_push($array_porc, (int)($value->porc));
    }
    $reporte->ultimos_porc = $array_porc;

    $fechas = Ticket::select('solicitud_fecha')
                    ->groupBy('solicitud_fecha')
                    ->orderBy('solicitud_fecha','desc')
                    ->distinct('solicitud_fecha')
                    ->limit(6)
                    ->get();

    $fechas = $fechas->sortBy('solicitud_fecha');
    $fecha2 = $fechas[0]->solicitud_fecha;
    $fecha1 = $fechas[count($fechas)-1]->solicitud_fecha;
    $array_fechas = array();

    foreach($fechas as $key => $value){
      array_push($array_fecha,date("d/m/Y", strtotime($value->solicitud_fecha)));
      array_push($array_fechas,$value->solicitud_fecha);
    }
    $reporte->fechas = $array_fecha;
    $reporte->fecha_last = $array_fechas;
    $reporte->fecha1 = $fecha1;
    $reporte->fecha2 = $fecha2;


    $categorias = Ticket::select('categoria_name')
                        ->join('categoria as c','c.id_categoria','sgi_solicitud.id_categoria')
                        ->where('solicitud_fecha','>=',$fecha1)
                        ->where('solicitud_fecha','<=',$fecha2)
                        ->orderBy('c.id_categoria','asc')
                        ->distinct('categoria_name')
                        ->get();


    $query = '';
    foreach($reporte->fecha_last as $key => $value){
      $query .= " SUM(CASE WHEN b.solicitud_fecha = '$value'
                  THEN (SELECT COUNT(*) AS conta FROM sgi_solicitud
                  WHERE id_categoria = b.id_categoria
                  AND solicitud_fecha = '$value' ) ELSE 0
                  END) AS '$value',";
    }
    $query = substr($query, 0, -1);


    $frecuentes = (DB::select("SELECT
                                      $query
                                      from
                                      (SELECT *,
                                      (
                                      SELECT COUNT(*)AS conta FROM sgi_solicitud
                                      WHERE id_categoria = A.id_categoria
                                      AND solicitud_fecha = A.solicitud_fecha
                                      ) AS contador
                                      from
                                      (SELECT DISTINCT categoria_name, solicitud_Fecha,s.id_categoria
                                      FROM sgi_solicitud s
                                      INNER JOIN sgi_area a ON a.id_area = s.id_area
                                      INNER JOIN categoria c ON c.id_categoria = s.id_categoria
                                      WHERE solicitud_fecha >= '$fecha1'
                                      AND solicitud_fecha <= '$fecha2'
                                      GROUP by solicitud_fecha_create,id_solicitud
                                      )AS A
                                      ORDER BY solicitud_fecha,id_categoria) AS b
                                      GROUP BY id_categoria
                                      ORDER BY id_categoria asc"));


    $array_serie = array();

    foreach($frecuentes as $value){
      $array_serie2 = array();
      foreach($reporte->fecha_last as $key => $val) {
        array_push($array_serie2,$value->$val);
      }
      $array_serie3[] = $array_serie2;
    }

    $array_seriefinal = [];
    foreach($array_serie3 as $key => $value){
      array_push($array_seriefinal,array('name'=>$categorias[$key]->categoria_name,'data'=>$value));
    }
    $reporte->categorias = $array_seriefinal;

    $soporte = Ticket::select(DB::raw('CONCAT(usu_apellidop,\' \' ,usu_apellidom) as usuario'))
                        ->join('usuario_aplicacion as u','u.id_usuario','sgi_solicitud.solicitud_asigna_user')
                        ->where('solicitud_fecha','>=',$fecha1)
                        ->where('solicitud_fecha','<=',$fecha2)
                        ->orderBy('usuario','asc')
                        ->distinct('usuario')
                        ->get();

    $query_sop = '';
    foreach($reporte->fecha_last as $key => $value){
      $query_sop .= " SUM(CASE WHEN b.solicitud_fecha = '$value'
                  THEN (SELECT COUNT(*) AS conta FROM sgi_solicitud
                  WHERE solicitud_asigna_user = b.solicitud_asigna_user
                  AND solicitud_fecha = '$value' ) ELSE 0
                  END) AS '$value',";
    }
    $query_sop = substr($query_sop, 0, -1);

    $frecuentes_sop = (DB::select("SELECT
                                      $query_sop
                                      from
                                      (SELECT *,
                                      (
                                      SELECT COUNT(*)AS conta FROM sgi_solicitud
                                      WHERE solicitud_asigna_user = A.solicitud_asigna_user
                                      AND solicitud_fecha = A.solicitud_fecha
                                      ) AS contador
                                      from
                                      (SELECT DISTINCT solicitud_asigna_user, solicitud_Fecha
                                      FROM sgi_solicitud s
                                      WHERE solicitud_fecha >= '$fecha1'
                                      AND solicitud_fecha <= '$fecha2'
                                      GROUP by solicitud_fecha_create,id_solicitud
                                      )AS A
                                      ORDER BY solicitud_fecha,solicitud_asigna_user) AS b
                                      GROUP BY solicitud_asigna_user
                                      ORDER BY solicitud_asigna_user asc"));


      $query_sop_sol = '';
      foreach($reporte->fecha_last as $key => $value){
        $query_sop_sol .= " SUM(CASE WHEN b.solicitud_fecha = '$value'
                    THEN (SELECT COUNT(*) AS conta FROM sgi_solicitud
                    WHERE solicitud_user_soluciona = b.solicitud_user_soluciona
                    AND solicitud_fecha = '$value' ) ELSE 0
                    END) AS '$value',";
      }
      $query_sop_sol = substr($query_sop_sol, 0, -1);

      $frecuentes_sop_sol = (DB::select("SELECT
                                        $query_sop_sol
                                        from
                                        (SELECT *,
                                        (
                                        SELECT COUNT(*)AS conta FROM sgi_solicitud
                                        WHERE solicitud_user_soluciona = A.solicitud_user_soluciona
                                        AND solicitud_fecha = A.solicitud_fecha
                                        ) AS contador
                                        from
                                        (SELECT DISTINCT solicitud_user_soluciona, solicitud_Fecha
                                        FROM sgi_solicitud s
                                        WHERE solicitud_fecha >= '$fecha1'
                                        AND solicitud_fecha <= '$fecha2'
                                        AND (solicitud_estado = 3 or solicitud_estado = 4)
                                        GROUP by solicitud_fecha_create,id_solicitud
                                        )AS A
                                        ORDER BY solicitud_fecha,solicitud_user_soluciona) AS b
                                        GROUP BY solicitud_user_soluciona
                                        ORDER BY solicitud_user_soluciona asc"));

    foreach($frecuentes_sop as $value){
      $array_sop = array();
      foreach($reporte->fecha_last as $key => $val) {
        array_push($array_sop,$value->$val);
      }
      $array_usu_sop[] = $array_sop;
    }

    foreach($frecuentes_sop_sol as $value){
      $array_sop_sol = array();
      foreach($reporte->fecha_last as $key => $val) {
        array_push($array_sop_sol,$value->$val);
      }
      $array_usu_sop_sol[] = $array_sop_sol;
    }

    $array_soportefinal = [];
    foreach($array_usu_sop as $key => $value){
      array_push($array_soportefinal,array('name'=>$soporte[$key]->usuario,'data'=>$value));
    }
    $reporte->soporte = $array_soportefinal;

    $array_soportefinal_sol = [];
    foreach($array_usu_sop_sol as $key => $value){
      array_push($array_soportefinal_sol,array('name'=>$soporte[$key]->usuario,'data'=>$value));
    }
    $reporte->soporte_solucion = $array_soportefinal_sol;

    return $reporte;
  }

}
