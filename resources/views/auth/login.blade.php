<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
  	<title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

  	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/animate/animate.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/css-hamburgers/hamburgers.min.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/animsition/css/animsition.min.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/toastr/toastr.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
  	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
  </head>
  <body style="background-color: #666666;">

  	<div class="limiter">
  		<div class="container-login100">
  			<div class="wrap-login100">
  				<form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="text-center">
              <img src="{{ asset('img/logo-sidebar.png') }}" alt="">
            </div>
  					<span class="login100-form-title p-b-43">
  						Iniciar Sesión para Continuar
  					</span>


  					<div class="wrap-input100 validate-input">
  						<input class="input100 @error('usuario') is-invalid @enderror" type="text" id="usuario" name="usuario" value="{{ old('usuario') }}" required autocomplete="usuario" autofocus>
  						<span class="focus-input100"></span>
  						<span class="label-input100">{{ __('Usuario') }}</span>
  					</div>

  					<div class="wrap-input100 validate-input" data-validate="Password is required">
  						<input class="input100 @error('password') is-invalid @enderror" type="password" id="password" name="password" required autocomplete="current-password">
  						<span class="focus-input100"></span>
  						<span class="label-input100">{{ __('Password') }}</span>
  					</div>
            <br>
  					{{-- <div class="flex-sb-m w-full p-t-3 p-b-32">
  						<div class="contact100-form-checkbox">
  							<input class="input-checkbox100" id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
  							<label class="label-checkbox100" for="remember">
  								{{ __('Recuérdame') }}
  							</label>
  						</div>
  					</div> --}}
  					<div class="container-login100-form-btn">
  						<button class="login100-form-btn" type="submit">
  							ACCEDER
  						</button>
  					</div>
  				</form>
  				<div class="login100-more" style="background-image: url('img/fondo_login_new.png');">
  				</div>
  			</div>
  		</div>
  	</div>
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/animsition/js/animsition.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/popper.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/countdowntime/countdowntime.js') }}"></script>
  	<script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript">
      @error('usuario')
      Command: toastr["error"]('{{ $message }}', "ALERTA")
      @enderror

      @error('password')
      Command: toastr["error"]('{{ $message }}', "ALERTA")
      @enderror
      toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
      }
    </script>

  </body>
</html>
