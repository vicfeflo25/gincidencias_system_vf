@extends('layouts.index')
@section('style')
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-rowreorder/css/rowReorder.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 {{ Auth::user()->getConf()->letra_theme }}">{{ $texto }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active"><a href="#">Reporte</a></li>
            <li class="breadcrumb-item active">{{ $subtexto }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">

      <!-- Default box -->
      <div class="col-md-12">
        <div class="card {{ Auth::user()->getConf()->body_theme.' '.Auth::user()->getConf()->border_theme }}">
          <div class="card-header {{ Auth::user()->getConf()->border_theme }}">
            <h3 class="card-title">{{ $subtexto }}</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>DESDE - HASTA:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" id="reservation">
                    <input type="hidden" name="type" id="type" value="{{ $type }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="row" id="divContentRRI" hidden>
              <div class="col-md-12 text-center">
                <div class="preloader"><br><br><img class="center-block" src="{{ asset('img/colegio_logo.png') }}" style="width: 250px;"><br><br><p><img src="https://www.gestionflesan.cl/controlflujo/images/preloader_2019.gif" style="width: 25px;"><strong style="color: #adadad!important;font-size:13px;"> OBTENIENDO DATOS</strong></p></div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.flash.min.js') }}"></script>
  <script src="{{ asset('/plugins/ajax/jszip.min.js') }}"></script>
  <script src="{{ asset('/plugins/ajax/pdfmake.min.js') }}"></script>
  <script src="{{ asset('/plugins/ajax/vfs_fonts.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>

  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-rowreorder/js/dataTables.rowReorder.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <script src="{{ asset('/plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('/plugins/daterangepicker/daterangepicker.js') }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $(".preloader-wrapper").fadeOut(500, 'swing');
      $('#reservation').daterangepicker({
        minDate: new Date(2020, 7, 01),
        // maxDate: new Date(),
        locale: {
          format: 'YYYY/MM/DD'
        },
      })



    });

    $('#reservation').on('apply.daterangepicker', function(ev, picker) {
      $("#divContentRRI .preloader").fadeIn(500, 'swing');
      $("#divContentRRI").removeAttr('hidden')
      let ruta = '';
      if($("#type").val() == 1){
        ruta = '../api/getRRI';
      }else if($("#type").val() == 2){
        ruta = '../api/getRULI';
      }else if($("#type").val() == 3){
        ruta = '../api/getINCIDENCIA';
      }

      $.ajax({
        url: ruta,
        method: "PUT",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: { 'fecha':$("#reservation").val() },
        success: function(respuesta) {
          $("#divContentRRI .preloader").fadeOut(500, 'swing');
          $("#divContentRRI").children().html(respuesta)
        }
      });
    });







  </script>
@endsection
