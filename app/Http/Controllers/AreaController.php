<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;

class AreaController extends Controller{

  public function __construct(){
      $this->middleware('auth');
  }

  public function listArea(){
    $area = Area::getListArea();
    return view('setting.area.listArea',compact('area'));
  }

  public function areaNew(){
    return view('setting.area.newArea');
  }

  public function viewArea(){
    $area = Area::getArea();
    return view('setting.area.viewArea',compact('area'));
  }

  public function editArea(){
    $area = Area::getArea();
    return view('setting.area.editArea',compact('area'));
  }


}
